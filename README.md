# E-Waste Manager

## Getting Started

### Prerequisites

Apache, PHP, MySQL.

## Built With

* [CodeIgniter](https://www.codeigniter.com/user_guide/) - The web framework used
* [Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/) - Front-end component library

## Contributing

## Authors

* **Levi Terante**

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.


## Acknowledgments

* Hat tip to anyone whose code was used

