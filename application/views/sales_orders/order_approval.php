<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Order approval</h1>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of orders for approval</h6>
    </div>
    <br>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" style="text-align:center;" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th style="text-align:center"> Order N°</th>
              <th style="text-align:center"> Selling to</th>
              <th style="text-align:center"> Contact person</th>
              <th style="text-align:center"> Ordered By</th>
              <th style="text-align:center"> Status</th>
              <th style="text-align:center"> Action</th>
            </tr>
          </thead>
          <tbody id="order-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm Cancellation</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            Do you want to cancel this order?
        </div>
        <div class="modal-footer">
          <button type="button" id="btnDelete" class="btn btn-danger">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div id="approveModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm approval</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" id="wrap">
          <table id="order-details" class="table table-bordered" style="text-align:center;">
            <thead>
              <thead id="order-details-header">
              </thead>
              <tr>
                <th style="text-align:center"> N°</th>
                <th style="text-align:center"> Product code</th>
                <th style="text-align:center"> Product</th>
                <th style="text-align:center"> Description</th>
                <th style="text-align:center"> Quantity</th>
                <th style="text-align:center"> Unit</th>
                <th style="text-align:center"> Price</th>
                <th style="text-align:center"> Amount</th>
              </tr>
            </thead>
            <tbody id="order-details-body">
            </tbody>
            <tfoot id="order-details-foot">
            </tfoot>
          </table>
            Do you want to approve this order?
          <form class="form-horizontal" action="" method="post" id="form-approval">
            <div class="form-group">
              <div class="container">
                <textarea class="form-control" name="approval-note" rows="2" id="approval-note" required></textarea>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnApprove" class="btn btn-success">Approve</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>
<script type="text/javascript">
  $(function(){
    var order_id  = null;
    showAllOrders();
    //Show all orders function
    function showAllOrders(){
      $('#dataTable').DataTable({
          "destroy": true,
          "ajax"   : {
          "type"   : "POST",
          "url"    : '<?php echo base_url() ?>orders/getAllOrdersForApproval',
          "dataSrc": function (data) {
            var return_data = new Array();
            for(var i=0;i< data.length; i++){
              return_data.push({
                //'order_code': data[i].order_code,
                'order_type'      : data[i].order_type,
                'ordered_at'      : data[i].order_date_time,
                'customer_name'   : data[i].customer_name,
                'status'          : data[i].status,
                'order_id'        : data[i].order_id,
                'staff_name'      : data[i].staff_name,
                'total_amount'    : data[i].total_amount,
                'update_date_time': data[i].update_date_time,
                'order_code'      : data[i].order_code,
                'company_name'    : data[i].company_name,
                'ordered_by'      : data[i].ordered_by
              })
            }
            return return_data;
          }
        },
          "columns": [
            {'data': 'order_code'},
            {'data': 'company_name'},
            {'data': 'customer_name'},
            {'data': 'ordered_by'},
            {'data': 'status'},
            {
              data: null,
              render: function ( data, type, row ) {
                if (data.status == 'approved' || data.status == 'cancelled') {
                  return '<a href="javascript:;" style="margin:5px;" id="details" class="btn btn-info order-details" data="'+data.order_id+'">Details</a>';
                }
                else{
                  return '<a href="javascript:;" style="margin:5px;" class="btn btn-success order-approve" data="'+data.order_id+'">Approve</a>' +
                         '<a href="javascript:;" style="margin:5px;" class="btn btn-danger order-cancel" data="'+data.order_id+'">Cancel</a>';
                }
              }
            }
          ]
      });
    }
    //Approve order
    $('#order-data').on('click', '.order-approve', function(){
      if ($(this).attr('id') == 'approved' || $(this).attr('id') == 'cancelled') {
        var order_status = jsUcfirst($(this).attr('id'));
        $('.alert-success').html('Order status : ' + order_status).fadeIn().delay(1000).fadeOut('slow');
      }
      else{
        order_id = $(this).attr('data');
        $('#approveModal').modal('show');
        $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>orders/getOrderDetailsById',
          data: {order_id: order_id},
          dataType: 'json',
          success: function(data){
            var html = '';
            var i;
            var total = 0;
            var quantity = 0;
            var order_id_label = ''
            var order_details_header = '<tr>' +
                                        '<th style="text-align:center" colspan="3"> Order ID : </th>' +
                                        '<td style="text-align:center" colspan="5"> ' + data[0].order_code + ' </td>' +
                                      '</tr>';
            for (i = 0; i < data.length; i++) {
              html += '<tr>' +
                        '<td>' + (i+ 1) + '</td>' +
                        '<td>' + data[i].product_code + '</td>' +
                        '<td>' + data[i].product_name + '</td>' +
                        '<td>' + data[i].description + '</td>' +
                        '<td>' + numberWithCommas(data[i].quantity) + '</td>' +
                        '<td>' + data[i].unit_of_measurement + '</td>' +
                        '<td>₦' + numberWithCommas(data[i].price) + '</td>' +
                        '<td>₦' + numberWithCommas(data[i].total) + '</td>' +
                      '</tr>';
              total += parseFloat(data[i].total);
              quantity += parseInt(data[i].quantity);
            }
            $('#order-details-body').html(html);
            html = '<tr>' +
                    '<th style="text-align:center" colspan="4">Total</th>' +
                    '<th>' + numberWithCommas(quantity) + '</th>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<th>₦' + numberWithCommas(total) + '</th>' +
                  '</tr>';
            $('#order-details-foot').html(html);
            $('#order-details-header').html(order_details_header);
          },
          error: function(){
            alert('Could not get Data from Database');
          }
        });
      }
    });
    //Confirm order approval
    $('#btnApprove').click(function(){
      //validate form
      var approval_note = $('textarea[name=approval-note]');
      var result = '';
      if(approval_note.val()==''){
        approval_note.parent().parent().addClass('has-error');
        Swal.fire({
          type: 'error',
          title:'Oops...',
          text: 'Please leave a message!',
        });
      }else{
        approval_note.parent().parent().removeClass('has-error');
        result += '1';
      }
      if (result == '1') {
        $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>orders/approveOrder',
          data: {order_id: order_id, approval_note: approval_note.val()},
          dataType: 'json',
          success: function(data){
            if(data.success){
              $('#approveModal').modal('hide');
              Swal.fire({
                type: 'success',
                title: 'Order successfully approved',
                showConfirmButton: false,
                timer: 1500
              });
              showAllOrders();
            }else{
              alert('Error');
            }
          },
          error: function(){
            alert('Could not approve order');
          }
        });
      }
    })
    //Cancel order
    $('#order-data').on('click', '.order-cancel', function(){
      if ($(this).attr('id') == 'cancelled') {
        $('.alert-success').html('Order status : Cancelled').fadeIn().delay(1000).fadeOut('slow');
      }
      else{
        order_id = $(this).attr('data');
        $('#deleteModal').modal('show');
      }
    })
    //Confirm order cancellation
    $('#btnDelete').click(function(){
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>orders/cancelOrder',
        data: {order_id: order_id},
        dataType: 'json',
        success: function(data){
          if(data.success){
            Swal.fire({
              type: 'success',
              title:'Cancelled',
              text: 'Order cancelled successfully',
              showConfirmButton: false,
              timer: 1500
            });
            $('#deleteModal').modal('hide');
            showAllOrders();
          }else{
            Swal.fire({
              type: 'error',
              title: 'Oops ...',
              text: 'Could not cancel order!',
              showConfirmButton: false,
              timer: 1500
            });
          }
        },
        error: function(){
          Swal.fire({
            type: 'error',
            title: 'Oops ...',
            text: 'Could not cancel order!!',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
    })
    //Capitalize first letter of string
    function jsUcfirst(string){
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    //Separate numbers with commas
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
    //Filter by status
    $("#view-type").change(function(){
      showAllOrders();
    })
    //clear modal
    $('[data-dismiss=modal]').on('click', function (e) {
        var $t = $(this),
            target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

      $(target)
        .find("input,textarea,select")
           .val('')
           .end()
        .find("input[type=checkbox], input[type=radio]")
           .prop("checked", "")
           .end();
    })
    $('#approveModal').on('hidden.bs.modal', function (e) {
      $(this)
        .find("input,textarea,select")
           .val('')
           .end()
        .find("input[type=checkbox], input[type=radio]")
           .prop("checked", "")
           .end();
    })
  })
</script>
