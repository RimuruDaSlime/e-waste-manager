<!-- Begin Page Content -->
<div class="container-fluid">

  <form class="order-form" action="" method="post">
    <div class="row">
      <!-- Page Heading -->
      <div class="col-md-5">
        <h4 style="text-align:left;"> Place an <b>Order</b></h4>
      </div>
      <div class="col-md-7">
        <button type="button" style="float:right; margin-right:15px;" class="btn btn-success place-order">Submit order</button>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading"><strong>Chosen customer details</strong></div>
          <div class="panel-body">
            <ul class="media-body list-unstyled customer-details">
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading"><strong>Delivery address</strong></div>
          <div class="panel-body">
            <ul class="media-body list-unstyled delivery-address">
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading"><strong> Summary </strong><i class="fas fa-user-check"></i></div>
          <div class="panel-body">
            <ul class="media-body list-unstyled summary">
              <li class="nbItems"></li>
              <li class="order-total"></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <hr>
        <div class="row" style="text-align:center;">
          <div class="col-md-6">
            <h4 style="text-align:left;"> List of <b>Items</b></h4>
          </div>
          <div class="col-md-6">
              <button style="float:right; margin-left:15px" type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</button>
          </div>
        </div>
        <div class="container-fluid">
            <div class="table-wrapper">
                <div class="table-title">
                </div>
                <br>
                <table id="order" style="text-align:center;"  class="table table-bordered table-responsive table-hover table-striped display" style="width:100%;">
                  <thead>
                    <tr>
                      <th>Product code</th>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>U.O.M</th>
                      <th>Price (₦)</th>
                      <th>Amount (₦)</th>
                      <th>In stock</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                   <tr class="tr_input" >
                     <td style="display:none;"><input type='hidden' class='product-id form-control active' id='prodid_1'></td>
                     <td><input required type='text' class='code form-control' id='code_1' name="product-code"></td>
                     <td><input required type='text' class='product form-control' id='product_1' name="product-name" ></td>
                     <td><input required type='text' class='quantity form-control active' id='quantity_1' name="quantity" value="0"></td>
                     <td><input type='text' class='uom form-control' id='uom_1' readonly></td>
                     <td><input required type='text' class='price form-control' id='price_1' name="price" value="0"></td>
                     <td><input type='text' class='amount form-control' id='amount_1' readonly value="0"></td>
                     <td><input required type='text' class='stock form-control' id='stock_1' name="stock" value="0" readonly></td>
                     <td>
                       <button type="button" class="btn btn-danger delete-order">X</button>
                     </td>
                   </tr>
                  </tbody>
                </table>
            </div>
        </div>
      </div>

    </div>
  </form>

</div>
<!-- /.container-fluid -->

<script type="text/javascript">
  $(function(){
    disableRemoveBtn();
    randomPick();
    fillSummary();
    var custid = null;
    var tableOriginal = document.getElementById('order').innerHTML;
    function randomPick(){
      $.ajax({
        url: "<?php echo base_url('customers/getCurrentCustomer') ?>",
        type: 'post',
        dataType: "json",
        success: function(data) {
          custid = data[0].customer_id;
          var html = '<li>'+data[0].company_name+'</li>' +
                     '<li>'+data[0].first_name + ' ' + data[0].last_name +'</li>' +
                     '<li>'+data[0].email+'</li>' +
                     '<li>'+data[0].phone+'</li>';
          $('.customer-details').html(html);
          var address = data[0].address.split(',');
          var html = '<li>'+address[0]+'</li>' +
                     '<li>'+address[1]+'</li>' +
                     '<li>'+address[2]+'</li>' +
                     '<li>'+address[3]+'</li>';
          $('.delivery-address').html(html);
        }
      });
    }
    // Append table with add row form on add new button click
    $(".add-new").click(function(){
      // Get last id
      var lastname_id = $('.tr_input input[type=text]:nth-child(1)').last().attr('id');
      var split_id = lastname_id.split('_');

      // New index
      var index = Number(split_id[1]) + 1;
      var row = '<tr class="tr_input">' +
                  "<td style='display:none;'><input type='hidden' class='product-id form-control active' id='prodid_"+index+"'></td>" +
                  "<td><input required type='text' class='code form-control' id='code_"+index+"' name='product-code'></td>" +
                  "<td><input type='text' class='product form-control' id='product_"+index+"' name='product-name' ></td>" +
                  "<td><input required type='text' class='quantity form-control' id='quantity_"+index+"' value='0' name='quantity'></td>" +
                  "<td><input type='text' class='uom form-control' id='uom_"+index+"' readonly></td>" +
                  "<td><input type='text' class='price form-control' id='price_"+index+"' value='0'></td>" +
                  "<td><input type='text' class='amount form-control' id='amount_"+index+"' value='0' readonly></td>" +
                  "<td><input required type='text' class='stock form-control' id='stock_"+index+"' name='stock' value='0' readonly></td>" +
                  "<td>" +
                  "<button type='button' class='btn btn-danger delete-order'>X</button>" +
                  "</td>" +
                "</tr>" ;
      $("#order tbody").append(row);
      disableRemoveBtn();
      fillSummary();
    });
    //fill summary
    function fillSummary(){
      var amount = 0;
      var numberOfItems = 0;
      $("tr.tr_input").each(function() {
        var product = $(this).find("input.product").val();
        var quantity = $(this).find("input.quantity").val();
        var uom = $(this).find("input.uom").val();
        if (product != '' && quantity != '' && quantity != 0) {
          amount += parseFloat($(this).find("input.amount").val().substring(1).replace(/,/g, ''));
          numberOfItems += parseInt(1);
        }
      });
      $(".nbItems").text('Number of items :' + numberWithCommas(numberOfItems));
      $(".order-total").text('Total amount  : ₦' + numberWithCommas(amount));
    }
    //Delete row on delete button click
    $(document).on("click", ".delete-order", function(){
      $(this).parents("tr").remove();
      disableRemoveBtn();
      fillSummary();
    });
    //Auto complete for product code
    $(document).on('keydown', '.code', function() {

      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      // Initialize jQuery UI autocomplete
      $( '#'+id ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "<?php echo base_url('products/getAllProducts2') ?>",
            type: 'post',
            dataType: "json",
            data: {
              search: request.term,request:1,id:custid
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        select: function (event, ui) {
        $(this).val(ui.item.label); // display the selected text
        var productid = ui.item.value; // selected value

          // AJAX
          $.ajax({
          url: "<?php echo base_url('products/getAllProducts2') ?>",
          type: 'post',
          data: {productid:productid,request:2},
          dataType: 'json',
            success:function(response){
              var len = response.length;

              if(len > 0){
              var product_id = response[0]['product_id'];
              var productcode = response[0]['product_code'];
              var productname = response[0]['product_name'];
              var stock = response[0]['in_stock'];
              var price = response[0]['price'];
              var uom = response[0]['uom'];

              // Set value to textboxes
              document.getElementById('prodid_'+index).value = product_id;
              document.getElementById('code_'+index).value = productcode;
              document.getElementById('product_'+index).value = productname;
              document.getElementById('stock_'+index).value = numberWithCommas(stock);
              document.getElementById('price_'+index).value = numberWithCommas(price);
              document.getElementById('uom_'+index).value = uom;
              }
            }
          });

          return false;
        }
      });
    });
    //Auto complete for product name
    $(document).on('keydown', '.product', function() {
      console.log(custid);
      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      // Initialize jQuery UI autocomplete
      $( '#'+id ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "<?php echo base_url('products/getAllProducts2') ?>",
            type: 'post',
            dataType: "json",
            data: {
              search: request.term,request:3,id:custid
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        select: function (event, ui) {
        $(this).val(ui.item.label); // display the selected text
        var productid = ui.item.value; // selected value

          // AJAX
          $.ajax({
          url: "<?php echo base_url('products/getAllProducts2') ?>",
          type: 'post',
          data: {productid:productid,request:2},
          dataType: 'json',
            success:function(response){
              var len = response.length;

              if(len > 0){
              var product_id = response[0]['product_id'];
              var productcode = response[0]['product_code'];
              var productname = response[0]['product_name'];
              var price = response[0]['price'];
              var uom = response[0]['uom'];
              var stock = response[0]['in_stock'];

              // Set value to textboxes
              document.getElementById('prodid_'+index).value = product_id;
              document.getElementById('code_'+index).value = productcode;
              document.getElementById('product_'+index).value = productname;
              document.getElementById('price_'+index).value = numberWithCommas(price);
              document.getElementById('stock_'+index).value = numberWithCommas(stock);
              document.getElementById('uom_'+index).value = uom;
              }
            }
          });

          return false;
        }
      });
    });
    //Place order
    $(".place-order").click(function(){
      var res = '1';
      $("tr.tr_input").each(function() {
        var code = $(this).find("input.code");
        var product = $(this).find("input.product");
        var quantity = $(this).find("input.quantity");
        var price = $(this).find("input.price");
        var stock = $(this).find("input.stock");
        //Validate form
        //Product name
        if (product.val() == '') {
          product.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else{
          product.parent().removeClass('has-error');
        }
        //Product code
        if (code.val() == '') {
          code.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else{
          code.parent().removeClass('has-error');
        }
        //Quantity
        if (quantity.val() == '') {
          quantity.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else if (quantity.val() == 0) {
          quantity.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'Product quantity is 0!',
          });
        }
        else{
          quantity.parent().removeClass('has-error');
        }
        //Price
        if (price.val() == '') {
          price.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else if (price.val() == 0) {
          price.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'Price is 0!',
          });
        }
        else{
          price.parent().removeClass('has-error');
        }
        //Stock
        if (stock.val() == '') {
          stock.parent().parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else{
          stock.parent().parent().removeClass('has-error');
        }
      });

      if (res == '1') {
        var data = [];
        $("tr.tr_input").each(function() {
          var prodid = $(this).find("input.product-id").val();
          var qty = parseInt($(this).find("input.quantity").val().replace(/,/g, ''));
          var price = parseInt($(this).find("input.price").val().replace(/,/g, ''));
          data.push({'prodid':prodid, 'qty': qty, 'price':price});
        });
        result = [];
        data.forEach(function (a) {
        if (!this[a.prodid]) {
          this[a.prodid] = { prodid: a.prodid, qty: 0, price: a.price };
          result.push(this[a.prodid]);
        }
        this[a.prodid].qty += parseInt(a.qty);
        }, Object.create(null));
        $.ajax({
          type: 'ajax',
          method: 'post',
          url: "<?php echo base_url('orders/placeOrder') ?>",
          data: {custid:custid,data:result},
          dataType: 'json',
          success: function(response){
            if(response.success){
              randomPick();
              document.getElementById('order').innerHTML = tableOriginal;
              $(".nbItems").text('Number of items :0');
              $(".order-total").text('Total amount  : ₦0');
              Swal.fire({
                type: 'success',
                title: 'Your order has been submitted',
                showConfirmButton: false,
                timer: 2000
              });
            }else{
              randomPick();
              document.getElementById('order').innerHTML = tableOriginal;
              Swal.fire({
                type: 'error',
                title: 'Error in submitting order',
                showConfirmButton: false,
                timer: 1500
              });
            }
          },
          error: function(){
            randomPick();
            document.getElementById('order').innerHTML = tableOriginal;
            Swal.fire({
              type: 'error',
              title: 'Could not submit order',
              showConfirmButton: false,
              timer: 1500
            });
          }
        });
      }
    });
    //Check number of rows and disable button
    function disableRemoveBtn(){
      var count = $('#order tr').length;
      if (count <= 2) {
        $(".delete-order").attr("disabled", true);
      }
      else{
        $(".delete-order").attr("disabled", false);
      }
    }
    //Calculate amount
    $(document).on('change', '.quantity', function(){
      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      // Set value to textboxes
      var price = document.getElementById('price_'+index).value.replace(/,/g, '');
      var qty = document.getElementById('quantity_'+index).value.replace(/,/g, '');
      var amount = ((parseFloat(price) * parseInt(qty)));
      document.getElementById('amount_'+index).value = '₦' + numberWithCommas(amount);
      fillSummary();

    });
    $(document).on('change', '.price', function(){
      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      // Set value to textboxes
      var price = document.getElementById('price_'+index).value.replace(/,/g, '');
      var qty = document.getElementById('quantity_'+index).value.replace(/,/g, '');
      var amount = ((parseFloat(price) * parseInt(qty)));
      document.getElementById('amount_'+index).value = '₦' + numberWithCommas(amount);
      fillSummary();
    });
    //Separate numbers with commas
    function numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }
    //separate with commas automatically
    $(document).on('keyup', "input.quantity", function(event){
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;
      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });
    $(document).on('keyup', "input.price", function(event){
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;
      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });
  })
</script>
