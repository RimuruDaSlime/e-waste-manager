<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Orders</h1>
  <p class="mb-4">To place a new product, click on this link: <a href=<?php echo base_url("sales_orders/place_order"); ?>>Place an order</a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of products</h6>
    </div>

    <div class="card-body">

      <div id="reportrange" class="rounded" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 23%">
          <i class="fa fa-calendar"></i>&nbsp;
          <span></span> <i class="fa fa-caret-down"></i>
      </div>

      <br>

      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align:center;">
          <thead>
            <tr>
              <th>Order N°</th>
              <th>Company name</th>
              <th>Contact person</th>
              <th>Ordered by</th>
              <th>Ordered at</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="order-data">
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="detailsModal" style="text-align:center;" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Order Details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <table id="order-details" class="table table-bordered" style="margin-top: 20px;">
              <thead>
                <thead id="order-details-header">
                </thead>
                <tr>
                  <th style="text-align:center"> N°</th>
                  <th style="text-align:center"> Product code</th>
                  <th style="text-align:center"> Product</th>
                  <th style="text-align:center"> Description</th>
                  <th style="text-align:center"> Quantity</th>
                  <th style="text-align:center"> Unit</th>
                  <th style="text-align:center"> Price</th>
                  <th style="text-align:center"> Amount</th>
                </tr>
              </thead>
              <tbody id="order-details-body">
              </tbody>
              <tfoot id="order-details-foot">
              </tfoot>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>
<script type="text/javascript">
  $(function(){
    showAllOrders();
    initDateRangePicker();
    //Show all orders function
    function showAllOrders(from, to){
      $('#dataTable').DataTable({
        "destroy": true,
        "ajax"   : {
          "method" : "POST",
          "data"    : {"from": from, "to": to},
          "dataType": 'json',
          "url"    : '<?php echo base_url() ?>orders/getAllOrders',
          "dataSrc": function (data) {
            var return_data = new Array();
            for(var i=0;i< data.length; i++){
              return_data.push({
                'order_code'   : data[i].order_code,
                'order_type'   : data[i].order_type,
                'ordered_at'   : data[i].order_date_time,
                'customer_name': data[i].customer_name,
                'status'       : data[i].status,
                'order_id'     : data[i].order_id,
                'company_name' : data[i].company_name,
                'ordered_by'   : data[i].ordered_by
            })
          }
          return return_data;
        }
      },
        "columns"    : [
          {'data': 'order_code'},
          {'data': 'company_name'},
          {'data': 'customer_name'},
          {'data': 'ordered_by'},
          {'data': 'ordered_at'},
          {'data': 'status'},
          {
            data: null,
            render: function ( data, type, row ) {
              return '<a href="javascript:;" style="margin:5px;" class="btn btn-info order-details" data="'+data.order_id+'">Details</a>';
            }
          }
        ]
      });
    }
    //Show order details
    $('#order-data').on('click', '.order-details', function(){
      var order_id = $(this).attr('data');
      $('#detailsModal').modal('show');
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>orders/getOrderDetailsById',
        data: {order_id: order_id},
        dataType: 'json',
        success: function(data){
          var html = '';
          var i;
          var total = 0;
          var quantity = 0;
          var order_id_label = ''
          var order_details_header = '<tr>' +
                                      '<th style="text-align:center" colspan="3"> Order ID : </th>' +
                                      '<td style="text-align:center" colspan="5"> ' + data[0].order_code + ' </td>' +
                                    '</tr>';
          for (i = 0; i < data.length; i++) {
            html += '<tr>' +
                      '<td>' + (i+ 1) + '</td>' +
                      '<td>' + data[i].product_code + '</td>' +
                      '<td>' + data[i].product_name + '</td>' +
                      '<td>' + data[i].description + '</td>' +
                      '<td>' + numberWithCommas(data[i].quantity) + '</td>' +
                      '<td>' + data[i].unit_of_measurement + '</td>' +
                      '<td>₦' + numberWithCommas(data[i].price) + '</td>' +
                      '<td>₦' + numberWithCommas(data[i].total) + '</td>' +
                    '</tr>';
            total += parseFloat(data[i].total);
            quantity += parseInt(data[i].quantity);
          }
          $('#order-details-body').html(html);
          html = '<tr>' +
                  '<th style="text-align:center" colspan="4">Total</th>' +
                  '<th>' + numberWithCommas(quantity) + '</th>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<th>₦' + numberWithCommas(total) + '</th>' +
                '</tr>';
          $('#order-details-foot').html(html);
          $('#order-details-header').html(order_details_header);
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
    });
    //Separate numbers with commas
    function numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }
    //Get products
    function getProductDetails(){
      var html = '';
      $.ajax({
        type: 'ajax',
        method: 'post',
        url: '<?php echo base_url() ?>products/getAllProducts',
        dataType: 'json',
        success: function(data){
          var i;
          for (i = 0; i < data.length; i++) {
            html += '<option value="'+data[i].product_id+'">'+data[i].product_name+'</option>';
          }
          $('#product-id-add').html(html);
          $('#product-id-add').selectpicker('refresh');
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
    }
    //Get customers
    function getCustomerDetails(){
      var html = '';
      $.ajax({
        type: 'ajax',
        method: 'post',
        url: '<?php echo base_url() ?>customers/getAllCustomers',
        dataType: 'json',
        success: function(data){
          var i;
          for (i = 0; i < data.length; i++) {
            html += '<option data-subtext="ID:'+data[i].last_name+data[i].customer_id+'" value="'+data[i].customer_id+'">'+data[i].first_name + ' ' + data[i].last_name +'</option>';
          }
          $('#customer-id').html(html);
          $('#customer-id').selectpicker('refresh');
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
    }
    //daterangepicker
    function initDateRangePicker(){
      var start = moment().subtract(29, 'days');
      var end = moment();

      function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today'        : [moment(), moment()],
           'Yesterday'    : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days'  : [moment().subtract(6, 'days'), moment()],
           'Last 30 Days' : [moment().subtract(29, 'days'), moment()],
           'This Month'   : [moment().startOf('month'), moment().endOf('month')],
           'Last Month'   : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      }, cb);

      cb(start, end);
    }
    //filter table by date
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      var from = picker.startDate.format('YYYY-MM-DD');
      var to = picker.endDate.format('YYYY-MM-DD');
      showAllOrders(from, to);
    });
  })
</script>
