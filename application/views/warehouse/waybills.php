<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Waybills</h1>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of deliveries</h6>
    </div>
    <br>
    <div class="card-body">

      <div id="reportrange" class="rounded" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 23%">
          <i class="fa fa-calendar"></i>&nbsp;
          <span></span> <i class="fa fa-caret-down"></i>
      </div>
      <br>

      <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" style="text-align: center;" cellspacing="0">
          <thead>
            <tr>
              <th>Waybill code</th>
              <th>Description</th>
              <th>Amount</th>
              <th>Status</th>
              <th>Approved by</th>
              <th>Approve date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="waybill-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- Waybill template -->
  <div class="container invoice" id="printJS-form">
    <h1 style="text-align:center">Waybill <small>Details</small></h1>
    <div class="invoice-header">
      <div class="row">

        <div class="col-md-6">
          <div class="row">
            <div class="col">
              <h4 class="text-muted waybill-id">NO: 554775/R1</h4>
            </div>
            <div class="col">
              <h4 class="text-muted waybill-date text-right">Date of issue: 01/01/2015</h4>
            </div>
          </div>
        </div>

        <div class="col-md-6 text-right">
          <div class="media">
            <ul class="media-body list-unstyled">
              <li><strong>Tech Company</strong></li>
              <li>Technology</li>
              <li>info@techno.com</li>
            </ul>
            <div class="media-right">
              <img class="media-object logo" src="https://dummyimage.com/70x70/000/fff&text=TECH" />
            </div>
          </div>
        </div>

      </div>

    </div>
    <div class="invoice-body">
      <div class="row">
        <div class="col-md">
          <div class="row">

            <div class="col">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="card-header py-3">
                    <h3 class="m-0 font-weight-bold">Origin</h3>
                  </div>
                </div>
                <div class="panel-body">
                  <dl class="dl-horizontal">
                    <dt>Name</dt>
                    <dd>TECH</dd>
                    <dt>Industry</dt>
                    <dd>Software Development</dd>
                    <dt>Address</dt>
                    <dd>Field 3, Moon</dd>
                    <dt>Email</dt>
                    <dd>mainl@acme.com</dd>
                </div>
              </div>
            </div>

            <div class="col">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="card-header py-3">
                    <h3 class="m-0 font-weight-bold">Destination</h3>
                  </div>
                </div>
                <div class="panel-body">
                  <dl class="dl-horizontal customer-details">
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Products</h3>
        </div>
        <table class="table table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center colfix">N°</th>
              <th class="text-center colfix">Product code</th>
              <th class="text-center colfix">Product</th>
              <th class="text-center colfix">Category</th>
              <th class="text-center colfix">Quantity</th>
              <th class="text-center colfix">Unit</th>
            </tr>
          </thead>
          <tbody class="waybill-product-details">
          </tbody>
          <tfoot>
            <tr>
              <th class="text-center" colspan="4">Total</th>
              <th class="text-center rowtotal mono waybill-total"></th>
              <th></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <div class="invoice-footer">
      Thank you for choosing our products.
      <br/> We hope to see you again soon
      <br/>
      <strong>~EUROMEGA~</strong>
    </div>
  </div>
  <!-- End of Waybill template -->

</div>

<style media="screen">
@media screen {
  #printJS-form { display: none; }
}
@media print {
  #printJS-form { display: block !important; }
}
</style>

<script type="text/javascript">
  $(function(){
    var waybill_id = null;
    initDateRangePicker();
    showAllWayBills();
    function showAllWayBills(from, to){
      $('#dataTable').DataTable({
        "destroy": true,
        "ajax"   : {
          "method"  : 'post',
          "data"    : {"from": from, "to": to},
          "dataType": 'json',
          "url"     : '<?php echo base_url("waybills/getAllWayBills") ?>',
          "dataSrc" : function (data) {
            var return_data = new Array();
            for(var i=0;i< data.length; i++){
              return_data.push({
                'waybill_code'   : data[i].waybill_code,
                'description'    : data[i].description,
                'amount'         : '₦' + numberWithCommas(data[i].amount),
                'status'         : data[i].status,
                'staff_name'     : data[i].staff_name,
                'approve_date'   : data[i].approve_date_time,
                'waybill_id'     : data[i].waybill_id
              })
            }
            return return_data;
          }
        },
          "columns"    : [
            {'data': 'waybill_code'},
            {'data': 'description'},
            {'data': 'amount'},
            {'data': 'status'},
            {'data': 'staff_name'},
            {'data': 'approve_date'},
            {
              data: null,
              render: function ( data, type, row ) {
                if (data.status == 'approved' || data.status == 'cancelled') {
                  return '<a href="javascript:;" style="margin:5px;" class="btn btn-info waybill-print" data="'+data.waybill_id+'">Print</a>';
                }
                else{
                  return '<a href="javascript:;" style="margin:5px;" class="btn btn-success waybill-approve" data="'+data.waybill_id+'">Approve</a>';
                }
              }
            }
          ]
      });
    }
    //Confirm waybill approval
    $('#waybill-data').on('click', '.waybill-approve', function(){
      var waybill_id = $(this).attr('data');
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>waybills/approveWaybill',
        data: {waybill_id: waybill_id},
        dataType: 'json',
        success: function(data){
          if(data.success){
            $('#approveModal').modal('hide');
            Swal.fire({
              type: 'success',
              title: 'Waybill successfully approved',
              showConfirmButton: false,
              timer: 1500
            });
            //printWaybill(waybill_id);
            showAllWayBills();
          }else{
            alert('Error');
          }
        },
        error: function(){
          alert('Could not approve waybill');
        }
      });
    })
    //Print
    $('#waybill-data').on('click', '.waybill-print', function(){
      var waybill_id = $(this).attr('data');
      printWaybill(waybill_id);
    });
    //print function
    function printWaybill(id){
      $.ajax({
        type: 'ajax',
        method: 'post',
        url: '<?php echo base_url() ?>waybills/getWayBillDetailsById',
        data: {waybill_id: id},
        dataType: 'json',
        success: function(data){
          $(".waybill-id").text('Reference N°: '+ data.customer[0].waybill_code);
          $(".waybill-date").text('Date of issue: '+data.customer[0].approve_date);
          var html = '<dt>Name</dt>' +
                     '<dd>'+data.customer[0].customer_name+'</dd>' +
                     '<dt>Address</dt>' +
                     '<dd>'+data.customer[0].address+'</dd>' +
                     '<dt>Phone</dt>' +
                     '<dd>'+data.customer[0].phone+'</dd>' +
                     '<dt>Email</dt>' +
                     '<dd>'+data.customer[0].email+'</dd>';
          $(".customer-details").html(html);
          html = '';
          var total = 0;
          for (var i = 0; i < data.product.length; i++) {
            html += '<tr>'+
                      '<td class="text-center">'+
                        '<span class="mono">'+(i + 1)+'</span>'+
                      '</td>'+
                      '<td class="text-center">'+
                        '<span class="mono">'+data.product[i].product_code+'</span>'+
                      '</td>'+
                      '<td class="text-center">'+
                        '<span class="mono">'+jsUcfirst(data.product[i].product_name)+'</span>'+
                      '</td>'+
                      '<td>'+
                        jsUcfirst(data.product[i].category_name)+
                        '<br>'+
                        '<small class="text-muted">'+data.product[i].description+'</small>'+
                      '</td>'+
                      '<td class="text-center">'+
                        '<span class="mono">'+numberWithCommas(data.product[i].quantity)+'</span>'+
                      '</td>'+
                      '<td class="text-center">'+
                        '<span class="mono">'+data.product[i].unit_of_measurement+'</span>'+
                      '</td>'+
                    '</tr>';
            total += parseInt(data.product[i].quantity);
          }
          $(".waybill-product-details").html(html);
          $(".waybill-total").text(numberWithCommas(total));
          $('#printJS-form').printThis();
        },
        error: function(){
          alert('Could not get Data from Database');
        },
      });
    }
    //Separate numbers with commas
    function numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }
    //Capitalize first letter of string
    function jsUcfirst(string){
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    //daterangepicker
    function initDateRangePicker(){
      var start = moment().subtract(29, 'days');
      var end = moment();

      function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today'        : [moment(), moment()],
           'Yesterday'    : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days'  : [moment().subtract(6, 'days'), moment()],
           'Last 30 Days' : [moment().subtract(29, 'days'), moment()],
           'This Month'   : [moment().startOf('month'), moment().endOf('month')],
           'Last Month'   : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      }, cb);

      cb(start, end);
    }
    //filter table by date
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      var from = picker.startDate.format('YYYY-MM-DD');
      var to = picker.endDate.format('YYYY-MM-DD');
      showAllWayBills(from, to);
    });
    //separate with commas
    function numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }
  })
</script>
