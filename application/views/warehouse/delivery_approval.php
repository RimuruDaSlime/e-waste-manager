<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Delivery approval</h1>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of deliveries</h6>
    </div>
    <br>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" style="text-align: center;" cellspacing="0">
          <thead>
            <tr>
              <th>Delivery code</th>
              <th>Customer name</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="delivery-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="approveModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm approval</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" id="wrap">
          <table id="delivery-details" class="table table-bordered" style="text-align:center;">
            <thead>
              <tr>
                <th style="text-align:center">N°</th>
                <th style="text-align:center">Product code</th>
                <th style="text-align:center">Product name</th>
                <th style="text-align:center">Description</th>
                <th style="text-align:center">Quantity ordered</th>
                <th style="text-align:center">Unit</th>
                <th style="text-align:center">In stock</th>
                <th style="text-align:center">Suggested quantity</th>
              </tr>
            </thead>
            <tbody id="delivery-details-body">
            </tbody>
            <tfoot id="delivery-details-foot">
            </tfoot>
          </table>
            Do you want to approve this delivery?
            <form class="form-horizontal" action="" method="post" id="form-approval">
              <div class="form-group">
                <div class="container-fluid">
                  <textarea class="form-control" name="approval-note" rows="2" id="approval-note" required></textarea>
                </div>
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnApprove" class="btn btn-success">Approve</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm Cancellation</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            Do you want to cancel this delivery?
        </div>
        <div class="modal-footer">
          <button type="button" id="btnDelete" class="btn btn-danger">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>
<script type="text/javascript">
  $(function(){
    var list_id = {};
    var delivery_id = null;
    showAllDeliveries();
    //Show all orders function
    function showAllDeliveries(){
      $('#dataTable').DataTable({
          "destroy": true,
          "ajax"   : {
          "url"    : '<?php echo base_url("deliveries/getAllDeliveriesForApproval") ?>',
          "dataSrc": function (data) {
            var return_data = new Array();
            for(var i=0;i< data.length; i++){
              return_data.push({
                'customer_name'   : data[i].customer_name,
                'status'          : data[i].status,
                'delivery_id'     : data[i].delivery_id,
                'staff_name'      : data[i].staff_name,
                'update_date_time':data[i].update_date_time,
                'delivery_code'   :data[i].delivery_code
              })
            }
            return return_data;
          }
        },
          "columns": [
            {'data': 'delivery_code'},
            {'data': 'customer_name'},
            {'data': 'status'},
            {
              data: null,
              render: function ( data, type, row ) {
                if (data.status == 'approved') {
                  return '<a href="javascript:;" style="margin:5px;" id="approved" class="btn btn-success delivery-approve" data="'+data.delivery_id+'" disabled>Approve</a>' +
                         '<a href="javascript:;" style="margin:5px;" class="btn btn-danger delivery-cancel" data="'+data.delivery_id+'">Cancel</a>';
                }
                else if(data.status == 'cancelled'){
                  return '<a href="javascript:;" style="margin:5px;" id="cancelled" class="btn btn-success delivery-approve" data="'+data.delivery_id+'" disabled>Approve</a>' +
                         '<a href="javascript:;" style="margin:5px;" id="cancelled" class="btn btn-danger delivery-cancel" data="'+data.delivery_id+'" disabled>Cancel</a>';
                }
                else{
                  return '<a href="javascript:;" style="margin:5px;" class="btn btn-success delivery-approve" data="'+data.delivery_id+'">Approve</a>' +
                         '<a href="javascript:;" style="margin:5px;" class="btn btn-danger delivery-cancel" data="'+data.delivery_id+'">Cancel</a>';
                }
              }
            }
          ]
      });
    }
    //Approve a delivery
    $('#delivery-data').on('click', '.delivery-approve', function(){
      if ($(this).attr('id') == 'approved' || $(this).attr('id') == 'cancelled') {
        //do nothing...
      }
      else{
        delivery_id = $(this).attr('data');
        $('#approveModal').modal('show');
        $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>deliveries/getDeliveryDetailsById',
          data: {delivery_id: delivery_id},
          dataType: 'json',
          success: function(data){
            var html = '';
            var i;
            var total = 0;
            var total_suggestion = 0;
            var status = data[0].status;
            if (status == 'approved') {
              $("#btnSave").attr("disabled", true);
            }
            else{
              $("#btnSave").attr("disabled", false);
            }
            for (i = 0; i < data.length; i++) {
              var suggested = ((data[i].in_stock - data[i].quantity) > 0) ? data[i].quantity : (data[i].quantity - data[i].in_stock );
              html += '<tr class="tr_input">' +
                        '<td>' + (i+ 1) + '</td>' +
                        '<td>' + data[i].product_code + '</td>' +
                        '<td>' + data[i].product_name + '</td>' +
                        '<td>' + data[i].description + '</td>' +
                        '<td name="quantity" class="qty_class">' + numberWithCommas(data[i].quantity) + '</td>' +
                        '<td>' + data[i].unit_of_measurement + '</td>' +
                        '<td>' + numberWithCommas(data[i].in_stock) + '</td>' +
                        '<td><input type="text" name="'+data[i].product_name+'" style="text-align:center;" id="'+data[i].product_id+'" class="form-control qty" value="'+numberWithCommas(suggested)+'" placeholder="'+numberWithCommas(suggested)+'"></td>' +
                      '</tr>';
              total += parseInt(data[i].quantity);
              total_suggestion += parseInt(suggested);
              list_id[data[i].product_id] = data[i].product_name;
            }
            $('#delivery-details-body').html(html);
            html = '<tr>' +
                    '<th style="text-align:right" colspan="4">Total</th>' +
                    '<th>' + numberWithCommas(total) + '</th>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<th><input type="text" id="totalQuantity" style="text-align:center;" value="'+numberWithCommas(total_suggestion)+'" class="form-control" readonly/></th>' +
                  '</tr>';
            $('#delivery-details-foot').html(html);
          },
          error: function(){
            alert('Could not get Data from Database');
          }
        });
      }
    });
    //Confirm approval
    $('#btnApprove').click(function(){
      //validate form
      var approval_note = $('textarea[name=approval-note]');
      var result = false;
      var note = '';
      if(approval_note.val()==''){
        approval_note.parent().addClass('has-error');
      }else{
        approval_note.parent().removeClass('has-error');
        result = true;
        note = approval_note.val();
      }
      $("tr.tr_input").each(function() {
        var delivery_qty = $(this).find("td.qty_class").text();
        var suggested_qty = $(this).find("input.qty");
        if ((parseInt(suggested_qty.val().replace(/,/g, '')) <= parseInt(delivery_qty.replace(/,/g, '')))) {
          suggested_qty.parent().removeClass('has-error');
        }
        else{
          suggested_qty.parent().addClass('has-error');
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Invalid quantity!',
          });
          result = false;
        }
      });
      if (result) {
        var res = true;
        for (var k in list_id){
            if (list_id.hasOwnProperty(k)) {
                 var input = $('input[name='+list_id[k]+']');
                 list_id[k] = parseInt(input.val().replace(/,/g, ''));
                 if (input.val() == '') {
                   res = false;
                 }
            }
        }
        if (res) {
          $.ajax({
            type: 'ajax',
            method: 'post',
            url: '<?php echo base_url() ?>deliveries/saveDeliveryDetails',
            data: {id: delivery_id, data: list_id, note: note},
            dataType: 'json',
            success: function(data){
              list_id = {};
              if(data.success){
                $('#approveModal').modal('hide');
                Swal.fire({
                  type: 'success',
                  title: 'Delivery successfully approved',
                  showConfirmButton: false,
                  timer: 1500
                });
                showAllDeliveries();
              }else{
                alert('Error');
              }
            },
            error: function(){
              alert('Could not approve delivery');
            }
          });
          $("#approveModal")
            .find("input,textarea,select")
               .val('')
               .end()
            .find("input[type=checkbox], input[type=radio]")
               .prop("checked", "")
               .end();
        }
      }
    });
    //Cancel delivery
    $('#delivery-data').on('click', '.delivery-cancel', function(){
      if ($(this).attr('id') == 'cancelled') {
        $('.alert-success').html('Delivery status : Cancelled').fadeIn().delay(1000).fadeOut('slow');
      }
      else{
        delivery_id = $(this).attr('data');
        $('#deleteModal').modal('show');
      }
    })
    //Confirm delivery cancellation
    $('#btnDelete').click(function(){
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>deliveries/cancelDelivery',
        data: {delivery_id: delivery_id},
        dataType: 'json',
        success: function(data){
          if(data.success){
            $('#deleteModal').modal('hide');
            $('.alert-success').html('Delivery cancelled successfully').fadeIn().delay(1000).fadeOut('slow');
            showAllDeliveries();
          }else{
            alert('Error');
          }
        },
        error: function(){
          alert('Could not cancel delivery');
        }
      });
    })
    //Add commas to numbers
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
    $("#delivery-details").on('input', '.qty', function () {
      var calculated_total_sum = 0;

      $("#delivery-details .qty").each(function (){
        var get_textbox_value = $(this).val().replace(/,/g, '');
        if ($.isNumeric(get_textbox_value)){
          calculated_total_sum += parseInt(get_textbox_value);
        }
      });
      $("#totalQuantity").val(numberWithCommas(calculated_total_sum));
    });
    $(document).on('keyup', "input.qty", function(event){
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;
      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });
    //clear modal
    $('[data-dismiss=modal]').on('click', function (e) {
        var $t = $(this),
            target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

      $(target)
        .find("input,textarea,select")
           .val('')
           .end()
        .find("input[type=checkbox], input[type=radio]")
           .prop("checked", "")
           .end();
    })
  })
</script>
