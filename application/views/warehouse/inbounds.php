<!-- Begin Page Content -->
<div class="container-fluid">

  <form class="inbound-form" action="" method="post">
    <div class="row">
      <!-- Page Heading -->
      <div class="col-md-5">
        <h4 style="text-align:left;"> Inbound <b>Transaction</b></h4>
      </div>
      <div class="col-md-7">
        <button type="button" style="float:right; margin-right:15px;" class="btn btn-success submit-inbound">Submit inbound</button>
      </div>
    </div>
    <hr>
    <div class="row">

      <div class="col-md-8">
        <div class="row" style="text-align:center;">
          <div class="col-md-6">
            <h4 style="text-align:left;"> List of <b>Items</b></h4>
          </div>
          <div class="col-md-6">
              <button style="float:right; margin-left:15px" type="button" class="btn btn-success add-new"><i class="fa fa-plus"></i> Add New</button>
          </div>
        </div>
        <div class="container-fluid">
            <div class="table-wrapper">
                <div class="table-title">
                </div>
                <br>
                <table id="inbound" style="text-align:center;"  class="table table-bordered table-responsive table-hover table-striped display" style="width:100%;">
                  <thead>
                    <tr>
                      <th>Product code</th>
                      <th>Product</th>
                      <th>Quantity</th>
                      <th>In stock</th>
                      <th>U.O.M</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                   <tr class="tr_input" >
                     <td style="display:none;"><input type='hidden' class='product-id form-control active' id='prodid_1'></td>
                     <td><input required type='text' class='code form-control' id='code_1' name="product-code"></td>
                     <td><input required type='text' class='product form-control' id='product_1' name="product-name" ></td>
                     <td><input required type='text' class='quantity form-control active' id='quantity_1' name="quantity" value="0"></td>
                     <td><input required type='text' class='stock form-control' id='stock_1' name="stock" value="0" readonly></td>
                     <td><input type='text' class='uom form-control' id='uom_1' readonly></td>
                     <td>
                       <button type="button" class="btn btn-danger delete-inbound">X</button>
                     </td>
                   </tr>
                  </tbody>
                </table>
            </div>
        </div>
      </div>

      <div class="col-md-4" id="summary">
        <div class="panel panel-info">
          <div class="panel-heading"><strong>Summary</strong></div>
          <div class="panel-body">
            <table class="table table-bordered table-hover table-striped display" >
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Quantity</th>
                  <th>U.O.M</th>
                </tr>
              </thead>
              <tbody class="summary-body">
                <tr>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th class="text-center col-xs-1" colspan="3">Total quantity</th>
                </tr>
                <tr>
                  <th class="text-center rowtotal mono inbound-total" colspan="3">0</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <div class="panel-footer">
            Verify your inbound
          </div>
        </div>
      </div>

    </div>
  </form>

</div>
<!-- /.container-fluid -->

<script type="text/javascript">
  $(function(){
    //disableRemoveBtn();
    //fillSummary();
    var tableOriginal = document.getElementById('inbound').innerHTML;
    // Append table with add row form on add new button click
    $(".add-new").click(function(){
      // Get last id
      var lastname_id = $('.tr_input input[type=text]:nth-child(1)').last().attr('id');
      var split_id = lastname_id.split('_');

      // New index
      var index = Number(split_id[1]) + 1;
      var row = '<tr class="tr_input">' +
                  "<td style='display:none;'><input type='hidden' class='product-id form-control active' id='prodid_"+index+"'></td>" +
                  "<td><input required type='text' class='code form-control' id='code_"+index+"' name='product-code'></td>" +
                  "<td><input type='text' class='product form-control' id='product_"+index+"' name='product-name' ></td>" +
                  "<td><input required type='text' class='quantity form-control' id='quantity_"+index+"' value='0' name='quantity'></td>" +
                  "<td><input required type='text' class='stock form-control' id='stock_"+index+"' name='stock' value='0' readonly></td>" +
                  "<td><input type='text' class='uom form-control' id='uom_"+index+"' readonly></td>" +
                  "<td>" +
                  "<button type='button' class='btn btn-danger delete-inbound'>X</button>" +
                  "</td>" +
                "</tr>" ;
      $("#inbound tbody").append(row);
      disableRemoveBtn();
      fillSummary();
    });
    //fill summary
    function fillSummary(){
      var total_qty = 0;
      var html = '';
      $("tr.tr_input").each(function() {
        var product = $(this).find("input.product").val();
        var quantity = $(this).find("input.quantity").val();
        var uom = $(this).find("input.uom").val();
        if (product != '' && quantity != '' && quantity != 0) {
          html += '<tr>' +
                    '<td>'+product+'</td>' +
                    '<td>'+quantity+'</td>' +
                    '<td>'+uom+'</td>' +
                  '</tr>';
          total_qty += parseFloat(quantity.replace(/,/g, ''));
        }
      });
      $(".summary-body").html(html);
      $(".inbound-total").text(numberWithCommas(total_qty));
    }
    //Delete row on delete button click
    $(document).on("click", ".delete-inbound", function(){
      $(this).parents("tr").remove();
      disableRemoveBtn();
      fillSummary();
    });
    //Auto complete for products
    $(document).on('keydown', '.code', function() {

      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      // Initialize jQuery UI autocomplete
      $( '#'+id ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "<?php echo base_url('products/getAllProducts2') ?>",
            type: 'post',
            dataType: "json",
            data: {
              search: request.term,request:1
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        select: function (event, ui) {
        $(this).val(ui.item.label); // display the selected text
        var productid = ui.item.value; // selected value

          // AJAX
          $.ajax({
          url: "<?php echo base_url('products/getAllProducts2') ?>",
          type: 'post',
          data: {productid:productid,request:2},
          dataType: 'json',
            success:function(response){
              var len = response.length;

              if(len > 0){
              var product_id = response[0]['product_id'];
              var productcode = response[0]['product_code'];
              var productname = response[0]['product_name'];
              var stock = response[0]['in_stock'];
              var price = response[0]['price'];
              var uom = response[0]['uom'];

              // Set value to textboxes
              document.getElementById('prodid_'+index).value = product_id;
              document.getElementById('code_'+index).value = productcode;
              document.getElementById('product_'+index).value = productname;
              document.getElementById('stock_'+index).value = numberWithCommas(stock);
              document.getElementById('uom_'+index).value = uom;
              }
            }
          });

          return false;
        }
      });
    });
    $(document).on('keydown', '.product', function() {

      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      // Initialize jQuery UI autocomplete
      $( '#'+id ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "<?php echo base_url('products/getAllProducts2') ?>",
            type: 'post',
            dataType: "json",
            data: {
              search: request.term,request:3
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        select: function (event, ui) {
        $(this).val(ui.item.label); // display the selected text
        var productid = ui.item.value; // selected value

          // AJAX
          $.ajax({
          url: "<?php echo base_url('products/getAllProducts2') ?>",
          type: 'post',
          data: {productid:productid,request:2},
          dataType: 'json',
            success:function(response){
              var len = response.length;

              if(len > 0){
              var product_id = response[0]['product_id'];
              var productcode = response[0]['product_code'];
              var productname = response[0]['product_name'];
              var stock = response[0]['in_stock'];
              var uom = response[0]['uom'];

              // Set value to textboxes
              document.getElementById('prodid_'+index).value = product_id;
              document.getElementById('code_'+index).value = productcode;
              document.getElementById('product_'+index).value = productname;
              document.getElementById('stock_'+index).value = numberWithCommas(stock);
              document.getElementById('uom_'+index).value = uom;
              }
            }
          });

          return false;
        }
      });
    });
    //Place inbound
    $(".submit-inbound").click(function(){
      var res = '1';
      $("tr.tr_input").each(function() {
        var code = $(this).find("input.code");
        var product = $(this).find("input.product");
        var quantity = $(this).find("input.quantity");
        var price = $(this).find("input.price");
        //Validate form
        //Product name
        if (product.val() == '') {
          product.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else{
          product.parent().removeClass('has-error');
        }
        //Product code
        if (code.val() == '') {
          code.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else{
          code.parent().removeClass('has-error');
        }
        //Quantity
        if (quantity.val() == '') {
          quantity.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Missing field required!',
          });
        }
        else if (quantity.val() == 0) {
          quantity.parent().addClass('has-error');
          res = '0';
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'Product quantity is 0!',
          });
        }
        else{
          quantity.parent().removeClass('has-error');
        }
      });

      if (res == '1') {
        var data = [];
        $("tr.tr_input").each(function() {
          var prodid = $(this).find("input.product-id").val();
          var qty = parseInt($(this).find("input.quantity").val().replace(/,/g, ''));
          data.push({'prodid':prodid, 'qty': qty});
        });
        result = [];
        data.forEach(function (a) {
        if (!this[a.prodid]) {
          this[a.prodid] = { prodid: a.prodid, qty: 0};
          result.push(this[a.prodid]);
        }
        this[a.prodid].qty += parseInt(a.qty);
        }, Object.create(null));
        var staffid = '<?php echo $this->session->userdata('staff_id'); ?>';
        $.ajax({
          type: 'ajax',
          method: 'post',
          url: "<?php echo base_url('products/submitInbound') ?>",
          data: {staffid:staffid,data:result},
          dataType: 'json',
          success: function(response){
            $(".summary-body").html('');
            $(".inbound-total").text(0);
            if(response.success){
              document.getElementById('inbound').innerHTML = tableOriginal;
              Swal.fire({
                type: 'success',
                title: 'Your inbound has been submitted',
                showConfirmButton: false,
                timer: 2000
              });
            }else{
              document.getElementById('inbound').innerHTML = tableOriginal;
              Swal.fire({
                type: 'error',
                title: 'Error in submitting inbound',
                showConfirmButton: false,
                timer: 1500
              });
            }
          },
          error: function(){
            document.getElementById('inbound').innerHTML = tableOriginal;
            Swal.fire({
              type: 'error',
              title: 'Could not submit inbound',
              showConfirmButton: false,
              timer: 1500
            });
          }
        });
      }
    });
    //Check number of rows and disable button
    function disableRemoveBtn(){
      var count = $('#inbound tr').length;
      if (count <= 2) {
        $(".delete-inbound").attr("disabled", true);
      }
      else{
        $(".delete-inbound").attr("disabled", false);
      }
    }
    //Calculate amount
    $(document).on('change', '.quantity', function(){
      fillSummary();
    });
    //Separate numbers with commas
    function numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
    }
    //separate with commas automatically
    $(document).on('keyup', "input.quantity", function(event){
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;
      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });
  })
</script>
