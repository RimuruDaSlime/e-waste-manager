<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Inbound approval</h1>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of inbounds</h6>
    </div>
    <br>
    <div class="card-body">

      <div id="reportrange" class="rounded" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 23%">
          <i class="fa fa-calendar"></i>&nbsp;
          <span></span> <i class="fa fa-caret-down"></i>
      </div>
      <br>

      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" style="text-align: center;" cellspacing="0">
          <thead>
            <tr>
              <th>Inbound code</th>
              <th>Staff name</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="inbound-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="approveModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm approval</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" id="wrap">
          <table id="inbound-details" class="table table-bordered" style="text-align:center;">
            <thead>
              <tr>
                <th>Product name</th>
                <th>U.O.M</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody id="inbound-details-body">
            </tbody>
            <tfoot id="inbound-details-foot">
            </tfoot>
          </table>
            Do you want to approve this delivery?
            <form class="form-horizontal" action="" method="post" id="form-approval">
              <div class="form-group">
                <div class="container-fluid">
                  <textarea class="form-control" name="approval-note" rows="2" id="approval-note" required></textarea>
                </div>
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnApprove" class="btn btn-success">Approve</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div id="detailsModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Inbound details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" id="wrap">
          <table id="inbound-details2" class="table table-bordered" style="text-align:center;">
            <thead>
              <tr>
                <th>Product name</th>
                <th>U.O.M</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody id="inbound-details-body2">
            </tbody>
            <tfoot id="inbound-details-foot2">
              <th>Product name</th>
              <th>U.O.M</th>
              <th>Quantity</th>
            </tfoot>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm Cancellation</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            Do you want to cancel this delivery?
        </div>
        <div class="modal-footer">
          <button type="button" id="btnDelete" class="btn btn-danger">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>

<script type="text/javascript">
  $(function(){
    var inbound_id = null;
    showAllInbounds();
    initDateRangePicker();
    //Show all orders function
    function showAllInbounds(from, to){
      $('#dataTable').DataTable({
          "destroy": true,
          "ajax"   : {
            "method"  : 'post',
            "data"    : {"from": from, "to": to},
            "dataType": 'json',
            "url"    : '<?php echo base_url("products/getAllInboundsForApproval") ?>',
            "dataSrc": function (data) {
              var return_data = new Array();
              for(var i=0;i< data.length; i++){
                return_data.push({
                  'status'          : data[i].status,
                  'inbound_id'      : data[i].inbound_id,
                  'staff_name'      : data[i].staff_name,
                  'inbound_code'    : data[i].inbound_code
                })
              }
              return return_data;
            }
          },
          "columns": [
            {'data': 'inbound_code'},
            {'data': 'staff_name'},
            {'data': 'status'},
            {
              data: null,
              render: function ( data, type, row ) {
                if (data.status == 'approved' || data.status == 'cancelled') {
                  return '<a href="javascript:;" style="margin:5px;" id="approved" class="btn btn-info inbound-details" data="'+data.inbound_id+'">Details</a>';
                }
                else{
                  return '<a href="javascript:;" style="margin:5px;" class="btn btn-success inbound-approve" data="'+data.inbound_id+'">Approve</a>' +
                         '<a href="javascript:;" style="margin:5px;" class="btn btn-danger inbound-cancel" data="'+data.inbound_id+'">Cancel</a>';
                }
              }
            }
          ]
      });
    }
    //Approve inbound
    $('#inbound-data').on('click', '.inbound-approve', function(){
      if ($(this).attr('id') == 'approved' || $(this).attr('id') == 'cancelled') {
        var order_status = jsUcfirst($(this).attr('id'));
        $('.alert-success').html('Order status : ' + order_status).fadeIn().delay(1000).fadeOut('slow');
      }
      else{
        inbound_id = $(this).attr('data');
        $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>products/getInboundDetailsById',
          data: {inbound_id: inbound_id},
          dataType: 'json',
          success: function(data){
            var html = '';
            var i;
            var total_qty = 0;
            var inbound_id_label = ''
            var inbound_details_header = '<tr>' +
                                          '<th style="text-align:center" colspan="2"> Inbound code : </th>' +
                                          '<th style="text-align:center" colspan="4"> ' + data[0].inbound_code + ' </th>' +
                                         '</tr>';
            for (i = 0; i < data.length; i++) {
              html += '<tr>' +
                        '<td>' + data[i].product_name + '</td>' +
                        '<td>' + numberWithCommas(data[i].unit_of_measurement) + '</td>' +
                        '<td>' + numberWithCommas(data[i].quantity) + '</td>' +
                      '</tr>';
              total_qty += parseInt(data[i].quantity);
            }
            $('#inbound-details-body').html(html);
            html = '<tr>' +
                    '<th style="text-align:center" colspan="3">Total</th>' +
                  '</tr>'+
                  '<tr>' +
                    '<td colspan="3">₦' + numberWithCommas(total_qty) + '</td>' +
                  '</tr>';
            $('#inbound-details-foot').html(html);
            $('#inbound-details-header').html(inbound_details_header);
          },
          error: function(){
            alert('Could not get Data from Database');
          }
        });
        $('#approveModal').modal('show');
      }
    });
    //Confirm inbound approval
    $('#btnApprove').click(function(){
      //validate form
      var approval_note = $('textarea[name=approval-note]');
      var result = '';
      if(approval_note.val()==''){
        approval_note.parent().addClass('has-error');
        Swal.fire({
          type: 'error',
          title:'Oops...',
          text: 'Please leave a message!',
        });
      }else{
        approval_note.parent().removeClass('has-error');
        result += '1';
      }
      if (result == '1') {
        $.ajax({
          type: 'ajax',
          method: 'post',
          url: '<?php echo base_url() ?>products/approveInbound',
          data: {inbound_id: inbound_id, approval_note: approval_note.val()},
          dataType: 'json',
          success: function(data){
            if(data.success){
              $('#approveModal').modal('hide');
              Swal.fire({
                type: 'success',
                title: 'Inbound successfully approved',
                showConfirmButton: false,
                timer: 1500
              });
              showAllInbounds();
            }else{
              alert('Error');
            }
          },
          error: function(){
            alert('Could not approve inbound');
          }
        });
      }
    });
    //Approve inbound
    $('#inbound-data').on('click', '.inbound-details', function(){
      $('#detailsModal').modal('show');
      var inbound_id = $(this).attr('data');
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>products/getInboundDetailsById',
        data: {inbound_id: inbound_id},
        dataType: 'json',
        success: function(data){
          var html = '';
          var i;
          for (var i = 0; i < data.length; i++) {
            html += '<tr>'+
                      '<td>'+data[i].product_name+'</td>'+
                      '<td>'+data[i].unit_of_measurement+'</td>'+
                      '<td>'+data[i].quantity+'</td>'+
                    '</tr>';
          }
          $("#inbound-details-body2").html(html);
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
    });
    //Cancel order
    $('#order-data').on('click', '.order-cancel', function(){
      if ($(this).attr('id') == 'cancelled') {
        $('.alert-success').html('Order status : Cancelled').fadeIn().delay(1000).fadeOut('slow');
      }
      else{
        order_id = $(this).attr('data');
        $('#deleteModal').modal('show');
      }
    })
    //Confirm order cancellation
    $('#btnDelete').click(function(){
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>orders/cancelOrder',
        data: {order_id: order_id},
        dataType: 'json',
        success: function(data){
          if(data.success){
            showAllOrders();
            window.location.reload();
            $('#deleteModal').modal('hide');
            $('.alert-success').html('Order cancelled successfully').fadeIn().delay(1000).fadeOut('slow');
          }else{
            alert('Error');
          }
        },
        error: function(){
          alert('Could not cancel order');
        }
      });
    })
    //Capitalize first letter of string
    function jsUcfirst(string){
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    //Separate numbers with commas
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
    //Filter by status
    $("#view-type").change(function(){
      showAllOrders();
    });
    //daterangepicker
    function initDateRangePicker(){
      var start = moment().subtract(29, 'days');
      var end = moment();

      function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today'        : [moment(), moment()],
           'Yesterday'    : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days'  : [moment().subtract(6, 'days'), moment()],
           'Last 30 Days' : [moment().subtract(29, 'days'), moment()],
           'This Month'   : [moment().startOf('month'), moment().endOf('month')],
           'Last Month'   : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      }, cb);

      cb(start, end);
    }
    //filter table by date
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      var from = picker.startDate.format('YYYY-MM-DD');
      var to = picker.endDate.format('YYYY-MM-DD');
      showAllInbounds(from, to);
    });
  })
</script>
