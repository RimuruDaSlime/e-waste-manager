<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Deliveries</h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of products</h6>
    </div>
    <div class="card-body">

      <div id="reportrange" class="rounded" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 23%">
          <i class="fa fa-calendar"></i>&nbsp;
          <span></span> <i class="fa fa-caret-down"></i>
      </div>
      <br>

      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" style="text-align: center;" cellspacing="0">
          <thead>
            <tr>
              <th>Delivery code</th>
              <th>Customer name</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="delivery-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm Cancellation</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            Do you want to cancel this delivery?
        </div>
        <div class="modal-footer">
          <button type="button" id="btnDelete" class="btn btn-danger">Cancel</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div id="detailsModal" style="text-align:center;" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Delivery Details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <table id="delivery-details" class="table table-bordered" style="margin-top: 20px; text-align:center">
              <thead>
                <tr>
                  <th >N°</th>
                  <th >Product code</th>
                  <th >Product name</th>
                  <th >Description</th>
                  <th >Quantity ordered</th>
                  <th >Unit</th>
                  <th >In stock</th>
                  <th id="qty-delivery">Quantity for delivery</th>
                </tr>
              </thead>
              <tbody id="delivery-details-body">
              </tbody>
              <tfoot id="delivery-details-foot">
              </tfoot>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


</div>

<script type="text/javascript">
  $(function(){
    initDateRangePicker();
    showAllDeliveries();
    var list_id = {};
    var delivery_id = null;
    //Show all delivery function
    function showAllDeliveries(from, to){
      $('#dataTable').DataTable({
        "destroy": true,
        "ajax"   : {
          "method"  : 'post',
          "data"    : {"from": from, "to": to},
          "dataType": 'json',
          "url"    : '<?php echo base_url("deliveries/getAllDeliveries") ?>',
          "dataSrc": function (data) {
            var return_data = new Array();
            for(var i=0;i< data.length; i++){
              return_data.push({
                'delivery_code'   : data[i].delivery_code,
                'customer_name'   : data[i].customer_name,
                'status'          : data[i].status,
                'staff_name'      : data[i].staff_name,
                'delivery_id'     : data[i].delivery_id
              })
            }
            return return_data;
          }
        },
        "columns"    : [
          {'data': 'delivery_code'},
          {'data': 'customer_name'},
          {'data': 'status'},
          {
            data: null,
            render: function ( data, type, row ) {
              return '<a href="javascript:;" style="margin:5px;" class="btn btn-info delivery-details" data="'+data.delivery_id+'">Details</a>';
            }
          }
        ]
      });
    }
    //Show delivery details
    $('#delivery-data').on('click', '.delivery-details', function(){
      delivery_id = $(this).attr('data');
      $('#detailsModal').modal('show');
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>deliveries/getDeliveryDetailsById',
        data: {delivery_id: delivery_id},
        dataType: 'json',
        success: function(data){
          var html = '';
          var i;
          var total = 0;
          var total_suggestion = 0;
          var status = data[0].status;
          for (i = 0; i < data.length; i++) {
            var suggested = 0;
            var delivered = null;
            if (status == 'ready for delivery') {
              suggested = ((data[i].in_stock - data[i].quantity) > 0) ? data[i].quantity : (data[i].quantity - data[i].in_stock );
            }
            else if (status == 'delivered successfully') {

            }
            html += '<tr class="tr_input">' +
                      '<td>' + (i+ 1) + '</td>' +
                      '<td>' + data[i].product_code + '</td>' +
                      '<td>' + data[i].product_name + '</td>' +
                      '<td>' + data[i].description + '</td>' +
                      '<td name="quantity" class="qty_class">' + numberWithCommas(data[i].quantity) + '</td>' +
                      '<td>' + data[i].unit_of_measurement + '</td>' +
                      '<td>' + numberWithCommas(data[i].in_stock) + '</td>' +
                      '<td id="qty-delivery"><input type="text" name="'+data[i].product_name+'" style="text-align:center;" id="'+data[i].product_id+'" class="form-control qty" value="'+numberWithCommas(suggested)+'" placeholder="'+numberWithCommas(suggested)+'" readonly></td>' +
                    '</tr>';
            total += parseInt(data[i].quantity);
            total_suggestion += parseInt(suggested);
            list_id[data[i].product_id] = data[i].product_name;
          }
          $('#delivery-details-body').html(html);
          html = '<tr>' +
                  '<th style="text-align:right" colspan="4">Total</th>' +
                  '<th>' + numberWithCommas(total) + '</th>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<th><input type="text" id="totalQuantity" style="text-align:center;" value="'+numberWithCommas(total_suggestion)+'" class="form-control" readonly/></th>' +
                '</tr>';
          $('#delivery-details-foot').html(html);
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
    });
    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
    //daterangepicker
    function initDateRangePicker(){
      var start = moment().subtract(29, 'days');
      var end = moment();

      function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today'        : [moment(), moment()],
           'Yesterday'    : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days'  : [moment().subtract(6, 'days'), moment()],
           'Last 30 Days' : [moment().subtract(29, 'days'), moment()],
           'This Month'   : [moment().startOf('month'), moment().endOf('month')],
           'Last Month'   : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      }, cb);

      cb(start, end);
    }
    //filter table by date
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      var from = picker.startDate.format('YYYY-MM-DD');
      var to = picker.endDate.format('YYYY-MM-DD');
      showAllDeliveries(from, to);
    });
  })
</script>
