<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Customers</h1>
  <p class="mb-4">Here is a list of all our customers. To register a new customer, click here: <a href=<?php echo base_url("customers/register_customer"); ?>>customer registration</a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of customers</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Customer code</th>
              <th>Company name</th>
              <th>Name</th>
              <th>Email</th>
              <th>Contact Number</th>
              <th>Address</th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Customer code</th>
              <th>Company name</th>
              <th>Name</th>
              <th>Email</th>
              <th>Contact Number</th>
              <th>Address</th>
              <th>Action</th>
            </tr>
          </tfoot>
          <tbody id="customer-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="detailsModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Customer details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">List of products</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="customer-details-table" width="100%" cellspacing="0" style="text-align:center;">
                  <thead>
                    <tr>
                      <th>N°</th>
                      <th>Product code</th>
                      <th>Product name</th>
                      <th>Description</th>
                      <th>Unit</th>
                      <th>Price</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>N°</th>
                      <th>Product code</th>
                      <th>Product name</th>
                      <th>Description</th>
                      <th>Unit</th>
                      <th>Price</th>
                    </tr>
                  </tfoot>
                  <tbody id="customer-details-data">

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
  $(function(){
    showAllCustomers();
    var details;
    //Show all orders function
    function showAllCustomers(){
      $('#dataTable').DataTable({
        "rowReorder": true,
        "destroy"   : true,
        "ajax"      : {
        "url"       : '<?php echo base_url("customers/getAllCustomers"); ?>',
        "dataSrc": function (data) {
          var return_data = new Array();
          for(var i=0;i< data.length; i++){
            return_data.push({
              'customer_code' : data[i].customer_code,
              'company_name'  : data[i].company_name,
              'name'          : data[i].first_name + ' ' + data[i].last_name,
              'email'         : data[i].email,
              'phone'         : data[i].phone,
              'address'       : data[i].address,
              'customer_id'   : data[i].customer_id
            })
          }
          return return_data;
        }
      },
        "columns"    : [
          {'data': 'customer_code'},
          {'data': 'company_name'},
          {'data': 'name'},
          {'data': 'email'},
          {'data': 'phone'},
          {'data': 'address'},
          {
            data: null,
            render: function ( data, type, row ) {
              return '<a href="javascript:;" style="margin:5px;" id="details" class="btn btn-info customer-details" data="'+data.customer_id+'">Details</a>';
            }
          }
        ]
      });
    }
    //show customer details
    $('#customer-data').on('click', '.customer-details', function(){
      var custid = $(this).attr('data');;
      $('#customer-details-table').DataTable({
        "destroy": true,
        "ajax"   : {
          "url"     : '<?php echo base_url("customers/getCustomerDetails"); ?>',
          "method"  : 'POST',
          "data"    : {id: custid},
          "dataType": 'JSON',
          "dataSrc" : function (data) {
            var return_data = new Array();
            for(var i=0;i< data.length; i++){
              return_data.push({
                'N°'            : (i + 1),
                'product_id'    : data[i].product_id,
                'product_code'  : data[i].product_code,
                'product_name'  : data[i].product_name,
                'description'   : data[i].description,
                'unit'          : data[i].unit,
                'price'         : data[i].price,
                'customer_id'   : data[i].customer_id
              })
            }
            return return_data;
          }
        },
        "columns"    : [
          {'data': 'N°'},
          {'data': 'product_code'},
          {'data': 'product_name'},
          {'data': 'description'},
          {'data': 'unit'},
          {'data': 'price'},
        ]
      });
      $('#detailsModal').modal('show');
    })

  })
</script>
