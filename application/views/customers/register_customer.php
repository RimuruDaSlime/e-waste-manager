<!-- Begin Page Content -->
<div class="container-fluid" >

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Register a customer</h1>
  <p class="mb-4">To view the list of customers, click here : <a href="<?php echo base_url("customers/customers") ?>">list of customers</a>.</p>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Customer registration</h6>
    </div>

    <div class="container" style="width: 70%;">
      <form class="form-horizontal" role="form" id="myForm" style="margin-top: 15px; margin-bottom: 15px;">
          <!-- company name input-->
          <div class="control-group">
              <label class="control-label">Company Name*</label>
                <input id="company_name" name="company_name" type="text" placeholder="company name" class="form-control">
                <p class="help-block"></p>
          </div>
          <!-- first name input-->
          <div class="control-group">
              <label class="control-label">First Name*</label>
              <input id="first_name" name="first_name" type="text" placeholder="first name" class="form-control">
              <p class="help-block"></p>
          </div>
          <!-- last name input-->
          <div class="control-group">
              <label class="control-label">Last Name*</label>
              <input id="last_name" name="last_name" type="text" placeholder="last name" class="form-control">
              <p class="help-block"></p>
          </div>
          <!-- email input-->
          <div class="control-group">
              <label class="control-label">Email*</label>
              <input id="email" name="email" type="text" placeholder="email" class="form-control">
              <p class="help-block"></p>
          </div>
          <!-- contact number input-->
          <div class="control-group">
              <label class="control-label">Contact Number*</label>
              <input id="phone" name="phone" type="text" placeholder="contact number" class="form-control">
              <p class="help-block"></p>
          </div>
          <!-- region input-->
          <div class="control-group">
              <label class="control-label">State / Province / Region*</label>
              <input id="region" name="region" type="text" placeholder="state / province / region"
              class="form-control">
              <p class="help-block"></p>
          </div>
          <!-- city input-->
          <div class="control-group">
              <label class="control-label">City / Town*</label>
              <input id="city" name="city" type="text" placeholder="city" class="form-control">
              <p class="help-block"></p>
          </div>
          <!-- address-line input-->
          <div class="control-group">
              <label class="control-label">Address Line*</label>
              <input id="addres-line" name="address" type="text" placeholder="address line"
              class="form-control">
              <p class="help-block">Street address, P.O. box, company name, c/o</p>
          </div>
          <!-- postal-code input-->
          <div class="control-group">
              <label class="control-label">Zip / Postal Code*</label>
              <input id="postal-code" name="postal_code" type="text" placeholder="zip or postal code"
              class="form-control">
              <p class="help-block"></p>
          </div><!-- products input-->
          <div class="control-group">
              <label class="control-label">Products buying*</label>
              <select class="selectpicker form-control" id="products" name="products" data-live-search="true" multiple>
              </select>
              <p class="help-block"></p>
              <input id="prod" name="prod" type="hidden" class="form-control">
          </div>
          <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3">
                  <span class="help-block">Required fields*</span>
              </div>
          </div>
          <button type="button" id="btnRegister" class="btn btn-primary float-right" style="margin-bottom: 15px;">Register customer</button>
      </form> <!-- /form -->
    </div> <!-- ./container -->

  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
  $(function(){
    var formOriginal = document.getElementById('myForm').innerHTML;
    fillSelectPicker();
    //Confirm customer addition or update to database
    $('#btnRegister').click(function(){
      var url = $('#myForm').attr('action');
      //validate form
      var company_name = $('input[name=company_name]');
      var first_name   = $('input[name=first_name]');
      var last_name    = $('input[name=last_name]');
      var email        = $('input[name=email]');
      var phone        = $('input[name=phone]');
      var region       = $('input[name=region]');
      var city         = $('input[name=city]');
      var address      = $('input[name=address]');
      var postal_code  = $('input[name=postal_code]');
      var products     = $('select[name=products]');

      var result = true;
      //Company name validation
      if(company_name.val()==''){
        company_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        company_name.parent().removeClass('has-error');
      }
      //First name validation
      if(first_name.val()==''){
        first_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        first_name.parent().removeClass('has-error');
      }
      //Last name validation
      if(last_name.val()==''){
        last_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        last_name.parent().removeClass('has-error');
      }
      //Email validation
      if(email.val()==''){
        email.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        email.parent().removeClass('has-error');
      }
      //Phone validation
      if(phone.val()==''){
        phone.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        phone.parent().removeClass('has-error');
      }
      //Region validation
      if(region.val()==''){
        region.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        region.parent().removeClass('has-error');
      }
      //City validation
      if(city.val()==''){
        city.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        city.parent().removeClass('has-error');
      }
      //Address validation
      if(address.val()==''){
        address.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        address.parent().removeClass('has-error');
      }
      //Postal code validation
      if(postal_code.val()==''){
        postal_code.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        postal_code.parent().removeClass('has-error');
      }
      //Products validation
      if(products.val().length === 0){
        products.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        products.parent().removeClass('has-error');
        $('input[name=prod]').val(products.val());
      }

      if(result==true){
        var data = $('#myForm').serialize();
        $.ajax({
          type: 'ajax',
          method: 'post',
          url: "<?php echo base_url("customers/addCustomer"); ?>",
          data: data,
          dataType: 'json',
          success: function(response){
            if(response.success){
              document.getElementById('myForm').innerHTML = formOriginal;
              Swal.fire({
                type: 'success',
                title: 'Customer registration',
                text: 'Customer registered successfully',
                showConfirmButton: false,
                timer: 1500
              })
            }else{
              Swal.fire({
                type: 'error',
                title: 'Error in registering customer',
                showConfirmButton: false,
                timer: 1500
              });
            }
          },
          error: function(){
            Swal.fire({
              type: 'error',
              title: 'Could not register customer',
              showConfirmButton: false,
              timer: 1500
            });
          }
        });
      }
    });
    //fill select picker
    function fillSelectPicker(){
      $.ajax({
        type: 'ajax',
        url: '<?php echo base_url() ?>products/getAllProducts',
        dataType: 'json',
        success: function(data){
          for (var i = 0; i < data.length; i++) {
            $('#products').append('<option value="'+data[i].product_id+'">'+jsUcfirst(data[i].product_name)+'</option>');
          }
            $('#products').selectpicker('refresh');
        },
        error: function(){
          alert('Could not approve order');
        }
      });
    }
  })
</script>
