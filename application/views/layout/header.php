<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>E.W Manager</title>

  <!-- Custom fonts for this template-->
  <link href=<?php echo base_url("assets/vendor/fontawesome-free/css/all.min.css") ?> rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url("assets/css/sb-admin-2.css") ?>" rel="stylesheet">
  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <!-- Datatables -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/datatables.min.css') ?>"/>
	<script type="text/javascript" src="<?php echo base_url('assets/DataTables/datatables.min.js'); ?>"></script>
  <!-- Sweet Alert -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <!-- Latest compiled and minified CSS bootstrap-select -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
  <!-- JQUERY UI -->
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/humanity/jquery-ui.css">
  <!-- Print this add on -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/printThis.js'); ?>"></script>
  <!-- Bootstrap theme add on -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-theme.css') ?>"/>
  <!-- Cookie font add on -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Cookie" />
  <!-- date range picker add on -->
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <!-- myJs -->
  <script type="text/javascript" src="<?php echo base_url('assets/js/myJs.js'); ?>"></script>

</head>

<body id="page-top" >

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-secondary sidebar sidebar-dark accordion" id="accordionSidebar" style="background-image: url('http://localhost/sim/assets/images/wallpapers/wallpaper1.jpg');">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fab fa-drupal"></i>
        </div>
        <div class="sidebar-brand-text mx-3" >E.W. Manager</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Master Data
      </div>

      <!-- Nav Item - Customers Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users"></i>
          <span>Customers</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Actions:</h6>
            <a class="collapse-item" href="<?php echo base_url("customers/customers"); ?>">Customers</a>
            <a class="collapse-item" href="<?php echo base_url("customers/register_customer"); ?>">Register customer</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Products Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-dumpster"></i>
          <span>Products</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Actions:</h6>
            <a class="collapse-item" href="<?php echo base_url("products/products"); ?>">Products</a>
            <a class="collapse-item" href="<?php echo base_url("products/add_product"); ?>">Add product</a>
          </div>
        </div>
      </li>

      <?php if($this->session->userdata('role_id') == 1) : ?>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
          Admin
        </div>

      <!-- Nav Item - Sales orders Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-user-friends"></i>
          <span>Users</span>
        </a>
        <div id="collapseUsers" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">User management:</h6>
            <a class="collapse-item" href="<?php echo base_url("users/register_user"); ?>">Create user</a>
            <a class="collapse-item" href="<?php echo base_url("users/users"); ?>">Manage users</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Sales orders Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseApprovals" aria-expanded="true" aria-controls="collapsePages">
          <i class="far fa-check-circle"></i>
          <span>Approvals</span>
        </a>
        <div id="collapseApprovals" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Sales orders:</h6>
            <a class="collapse-item" href="<?php echo base_url("sales_orders/order_approval") ?>">Approve orders</a>
            <h6 class="collapse-header">Warehouse:</h6>
            <a class="collapse-item" href="<?php echo base_url("warehouse/inbound_approval") ?>">Approve inbounds</a>
            <a class="collapse-item" href="<?php echo base_url("warehouse/delivery_approval") ?>">Approve deliveries</a>
          </div>
        </div>
      </li>

      <?php endif; ?>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
          Navigate
        </div>

      <?php if($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 2) : ?>

      <!-- Nav Item - Sales orders Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSalesOrders" aria-expanded="true" aria-controls="collapsePages">
          <i class="fab fa-accusoft"></i>
          <span>Sales orders</span>
        </a>
        <div id="collapseSalesOrders" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Order management:</h6>
            <a class="collapse-item" href="<?php echo base_url("sales_orders/place_order"); ?>">Place an order</a>
            <a class="collapse-item" href="<?php echo base_url("sales_orders/orders"); ?>">Orders</a>
          </div>
        </div>
      </li>

      <?php endif; ?>

      <?php if($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 3) : ?>

      <!-- Nav Item - Warehouse Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseWarehouse" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-warehouse"></i>
          <span>Warehouse</span>
        </a>
        <div id="collapseWarehouse" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Warehouse management:</h6>
            <a class="collapse-item" href="<?php echo base_url("warehouse/inbounds") ?>">Inbounds</a>
            <a class="collapse-item" href="<?php echo base_url("warehouse/deliveries") ?>">Deliveries</a>
            <a class="collapse-item" href="<?php echo base_url("warehouse/waybills") ?>">Waybills</a>
          </div>
        </div>
      </li>

      <?php endif; ?>

      <?php if($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 2 || $this->session->userdata('role_id') == 4) : ?>

      <!-- Nav Item - Warehouse Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFinance" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-credit-card"></i>
          <span>Finance</span>
        </a>
        <div id="collapseFinance" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Finance management:</h6>
            <a class="collapse-item" href="<?php echo base_url("finance/create_invoice") ?>">Generate invoice</a>
            <a class="collapse-item" href="<?php echo base_url("finance/invoices") ?>">Invoices</a>
          </div>
        </div>
      </li>

      <?php endif; ?>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand bg-gradient-dark topbar mb-4 static-top shadow" style="background-image: url('http://localhost/sim/assets/images/wallpapers/wallpaper1.jpg');">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font: normal 28px Cookie, Arial, Helvetica, sans-serif;">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><span style="color: #00faff;"><?php echo $this->session->userdata('name'); ?></span></span>
                <img class="img-profile rounded-circle"
                  alt="<?php echo base_url("assets/images/users/users.png"); ?>"
                  src=<?php
                          $img = $this->session->userdata('img');
                          echo base_url("assets/images/users/".$img);
                      ?>>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo base_url("users/edit_profile"); ?>">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
