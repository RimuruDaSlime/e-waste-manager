<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Users</h1>
  <p class="mb-4">Here is a list of all the users. To register a new user, click here: <a href=<?php echo base_url("users/register_user"); ?>>user registration</a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of customers</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align: center;">
          <thead>
            <tr>
              <th>User code</th>
              <th>Name</th>
              <th>Email</th>
              <th>Privilege</th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>User code</th>
              <th>Name</th>
              <th>Email</th>
              <th>Privilege</th>
              <th>Action</th>
            </tr>
          </tfoot>
          <tbody id="user-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirm removal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            Do you want to remove this employee?
        </div>
        <div class="modal-footer">
          <button type="button" id="btnDelete" class="btn btn-danger">Remove</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>
<!-- /.container-fluid -->

<script type="text/javascript">
  $(function(){
    showAllUsers();
      var user_id = null;
    //show all users
    function showAllUsers(){
      $('#dataTable').DataTable({
        "destroy": true,
        "ajax"   : {
        "url"    : '<?php echo base_url("users/getAllUsers"); ?>',
        "dataSrc": function (data) {
          var return_data = new Array();
          var role = '';
          for(var i=0;i< data.length; i++){
            var role_id = parseInt(data[i].role_id);
            switch (role_id) {
              case 1:
                role = 'admin';
                break;
              case 2:
                role = 'sales';
                break;
              case 3:
                role = 'warehouse';
                break;
              case 4:
                role = 'finance';
                break;

            }
            return_data.push({
              'user_id'   : data[i].staff_id,
              'user_code' : data[i].staff_code,
              'name'      : data[i].first_name + ' ' + data[i].last_name,
              'email'     : data[i].email,
              'privilege' : role,
            })
          }
          return return_data;
        }
      },
        "columns"    : [
          {'data': 'user_code'},
          {'data': 'name'},
          {'data': 'email'},
          {'data': 'privilege'},
          {
            data: null,
            render: function ( data, type, row ) {
              return '<a href="javascript:;" style="margin:5px;" class="btn btn-info user-details" data="'+data.user_id+'" hidden>View profile</a>' +
                     '<a href="javascript:;" style="margin:5px;" class="btn btn-danger user-remove" data="'+data.user_id+'">Remove</a>'
            }
          }
        ]
      });
    }
    //View user details
    $('#user-data').on('click', '.user-details', function(){
      var user_id = $(this).attr('data');
    });
    //Confirm removal
    $('#user-data').on('click', '.user-remove', function(){
      $('#deleteModal').modal('show');
      user_id = $(this).attr('data');
    });
    //Remove user
    $('#btnDelete').click(function(){
      $.ajax({
        type: 'ajax',
        method: 'post',
        url: '<?php echo base_url() ?>users/deleteUser',
        data: {id: user_id},
        dataType: 'json',
        success: function(data){
          if (data.success) {
            Swal.fire({
              type: 'success',
              title: 'Success',
              text: 'User successfully removed',
            });
            showAllUsers();
          }
          else{
            Swal.fire({
              type: 'error',
              title: 'Oops...',
              text: 'Unable to remove user!',
            });
            showAllUsers();
          }
        },
        error: function(){
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Unable to remove user!!',
          });
          showAllUsers();
        }
      });
      $('#deleteModal').modal('hide');
    })
  })
</script>
