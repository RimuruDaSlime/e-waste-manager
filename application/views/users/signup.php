<!-- Begin Page Content -->
<div class="container-fluid" >

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Register a user</h1>
  <p class="mb-4">To view the list of user, click here : <a href="<?php echo base_url("users/users") ?>">list of users</a>.</p>

	<!-- Card container -->
  <div class="card shadow mb-4">

    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">User registration</h6>
    </div>

		<div class="container" style="width: 60%; margin-top: 15px; margin-bottom: 15px;">
			<form class="form-horizontal" role="form" id="myForm">

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-signature"></i></i></span>
					</div>
					<input type="text" class="form-control" name="first_name" placeholder="First Name">
				</div>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-signature"></i></i></span>
					</div>
					<input type="text" class="form-control" name="last_name" placeholder="Last Name">
				</div>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-user"></i></span>
					</div>
					<input type="text" class="form-control" name="username" placeholder="Username">
				</div>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-key"></i></span>
					</div>
					<input type="password" class="form-control" name="password" placeholder="Password">
				</div>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-unlock-alt"></i></span>
					</div>
					<input type="password" class="form-control" name="password2" placeholder="Confirm Password">
				</div>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-envelope"></i></span>
					</div>
					<input type="text" class="form-control" name="email" placeholder="Email">
				</div>

				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fab fa-rebel"></i></i></span>
					</div>
					<select class="selectpicker border rounded" id="privilege" name="privilege" style="width:300px;">
						<option value="" selected hidden>User privilege</option>
						<option value="1">Admin</option>
						<option value="2">Sales</option>
            <option value="3">Warehouse</option>
            <option value="4">Finance</option>
					</select>
				</div>

				<div class="form-group">
					<input type="button" value="Register user" class="btn float-right signup_btn btn-primary">
				</div>
			</form> <!-- /form -->
	  </div> <!-- ./container -->

	</div>

</div>
<!-- /.container-fluid -->

<script type="text/javascript">
	$(function(){
		var formOriginal = document.getElementById('myForm').innerHTML;
		$(".signup_btn").click(function(){
			var result = true;
			var data = {};
			//validate form
      var first_name 	= $('input[name=first_name]');
      var last_name 	= $('input[name=last_name]');
			var username 		= $('input[name=username]');
			var password 		= $('input[name=password]');
			var password2 	= $('input[name=password2]');
      var email 			= $('input[name=email]');
			var privilege 	= $('select[name=privilege]');

			//First name validation
      if(first_name.val()==''){
        first_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        first_name.parent().removeClass('has-error');
      }
      //Last name validation
      if(last_name.val()==''){
        last_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        last_name.parent().removeClass('has-error');
      }
			//Username validation
      if(username.val()==''){
        username.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        username.parent().removeClass('has-error');
      }
			//Password validation
      if(password.val()==''){
        password.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        password.parent().removeClass('has-error');
      }
			//Password2 validation
      if(password2.val()!=password.val() || password2.val()==''){
				password.parent().addClass('has-error');
        password2.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Passwords do not match!',
        });
      }else{
				password.parent().removeClass('has-error');
        password2.parent().removeClass('has-error');
      }
      //Email validation
      if(email.val()==''){
        email.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        email.parent().removeClass('has-error');
      }
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      if (!emailReg.test(email.val())) {
        email.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Not a valid email!',
        });
      }else{
        email.parent().removeClass('has-error');
      }
			//Privilege validation
      if(privilege.children("option:selected").val()==''){
        privilege.parent().addClass('border-danger');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        privilege.parent().removeClass('border-danger');
      }

			if(result){
        var data = $('#myForm').serialize();
        $.ajax({
          type: 'ajax',
          method: 'post',
          url: "<?php echo base_url("users/signup"); ?>",
          data: data,
          dataType: 'json',
          success: function(response){
            if(response.success){
              $("#myForm")[0].reset();
              $('#privilege').prop('selectedIndex',0);
              Swal.fire({
                type: 'success',
                title: 'User registration',
                text: 'User registered successfully',
                showConfirmButton: false,
                timer: 1500
              })
            }else{
              Swal.fire({
                type: 'error',
                title: 'Error in registering user',
                showConfirmButton: false,
                timer: 1500
              });
            }
          },
          error: function(){
            Swal.fire({
              type: 'error',
              title: 'Could not register user',
              showConfirmButton: false,
              timer: 1500
            });
          }
        });
      }

		});
	})
</script>
