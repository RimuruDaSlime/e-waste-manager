<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Users</h1>
  <p class="mb-4">Here is a list of all the users. To register a new user, click here: <a href=<?php echo base_url("users/register_user"); ?>>user registration</a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of customers</h6>
    </div>
    <div class="card-body">
      <div class="container">
          <h1>Edit Profile</h1>
        	<hr>
      	<div class="row">
            <!-- left column -->
            <div class="col-md-3">
              <?php echo form_open_multipart('users/upload_image');?>
                <div class="text-center">
                  <img src="//placehold.it/100" class="edit-image rounded-circle" alt="avatar" id="blah" height="100" width="100">
                  <h6>Upload a different photo...</h6>

                  <input type="file" name="userfile" class="form-control-file" id="imgInp">
                </div>
                <button type="submit" style="display:none;" class="btn btn-default img-upload">Submit</button>
              </form>
            </div>

            <!-- edit form column -->
            <div class="col-md-9 personal-info">
              <h3>Personal info</h3>

              <form class="form-horizontal" role="form" id="myForm">
                <div class="form-group">
                  <label class="col-lg-3 control-label">First name:</label>
                  <div class="col-lg-8">
                    <input class="form-control id" name="id" type="hidden" value="<?php echo (isset($_SESSION['id'])) ? $_SESSION['id'] : $_SESSION['staff_id']; ?>">
                    <input class="form-control first-name" name="first-name" type="text" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">Last name:</label>
                  <div class="col-lg-8">
                    <input class="form-control last-name" name="last-name" type="text" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-lg-3 control-label">Email:</label>
                  <div class="col-lg-8">
                    <input class="form-control email" name="email" type="text" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Username:</label>
                  <div class="col-md-8">
                    <input class="form-control username" name="username" type="text" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Password:</label>
                  <div class="col-md-8 input-group" id="show_hide_password">
                    <input class="form-control" type="password" name="password" value="">
                    <span class="input-group-btn">
                      <button class="btn btn-default reveal" type="button"><i class="fa fa-eye-slash"></i></button>
                    </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Confirm password:</label>
                  <div class="col-md-8 input-group" id="show_hide_password">
                    <input class="form-control" type="password" name="password2" value="">
                    <span class="input-group-btn">
                      <button class="btn btn-default reveal" type="button"><i class="fa fa-eye-slash"></i></button>
                    </span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label"></label>
                  <div class="col-md-8">
                    <input type="button" class="btn btn-primary float-right edit-profile" value="Save Changes">
                  </div>
                </div>
              </form>
            </div>
        </div>
      </div>
      <hr>
    </div>
  </div>

</div>
<!-- /.container-fluid -->

<script type="text/javascript">
  $(function(){
    getUserInfo();
    //get user information
    function getUserInfo(){
      $.ajax({
        type: 'ajax',
        method: 'post',
        url: "<?php echo base_url("users/getUserInfo"); ?>",
        data: {id:"<?php echo (isset($_SESSION['id'])) ? $_SESSION['id'] : $_SESSION['staff_id']; ?>"},
        dataType: 'json',
        success: function(response){
          $(".first-name").val(response.first_name);
          $(".last-name").val(response.last_name);
          $(".email").val(response.email);
          $(".username").val(response.username);
        },
        error: function(){
          Swal.fire({
            type: 'error',
            title: 'Could not get user information',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });
    }
    //edit profile function
    $(".edit-profile").click(function(){
      var result = true;

      var first_name 	= $('input[name=first-name]');
      var last_name 	= $('input[name=last-name]');
      var username 		= $('input[name=username]');
      var password 		= $('input[name=password]');
      var password2 	= $('input[name=password2]');
      var email 			= $('input[name=email]');

      //First name validation
      if(first_name.val()==''){
        first_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        first_name.parent().removeClass('has-error');
      }
      //Last name validation
      if(last_name.val()==''){
        last_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        last_name.parent().removeClass('has-error');
      }
			//Username validation
      if(username.val()==''){
        username.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        username.parent().removeClass('has-error');
      }
			//Password validation
      if(password.val()==''){
        password.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        password.parent().removeClass('has-error');
      }
      //Password2 validation
      if(password2.val()!=password.val()){
        password2.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Passwords do not match!',
        });
      }else{
        password2.parent().removeClass('has-error');
      }
      //Email validation
      if(email.val()==''){
        email.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        email.parent().removeClass('has-error');
      }

      if (result) {
        var data = $('#myForm').serialize();
        $.ajax({
          type: 'ajax',
          method: 'post',
          url: "<?php echo base_url("users/edit"); ?>",
          data: data,
          dataType: 'json',
          success: function(response){
            if(response.success){
              getUserInfo();
              password.val('');
              password2.val('');
              Swal.fire({
                type: 'success',
                title: 'Profile edit',
                text: 'Changes saved successfully',
                showConfirmButton: false,
                timer: 1500
              });
            }else{
              Swal.fire({
                type: 'error',
                title: 'Error in editing profile',
                showConfirmButton: false,
                timer: 1500
              });
            }
          },
          error: function(){
            Swal.fire({
              type: 'error',
              title: 'Could not save changes',
              showConfirmButton: false,
              timer: 1500
            });
          }
        });
      }
    })
    //show password function
    $("#show_hide_password button").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
    //preview img
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#imgInp").change(function(){
        readURL(this);
    });
  })
</script>
