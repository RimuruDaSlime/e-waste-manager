<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Products</h1>
  <p class="mb-4">Here is a list of all our available products. To add a new product, click here: <a href=<?php echo base_url("products/add_product"); ?>>add a new product</a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of products</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align:center">
          <thead>
            <tr>
              <th>Product code</th>
              <th>Name</th>
              <th width="25%">Description</th>
              <th>Category</th>
              <th>Price</th>
              <th>Unit</th>
              <th>In stock</th>
              <th>Allocated</th>
              <th>In order</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Product code</th>
              <th>Name</th>
              <th>Description</th>
              <th>Category</th>
              <th>Price</th>
              <th>Unit</th>
              <th>In stock</th>
              <th>Allocated</th>
              <th>In order</th>
            </tr>
          </tfoot>
          <tbody id="product-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript">
  $(function(){
    showAllOrders();
    //Show all orders function
    function showAllOrders(){
      $('#dataTable').DataTable({
        "destroy": true,
        "ajax"   : {
        "url"    : '<?php echo base_url("products/getAllProducts"); ?>',
        "dataSrc": function (data) {
          var return_data = new Array();
          for(var i=0;i< data.length; i++){
            return_data.push({
              'product_code' : data[i].product_code,
              'category'     : data[i].category_name,
              'name'         : data[i].product_name,
              'description'  : data[i].description,
              'price'        : '₦' + numberWithCommas(data[i].price),
              'unit'         : data[i].unit_of_measurement,
              'in_stock'     : numberWithCommas(data[i].in_stock),
              'in_order'     : numberWithCommas(data[i].in_order),
              'allocated'    : numberWithCommas(data[i].allocated)
            })
          }
          return return_data;
        }
      },
        "columns"    : [
          {'data': 'product_code'},
          {'data': 'name'},
          {'data': 'description'},
          {'data': 'category'},
          {'data': 'price'},
          {'data': 'unit'},
          {'data': 'in_stock'},
          {'data': 'allocated'},
          {'data': 'in_order'}
        ]
      });
    }
  })
</script>
