<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Add a new product</h1>
  <p class="mb-4">To view the list of customers, click here : <a href="<?php echo base_url("products/products") ?>">list of products</a>.</p>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Add product</h6>
    </div>

    <div class="container" style="width: 75%;">
      <form class="form-horizontal" role="form" id="myForm" style="margin-top: 15px; margin-bottom: 15px;">
        <div class="row">
          <div class="col-sm-6">
            <!-- category input-->
            <div class="form-group">
              <label for="name" id="category-label" class="label-control col-md-4">Category*</label>
              <input type="hidden" name="category-id" class="form-control">
              <select class="selectpicker form-control" id="product-category"  data-live-search="true" required>
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <!-- unit input-->
            <div class="form-group">
              <label for="name" id="discount-label" class="label-control col-md-7">Unit of Measurement*</label>
              <input type="hidden" name="measurement-type-value" class="form-control">
              <select class="selectpicker form-control" id="measurement-type" data-live-search="true" required>
                <option value="cartons">cartons</option>
                <option value="pcs">pcs</option>
                <option value="boxes">boxes</option>
                <option value="pallets">pallets</option>
                <option value="kgs">kgs</option>
              </select>
            </div>
          </div>
        </div>
          <!-- product name input-->
          <div class="form-group">
            <label for="name" class="label-control col-md-4">Product name*</label>
            <input name="product-name" class="form-control " id="product-name" style="display: block; margin: 0 auto;">
          </div>
          <!-- description input-->
          <div class="form-group">
            <label for="name" id="description-label" class="label-control col-md-4">Description*</label>
              <textarea class="form-control" name="description" rows="5" id="description"></textarea>
          </div>
          <!-- price input-->
          <div class="form-group">
            <label for="name" id="price-label" class="label-control col-md-4">Price*</label>
              <input name="price" class="form-control" id="price" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
          </div>
          <div class="form-group">
              <div class="col-sm-9 col-sm-offset-3">
                  <span class="help-block">*Required fields</span>
              </div>
          </div>
          <button type="button" id="btnAdd" class="btn btn-primary float-right" style="margin-bottom:15px;">Add product</button>
      </form> <!-- /form -->
    </div> <!-- ./container -->
  </div>



  <div id="modalAddCategory" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add new category</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <form id="formAddCategory" action="<?php echo base_url() ?>products/addCategory" method="post">
            <div class="form-group">
              <label for="name" class="label-control col-md-4">Category name</label>
              <div>
                <input name="category-name" class="form-control" id="category-name">
              </div>
            </div>
            <div class="form-group">
              <label for="name" class="label-control col-md-4">Description</label>
              <div>
                <textarea class="form-control" name="category-description" rows="5" id="category-description"></textarea>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" id="btnAddCategory" class="btn btn-primary">Add</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>
<!-- /.container-fluid -->

<script type="text/javascript">
  $(function(){
    showAllCategories();
    showAddCategoryModal();
    //Get list of categories
    function showAllCategories(){
      $.ajax({
        type: 'ajax',
        url: '<?php echo base_url() ?>products/getAllCategories',
        dataType: 'json',
        success: function(data){
          var html = '<option value="addCategory">add new category... </option>';
          var i;
          for(i=0; i<data.length; i++){
            html += '<option value="' + data[i].category_id + '">' + data[i].category_name + '</option>';
          }
          $('#product-category').html(html);
          $('#product-category').val(1);
          $('#product-category').selectpicker('refresh');
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
    }
    //Show add category Modal
    function showAddCategoryModal(){
      $('#product-category').change(function() { //jQuery Change Function
        var opval = $(this).val(); //Get value from select element
        if(opval=="addCategory"){ //Compare it and if true
          $('#product-category').val(1);
          $('#myModal').modal("hide"); //Hide modal
          $('#modalAddCategory').modal("show"); //Open category modal
        }
      });
    }
    //Confirm product addition to database
    $('#btnAdd').click(function(){
      var result = true;
      //Validate form
      var select = document.getElementById("product-category");
      var type = select.options[select.selectedIndex].value;
      var category_id = $('input[name=category-id]');
      category_id.val(type);

      var select2 = document.getElementById("measurement-type");
      var type2 = select2.options[select2.selectedIndex].value;
      var measurement_type = $('input[name=measurement-type-value]');
      measurement_type.val(type2);

      var product_name = $('input[name=product-name]');
      var description = $('textarea[name=description]');
      var price = $('input[name=price]');
      //Product name
      if (product_name.val() == ''){
        product_name.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        product_name.parent().removeClass('has-error');
      }
      //Description
      if (description.val() == ''){
        description.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        description.parent().removeClass('has-error');
      }
      //Price
      if (price.val() == ''){
        price.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Missing field required!',
        });
      }else{
        price.parent().removeClass('has-error');
      }
      var url = $('#myForm').attr('action');
      if (result) {
        var data = $('#myForm').serialize();
        $.ajax({
          type: 'ajax',
          method: 'post',
          url: '<?php echo base_url('products/addProduct'); ?>',
          data: data,
          dataType: 'json',
          success: function(response){
            if(response.success){
              $('#myModal').modal('hide');
              $('#myForm')[0].reset();
              if(response.type=='add'){
                var type = 'added'
              }else if(response.type=='update'){
                var type ="updated"
              }
              Swal.fire({
                type: 'success',
                title: 'Added',
                text: 'Product '+type+' successfully',
              });
              showAllCategories();
            }else{
              alert('Error');
            }
          },
          error: function(){
            alert('Could not add data');
          }
        });
      }
    });
  })
</script>
