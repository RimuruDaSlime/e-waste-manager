<!-- Begin Page Content -->
<div class="container-fluid">
  <form class="order-form" action="" method="post" id="form">
    <div class="container-fluid">
        <div class="table-wrapper">
          <div class="table-title">
              <div class="row" style="text-align:center;">
                  <div class="row">
                    <h4> Generate <b>Invoice</b></h4>
                  </div>
              </div>
          </div>
            <label for="">Order Reference No.</label>
            <input type="hidden" name="order-id" value="">
            <input type='text' class='order-code form-control' id='order_1' name="order-code" placeholder='Enter reference number' style="width:250px;" required>
        </div>
    </div>
    <div class="container">
        <div class="table-wrapper">
          <br>
            <table id="order" class="table table-bordered table-hover" style="width:100%; text-align:center;">
              <thead>
                <tr>
                  <th>Product code</th>
                  <th>Product</th>
                  <th width="25%">Description</th>
                  <th>Quantity ordered</th>
                  <th>Unit</th>
                  <th>Price</th>
                  <th>Quantity for delivery</th>
                  <th>Amount (₦)</th>
                </tr>
              </thead>
              <tbody class="invoice-items">
               <tr class="tr_input">
                 <td style="display:none;"><input type='hidden' class='product-id form-control active' id='prodid_1'></td>
                 <td class='code' id='code_1' name="code"></td>
                 <td class='product' id='product_1' name="product-name"></td>
                 <td class='description' id='description_1' name="description"></td>
                 <td class='quantity' id='quantity_1' name="quantity"></td>
                 <td class='uom' id='uom_1' name="uom"></td>
                 <td class='price' id='price_1' name="price"></td>
                 <td><input type='text' class='delivery form-control' id='delivery_1' name="delivery"></td>
                 <td><input type='text' class='amount form-control' id='amount_1' name="amount" ></td>
               </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th class="text-center col-xs-1" colspan="6">Total</th>
                  <th class="text-center rowtotal mono invoice-total-delivery" ></th>
                  <th class="text-center rowtotal mono invoice-total" >₦0</th>
                </tr>
              </tfoot>
            </table>
        </div>
    </div>
  </form>
  <button type="button" style="float:right; margin-right:15px;" class="btn btn-success create-invoice"> Submit invoice</button>
</div>

<script type="text/javascript">
  $(function(){
    var formOriginal = document.getElementById('form').innerHTML;
    var total_amount = 0;
    //Auto complete for invoice
    $(document).on('keydown', '.order-code', function(){
      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      // Initialize jQuery UI autocomplete
      $( '#'+id ).autocomplete({
        source: function( request, response ) {
          $.ajax({
            url: "<?php echo base_url('orders/getAllOrdersForInvoice') ?>",
            type: 'post',
            dataType: "json",
            data: {
              search: request.term,request:1
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        select: function (event, ui) {
        $(this).val(ui.item.label); // display the selected text
        var orderid = ui.item.value; // selected value
        $('input[name=order-id]').val(orderid);
          // AJAX
          $.ajax({
          url: "<?php echo base_url('orders/getAllOrdersForInvoice') ?>",
          type: 'post',
          data: {orderid:orderid,request:2},
          dataType: 'json',
            success:function(data){
              var html = '';
    					var i;
              var total = 0;
              var delivery = 0;
    					for(i=0; i<data.length; i++){
                var amount      = (data[i].price * data[i].quantity);
                total          += parseFloat(amount);
                var deliveryid  = 'delivery_' + (i + 1);
                var qtyid       = 'quantity_' + (i + 1);
                var priceid     = 'price_'    + (i + 1);
                var amountid    = 'amount_'   + (i + 1);
                var prodid      = 'prod_'     + (i + 1);
                html += '<tr class="tr_input">' +
                          '<td style="display:none;"><input type="hidden" class="product-id form-control active" value="'+data[i].product_id+'" id="'+prodid+'"></td>' +
                          '<td class="code" name="code">'+data[i].product_code+'</td>' +
                          '<td class="product" name="product-name">'+data[i].product_name+'</td>' +
                          '<td class="description" name="description">'+data[i].description+'</td>' +
                          '<td class="quantity" name="quantity" id="'+qtyid+'">'+numberWithCommas(data[i].quantity)+'</td>' +
                          '<td class="uom" name="uom">'+data[i].unit_of_measurement+'</td>' +
                          '<td class="price" name="price" id="'+priceid+'">'+numberWithCommas(data[i].price)+'</td>' +
                          '<td><input type="text" class="delivery form-control" name="delivery" value="'+numberWithCommas(data[i].quantity)+'" id="'+deliveryid+'"></td>' +
                          '<td><input type="text" class="amount form-control" name="amount" value="'+numberWithCommas(data[i].total)+'" id="'+amountid+'"></td>' +
                        '</tr>';

    					}
    					$('.invoice-items').html(html);
              $(".invoice-total").text('₦' + numberWithCommas(total));
            }
          });
          return false;
        }
      })
    });
    //Calculate amount
    $(document).on('keyup', '.delivery', function(){
      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      var delivery_index = 'delivery_' + index;
      // Set value to textboxes
      var price = document.getElementById('price_'+index).innerText.replace(/,/g, '');
      var qty = (Number.isNaN(parseInt(document.getElementById(delivery_index).value))) ? 0 : parseInt(document.getElementById(delivery_index).value);
      var amount = price * qty;
      document.getElementById('amount_'+index).value = numberWithCommas(amount);
      total_amount = 0;
      $("tr.tr_input").each(function() {
        total_amount += parseFloat($(this).find("input.amount").val().replace(/,/g, ''));
      });
      $(".invoice-total").text('₦' + numberWithCommas(total_amount));
    });
    //Calculate qty
    $(document).on('keyup', '.amount', function(){
      var id = this.id;
      var splitid = id.split('_');
      var index = splitid[1];
      var amount_index = 'amount_' + index;
      // Set value to textboxes
      var price = document.getElementById('price_'+index).innerText.replace(/,/g, '');
      var amount = (document.getElementById(amount_index).value.replace(/,/g, '') === '') ? 0 : document.getElementById(amount_index).value.replace(/,/g, '');
      var qty = parseFloat(amount) / parseFloat(price);
      document.getElementById('delivery_'+index).value = numberWithCommas(Math.round(qty));
      total_amount = 0;
      $("tr.tr_input").each(function() {
        total_amount += (Number.isNaN(parseFloat($(this).find("input.amount").val().replace(/,/g, '')))) ? 0 : parseFloat($(this).find("input.amount").val().replace(/,/g, ''));
      });
      $(".invoice-total").text('₦' + numberWithCommas(total_amount));
    });
    //separate with commas automatically
    $(document).on('keyup', "input.amount", function(event){
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;
      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });
    //Submit invoice
    $(".create-invoice").click(function(){
      //validate form
      var order_id = $('input[name=order-id]');
      var total = $(".invoice-total").text().substr(1).replace(/,/g, '');
      var res = true;
      //Validate form
      //Order reference number
      if (order_id.val() == '') {
        order_id.parent().addClass('has-error');
        res = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Invalid reference number!',
        });
        $("#order_1").val('');
      }
      else{
        order_id.parent().removeClass('has-error');
      }

      $("tr.tr_input").each(function() {
        var code = $(this).find("td.code");
        var delivery = $(this).find("input.delivery");
        var amount = $(this).find("input.amount");
        var qty = $(this).find("td.quantity").text().replace(/,/g, '');
        var price = $(this).find("td.price").text().replace(/,/g, '');
        var amountControl = parseFloat(price) * parseFloat(qty);
        //Validate form
        //Quantity for delivery
        if (delivery.val() ==  0 || delivery.val().replace(/,/g, '') > parseInt(qty)) {
          delivery.parent().addClass('has-error');
          res = false;
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Invalid input!',
          });
        }
        else{
          delivery.parent().removeClass('has-error');
        }
        //Amount
        if (amount.val() ==  0 || amount.val().replace(/,/g, '') > amountControl) {
          amount.parent().addClass('has-error');
          res = false;
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Invalid input!',
          });
        }
        else{
          amount.parent().removeClass('has-error');
        }
        //Code
        if (code.text() == '') {
          res = false;
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Invalid input!',
          });
        }
      });

      if (res) {
        var data = [];
        $("tr.tr_input").each(function() {
          var prodid = $(this).find("input.product-id").val();
          var qty = parseInt($(this).find("input.delivery").val().replace(/,/g, ''));
          data.push({'prodid':prodid, 'qty': qty});
        });
        result = [];
        data.forEach(function (a) {
        if (!this[a.prodid]) {
          this[a.prodid] = { prodid: a.prodid, qty: 0};
          result.push(this[a.prodid]);
        }
        this[a.prodid].qty += parseInt(a.qty);
        }, Object.create(null));

        $.ajax({
          type: 'ajax',
          method: 'post',
          url: "<?php echo base_url('invoices/createInvoice') ?>",
          data: {order_id:order_id.val(),data:result,amount:total},
          dataType: 'json',
          success: function(response){
            console.log(response);
            if(response.success){
              document.getElementById('form').innerHTML = formOriginal;
              Swal.fire({
                type: 'success',
                title: 'Your invoice has been submitted',
                showConfirmButton: false,
                timer: 2000
              });
            }else{
              document.getElementById('form').innerHTML = formOriginal;
              Swal.fire({
                type: 'error',
                title: 'Could not submit invoice',
                showConfirmButton: false,
                timer: 1500
              });
            }
          },
          error: function(){
            document.getElementById('form').innerHTML = formOriginal;
            Swal.fire({
              type: 'error',
              title: 'Error in submitting invoice',
              showConfirmButton: false,
              timer: 1500
            });
          }
        });
      }

    });
  });
</script>
