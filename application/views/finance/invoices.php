<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Invoices and payments</h1>
  <p class="mb-4">Here is a list of all the invoice. To create a new invoice, click on this link: <a href=<?php echo base_url("finance/create_invoice"); ?>>create an invoice</a>.</p>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">List of customers</h6>
    </div>
    <div class="card-body">

      <div id="reportrange" class="rounded" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 23%">
          <i class="fa fa-calendar"></i>&nbsp;
          <span></span> <i class="fa fa-caret-down"></i>
      </div>
      <br>

      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="text-align:center;">
          <thead>
            <tr>
              <th>Invoice N°</th>
              <th>Company name</th>
              <th>Contact person</th>
              <th>Total Amount</th>
              <th>Balance</th>
              <th>Last payment date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Invoice N°</th>
              <th>Company name</th>
              <th>Customer name</th>
              <th>Total Amount</th>
              <th>Balance</th>
              <th>Last payment date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </tfoot>
          <tbody id="invoice-data">

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="approveModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Confirm payment</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>

          <div class="modal-body">
              <form id="editInvoice" action="<?php echo base_url() ?>invoices/approveInvoice" method="post" class="form-horizontal">
                <div class="form-group">
                  <label for="name" class="label-control">Invoice code</label>
                    <input name="invoice-id" class="form-control" readonly value="">
                </div>
                <div class="form-group">
                  <label class="label-control">Payment Type</label>
                  <div class="">
                    <input type="hidden" name="payment-type" class="form-control">
                    <select class="form-control" id="payment-type" required>
                      <option value="cash">Cash</option>
                      <option value="cheque">Cheque</option>
                      <option value="card">Card</option>
                    </select>
                  </div>
                </div>

                <div class="row">

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="name" class="label-control">Current balance</label>
                      <input name="current-balance" class="form-control current-balance" value="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" readonly>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="name" class="label-control">Available credit</label>
                      <input name="credit" class="form-control credit" value="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" readonly>
                      <label>Use credits? * </label>
                      <label class="radio-inline"> Yes <input type="radio" name="optradio" name="credit" value="yes" id="yes"></label>
                      <label class="radio-inline"> No <input type="radio" name="optradio" name="credit" value="no" id="no"></label>
                    </div>
                  </div>

                </div>

                <div class="form-group">
                  <label for="name" class="label-control">Suggested amount : <strong class="lbl-amount"></strong></label>
                  <input name="amount" class="form-control amount" value="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Type 0 if credit is higher than current balance">
                </div>

              </form>
              *If "Yes" and credit is higher than current balance, type 0
          </div>

          <div class="modal-footer">
            <button type="button" id="btnApprove" class="btn btn-success">Approve</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  <div id="detailsModal" style="text-align:center;" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Invoice Details</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
            <table id="invoice-details" class="table table-bordered" style="margin-top: 20px; text-align:center;">
              <thead>
                <thead id="invoice-details-header">
                </thead>
                <tr>
                  <th> Product</th>
                  <th> Price</th>
                  <th> Quantity</th>
                  <th> Unit</th>
                  <th> Amount</th>
                </tr>
              </thead>
              <tbody id="invoice-details-body">
              </tbody>
              <tfoot id="invoice-details-foot">
              </tfoot>
            </table>
            <br>
            <table id="invoice-payment-details" class="table table-bordered" style="margin-top: 20px; text-align:center;">
              <thead>
                <thead id="invoice-payment-details-header">
                  <th colspan="4">Payment details</th>
                </thead>
                <tr>
                  <th> N°</th>
                  <th> Payment type</th>
                  <th> Amount</th>
                  <th> Payment date</th>
                </tr>
              </thead>
              <tbody id="invoice-payment-details-body">
              </tbody>
              <tfoot id="invoice-payment-details-foot">
              </tfoot>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</div>

<style media="screen">
div.dataTables_length {
  margin-right: 1em;
}
</style>

<script type="text/javascript">
  $(function(){
    var invoice_id = null;
    $('#payment-type').selectpicker();
    var balance = 0;
    var credit = 0;
    var yes = false;
    initDateRangePicker();
    showAllInvoices();
    //Show all orders function
    function showAllInvoices(from, to){
      $('#dataTable').DataTable({
        "destroy": true,
        'dom': "<'row'"+
                 "<'col-md-5' l>" +
                 "<'col-md-3' B>" +
                 "<'col-md-4' f>" +
               ">"+
               "<'row'" +
                  "<'col-md-12' t>"+
                ">" +
               "<'row'"+
                 "<'col-md-10' i>"+
               ">",
        'buttons': [{
          extend: 'excelHtml5', className: 'btn-success', text : '<i class="fas fa-download"> Download </i>'
        },
        {
          extend: 'colvis', className: 'btn-primary', text : '<i class="fas fa-columns"> Select columns </i>'
        }
        ],
        "ajax"   : {
          "method"  : 'post',
          "data"    : {"from": from, "to": to},
          "dataType": 'json',
          "url"    : '<?php echo base_url("invoices/getAllInvoices"); ?>',
          "dataSrc": function (data) {
            var return_data = new Array();
            for(var i=0;i< data.length; i++){
              return_data.push({
                'invoice_code'  : data[i].invoice_code,
                'company_name'  : data[i].company_name,
                'customer_name' : data[i].customer_name,
                'total'         : '₦' + numberWithCommas(data[i].total),
                'balance'       : (data[i].balance < 0) ? '₦' + 0 : '₦' + numberWithCommas(data[i].balance),
                'update_date'   : data[i].update_date_time,
                'invoice_id'    : data[i].invoice_id,
                'status'        : data[i].status
              })
            }
            return return_data;
          }
        },
        "columns"    : [
          {'data': 'invoice_code'},
          {'data': 'company_name'},
          {'data': 'customer_name'},
          {'data': 'total'},
          {'data': 'balance'},
          {'data': 'update_date'},
          {'data': 'status'},
          {
            data: null,
            render: function ( data, type, row ) {
              if (data.status == 'approved' || data.status == 'cancelled' || data.status == 'fully paid') {
                return '<a href="javascript:;" style="margin:5px;" id="approved" class="btn btn-info invoice-details" data="'+data.invoice_id+'" >Details</a>';
              }
              else{
                return '<a href="javascript:;" style="margin:5px;" id="approved" class="btn btn-info invoice-details" data="'+data.invoice_id+'" >Details</a>' +
                       '<a href="javascript:;" style="margin:5px;" class="btn btn-success invoice-approve" data-invoice-code="'+data.invoice_code+'" data="'+data.invoice_id+'">Pay</a>';
              }
            }
          }
        ]
      });
    }
    //Show invoice details
    $('#invoice-data').on('click', '.invoice-details', function(){
      var invoice_id = $(this).attr('data');
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>invoices/getInvoiceDetailsById',
        data: {invoice_id: invoice_id},
        dataType: 'json',
        success: function(data){
          var html = '';
          var i, j;
          var total = 0;
          var payment_total = 0;
          //Fill table body
          for (i = 0; i < data.result.length; i++) {
            var amount = (parseFloat(data.result[i].price) * parseInt(data.result[i].quantity))
            html += '<tr>' +
                      '<td>'  + data.result[i].product_name               + '</td>' +
                      '<td>₦' + numberWithCommas(data.result[i].price)    + '</td>' +
                      '<td>'  + numberWithCommas(data.result[i].quantity) + '</td>' +
                      '<td>'  + data.result[i].unit_of_measurement        + '</td>' +
                      '<td>₦' + numberWithCommas(amount)                  + '</td>' +
                    '</tr>';
            total += amount;
          }
          $('#invoice-details-body').html(html);
          //Fill table footer
          var bal = (data.balance.balance < 0) ? '₦' + 0 : '₦' + numberWithCommas(data.balance.balance);
          html = '<tr>' +
                  '<th style="text-align:center" colspan="4">Total amount</th>' +
                  '<td style="text-align:center" >₦' + numberWithCommas(total) + '</td>' +
                '</tr>' +
                '<tr>'  +
                   '<th style="text-align:center" colspan="4">Balance</th>' +
                   '<td style="text-align:center" >' + bal  + '</td>' +
                '</tr>';
          $('#invoice-details-foot').html(html);
          //Fill table header
          html = '<tr>'  +
                   '<th style="text-align:center" colspan="5">Invoice N° : '+data.result[0].invoice_code+'</th>';
                 '</tr>';
          $('#invoice-details-header').html(html);
          //Fill payment details table
          html = '';
          for (j = 0; j < data.payment.length; j++) {
            html += '<tr>' +
                      '<td>'+(j + 1)+'</td>' +
                      '<td>'+data.payment[j].payment_type+'</td>' +
                      '<td>₦'+numberWithCommas(data.payment[j].amount)+'</td>' +
                      '<td>'+data.payment[j].payment_date_time+'</td>' +
                    '</tr>';
            payment_total += parseFloat(data.payment[j].amount);
          }
          $('#invoice-payment-details-body').html(html);
          html = '<tr>' +
                  '<td></td>' +
                  '<th style="text-align:center">Total amount paid</th>' +
                  '<td style="text-align:center" >₦' + numberWithCommas(payment_total) + '</td>' +
                  '<td></td>' +
                '</tr>';
          $('#invoice-payment-details-foot').html(html);
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
      $('#detailsModal').modal('show');
    });
    //Approve invoice
    $('#invoice-data').on('click', '.invoice-approve', function(){
      if ($(this).attr('id') == 'approved' || $(this).attr('id') == 'cancelled') {
        var invoice_status = jsUcfirst($(this).attr('id'));
        $('.alert-success').html('Invoice status : ' + invoice_status).fadeIn().delay(1000).fadeOut('slow');
      }
      else{
        invoice_id = $(this).attr('data');
        var invoice_code = $(this).attr('data-invoice-code');
        var invoice_id_input = $('input[name=invoice-id]');
        invoice_id_input.val(invoice_code);
        $('#approveModal').modal('show');
        $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?php echo base_url() ?>invoices/getInvoiceDetailsById2',
          data: {invoice_id: invoice_id},
          dataType: 'json',
          success: function(data){
            $('.current-balance').val('₦' + numberWithCommas(data.balance.balance));
            $('.credit').val('₦' + numberWithCommas(data.credit.credit));
            $('.lbl-amount').text('₦' + numberWithCommas(data.balance.balance));
            credit = data.credit.credit;
            balance = data.balance.balance;
          },
          error: function(){
            alert('Could not get Data from Database');
          }
        });
      }
    });
    //Confirm invoice approval
    $('#btnApprove').click(function(){
      var result = true;
      //Validate form
      var select = document.getElementById("payment-type");
      var type = select.options[select.selectedIndex].value;
      var payment_type = $('input[name=payment-type]');
      payment_type.val(type);
      var amount = $('input[name=amount]');

      var amountVal = new Number(amount.val().replace(/,/g, ''));
      var suggestedAmount = new Number($('.lbl-amount').text().substring(1).replace(/,/g, ''));

      var url = $('#editInvoice').attr('action');
      var data = $('#editInvoice').serialize();

      if (amount.val() == '' || parseFloat(amountVal) != parseFloat(suggestedAmount)) {
        amount.parent().addClass('has-error');
        result = false;
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Invalid amount!',
        });
      }

      else{
        amount.parent().removeClass('has-error');
      }

      if (result) {
        $.ajax({
          type: 'ajax',
          method: 'get',
          url: url,
          data: {invoice_id: invoice_id, payment_type: payment_type.val(), amount: amount.val().replace(/,/g, ''), credit: yes},
          dataType: 'json',
          success: function(data){
            $('#editInvoice')[0].reset();
            if(data.success){
              $('#approveModal').modal('hide');
              Swal.fire({
                type: 'success',
                title:'Payment',
                text: 'Payment registered successfully',
                showConfirmButton: false,
                timer: 1500
              });
              showAllInvoices();
            }else{
              alert('Error');
            }
          },
          error: function(){
            alert('Could not approve payment');
          }
        });
      }

    });
    $(document).on('keyup', "input.amount", function(event){
      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;
      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        ;
      });
    });
    $('input[type=radio]').change(function(e) {
      var suggestion = 0;
      if (this.id == 'yes') {
        yes = true;
        if (new Number(credit) < new Number(balance)) {
          suggestion = balance - credit;
        }
      }
      if (this.id == 'no') {
        yes = false;
        suggestion = balance;
      }
      $('.lbl-amount').text('₦' + numberWithCommas(suggestion));
    });
    //daterangepicker
    function initDateRangePicker(){
      var start = moment().subtract(29, 'days');
      var end = moment();

      function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today'        : [moment(), moment()],
           'Yesterday'    : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days'  : [moment().subtract(6, 'days'), moment()],
           'Last 30 Days' : [moment().subtract(29, 'days'), moment()],
           'This Month'   : [moment().startOf('month'), moment().endOf('month')],
           'Last Month'   : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      }, cb);

      cb(start, end);
    }
    //filter table by date
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      var from = picker.startDate.format('YYYY-MM-DD');
      var to = picker.endDate.format('YYYY-MM-DD');
      showAllInvoices(from, to);
    });
  })
</script>
