<?php
	/**
	 * Waybill controller that manages all transactions regarding a waybill
	 */
	class Waybills extends CI_Controller{

    public function __construct(){
			parent::__construct();
			$this->load->model('delivery_m', 'd');
      $this->load->model('waybill_m', 'w');
		}
		/**
		 * Get an array of waybills
		 * @return json
		 */
		public function getAllWayBills(){
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($from == null || $to == null) {
				$from = date("Y-m-d");
				$to = date("Y-m-d");
			}
			$result = $this->w->getAllWayBills($from, $to);
			echo json_encode($result);
		}
		/**
		 * Approve a waybill.
		 * Return a boolean indicating if approval was a success
		 * @return json
		 */
    public function approveWaybill(){
      $waybill_id = $this->input->get('waybill_id');
			$result = $this->w->approveWaybill($waybill_id);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Cancel a waybill.
		 * Return a boolean indicating if cancellation was a success
		 * @return json
		 */
    public function cancelWaybill(){
			$waybill_id = $this->input->get('waybill_id');
			$result = $this->w->cancelWaybill($waybill_id);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Get waybill using its waybill id
		 * @return json
		 */
		public function getWayBillDetailsById(){
			$waybill_id = $this->input->post('waybill_id');
			$customer_details = $this->w->getWayBillCustomerDetailsById($waybill_id);
			$product_details = $this->w->getWayBillProductDetailsById($waybill_id);
			if($customer_details && $product_details){
				$data['customer'] = $customer_details;
				$data['product'] = $product_details;
				echo json_encode($data);
			}
		}

		public function getAllWaybills2(){
			$request = $this->input->post('request');
			// Get waybill list
			if($request == 1){
    	  $result = $this->w->getWaybillDetailsByCode();
				foreach ($result as $row) {
					$response[] = array(
						"value"=>$row->waybill_id,
						"label"=>$row->waybill_code
					);
				}
				// encoding array to json format
				echo json_encode($response);
				exit;
			}
			if ($request == 2) {

				$result = $this->w->getWaybillDetailsById();
				// encoding array to json format
			  echo json_encode($result);
			  exit;
			}
			// Get product list
			if($request == 3){
    	  $result = $this->p->getProductDetailsByName();
				foreach ($result as $row) {
					$response[] = array(
						"value"=>$row->product_id,
						"label"=>$row->product_name
					);
				}
				// encoding array to json format
				echo json_encode($response);
				exit;
			}
		}
		/**
		 * Filter list of waybills by date
		 */
		public function filterByDate(){
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			$msg['from'] = $from;
			$msg['to'] = $from;
			echo json_encode($msg);
		}

	}
