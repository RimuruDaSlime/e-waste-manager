<?php

	class Orders extends CI_Controller{

    public function __construct(){
			parent::__construct();
			$this->load->model('order_m', 'o');
			$this->load->model('customer_m', 'c');
			$this->load->model('product_m', 'p');
		}
		/**
		 * Controls the views for sales orders
		 * @param  string $page name of selected page; 'orders' by default
		 * @return null redirects to selected page
		 */
		public function index($page = 'orders'){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			if(!file_exists(APPPATH.'views/sales_orders/'.$page.'.php')){
				show_404();
			}
			$access = false;
			if ($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 2) {
				$access = true;
			}
			if ($page == 'order_approval' && !$access) {
				show_404();
			}
			if ($page == 'orders' && !$access) {
				show_404();
			}
			if ($page == 'place_order' && !$access) {
				show_404();
			}

			$this->load->view('layout/header');
			$this->load->view('sales_orders/' . $page);
			$this->load->view('layout/footer');
    }
		/**
		 * Get order details using order id
		 * @return json returns the details of an order using its order id
		 */
		public function getOrderDetailsById(){
			$order_id = $this->input->get('order_id');
			$result = $this->o->getOrderDetailsById($order_id);
			echo json_encode($result);
		}
		/**
		 * Registers a new order
		 * @return json returns a boolean if the submission was successful
		 */
		public function placeOrder(){
			//Data to send
			$custid = $this->input->post('custid');
			$data = $this->input->post('data');

			$result = $this->o->placeOrder($custid, $data);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Adds a product to an order
		 */
		public function addProductToOrder(){
			$msg['success'] = false;
			$msg['type'] = 'add';
			$result = $this->o->addProductToOrder();
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Get list of Orders
		 * @return json
		 */
		public function getAllOrders(){
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($from == null || $to == null) {
				$from = date("Y-m-d");
				$to = date("Y-m-d");
			}
			$result = $this->o->getAllOrders($from, $to);
			echo json_encode($result);
		}

		public function getAllOrders2(){
			$result = $this->o->getAllOrders2();
			foreach ($result as $row) {
				$tab = array();
				$tab["customer_name"] = $row->customer_name;
				$tab["staff_name"] = $row->staff_name;
				$tab["product_id"] = $row->product_id;
				$tab["quantity"] = $row->quantity;
				$r_tab[] = $tab;
			}
			$output = array(
					"data" =>  $r_tab
			);
			echo json_encode($output);
		}
		/**
		 * Get list of orders for approval
		 * @return json
		 */
		public function getAllOrdersForApproval(){
			$result = $this->o->getAllOrdersForApproval();
			echo json_encode($result);
		}
		/**
		 * Cancel order using its order id.
		 * Returns a boolean indicating if cancellation was succesful
		 * @return json
		 */
		public function cancelOrder(){
			$order_id = $this->input->get('order_id');
			$result = $this->o->cancelOrder($order_id);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Approve order using its order id.
		 * Returns a boolean indicating if approval was succesful
		 * @return json
		 */
		public function approveOrder(){
			$order_id = $this->input->get('order_id');
			$result = $this->o->approveOrder($order_id);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Get number of pending orders
		 * @return json
		 */
		public function getNbPendingOrders(){
			$result = $this->o->getNbPendingOrders();
			echo json_encode($result);
		}
		/**
		 * Get order for a specific invoice
		 * @return json
		 */
		public function getAllOrdersForInvoice(){
			$request = $this->input->post('request');
			$search = $this->input->post('search');
			// Get order list
			if($request == 1){
    	  $result = $this->o->getOrderDetailsByCode($search);
				foreach ($result as $row) {
					$response[] = array(
						"value"=>$row->order_id,
						"label"=>$row->order_code
					);
				}
				// encoding array to json format
				echo json_encode($response);
				exit;
			}
			if ($request == 2) {
				$order_id = $this->input->post('orderid');
				$result = $this->o->getOrderDetailsById($order_id);
				// encoding array to json format
			  echo json_encode($result);
			  exit;
			}
		}

	}
