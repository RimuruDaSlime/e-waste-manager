<?php

	class Customers extends CI_Controller{

    public function __construct(){
			parent::__construct();
			$this->load->model('customer_m', 'c');
		}
		/**
		 * Redirects to customers page
		 */
		public function index(){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			$this->load->view('layout/header');
			$this->load->view('customers/customers');
			$this->load->view('layout/footer');
    }
		/**
		 * Redirects to customer registration
		 */
		public function index2(){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			$this->load->view('layout/header');
			$this->load->view('customers/register_customer');
			$this->load->view('layout/footer');
    }
		/**
		 * Get all registered customers
		 * @return json
		 */
		public function getAllCustomers(){
			$result = $this->c->getAllCustomers();
			echo json_encode($result);
		}
		/**
		 * Gather all data sent from view to transfer to model
		 * in order to register a new customer
		 */
		public function addCustomer(){
			//Customer information
			$company_name 	= $this->input->post('company_name');
			$first_name 		= $this->input->post('first_name');
			$last_name 			= $this->input->post('last_name');
			$email 					= $this->input->post('email');
			$address 				= $this->input->post('address');
			$phone 					= $this->input->post('phone');
			$region 				= $this->input->post('region');
			$city 					= $this->input->post('city');
			$postal_code 		= $this->input->post('postal_code');
			$prod 					= $this->input->post('prod');

			$full_address = $region . ',' . $city . ',' . $address . ',' . $postal_code;

			//Put customer info into array
			$data = array(
				'company_name' => $company_name,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'address' => $full_address,
				'phone' => $phone,
				'staff_id'=> $this->session->userdata('staff_id')
			);

			//Send data to model
			$array = explode(' ', str_replace( ',', ' ', $prod));
			$result = $this->c->addCustomer($data, $array);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Returns randomly picked customer
		 * @return json
		 */
		public function randomPick(){
			$result = $this->c->randomPick();
			echo json_encode($result);
		}
		/**
		 * Returns details of customer using their id
		 * @return json
		 */
		public function getCustomerDetails(){
			$id = $this->input->post('id');
			$result = $this->c->getCustomerDetails($id);
			echo json_encode($result);
		}
		/**
		 * Returns details of customer for the next sales orders
		 * @return json
		 */
		public function getCurrentCustomer(){
			$result = $this->c->getCurrentCustomer();
			echo json_encode($result);
		}

	}
