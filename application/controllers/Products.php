<?php
	/**
	 * Product controller that manages all transactions regarding a product
	 */
	class Products extends CI_Controller{

    public function __construct(){
			parent::__construct();
			$this->load->model('product_m', 'p');
		}
		/**
		 * Redirects to product page
		 * @param  string $page
		 */
    public function index($page = 'products'){
			if(!file_exists(APPPATH.'views/products/'.$page.'.php')){
				show_404();
			}
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			$this->load->view('layout/header');
			$this->load->view('products/'.$page);
			$this->load->view('layout/footer');
    }
		/**
		 * Redirects to product registration page
		 * @param  string $page
		 */
		public function index2($page = 'add_product'){
			if(!file_exists(APPPATH.'views/products/'.$page.'.php')){
				show_404();
			}
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			$this->load->view('layout/header');
			$this->load->view('products/'.$page);
			$this->load->view('layout/footer');
    }
		/**
		 * Get list of registered products
		 * @return json
		 */
		public function getAllProducts(){
			$result = $this->p->getAllProducts();
			echo json_encode($result);
		}
		/**
		 * Get list of products according to what is being searched
		 * @return json
		 */
		public function getAllProducts2(){
			$request = $this->input->post('request');
			$search = $this->input->post('search');
			$id = $this->input->post('id');
			// Get product list
			if($request == 1){
    	  $result = $this->p->getProductDetailsByCode($search,$id);
				foreach ($result as $row) {
					$response[] = array(
						"value"=>$row->product_id,
						"label"=>$row->product_code
					);
				}
				// encoding array to json format
				echo json_encode($response);
				exit;
			}
			if ($request == 2) {

				$result = $this->p->getProductDetailsById();
				$id = $this->input->post('productid');
				//$result['id'] = $id;

				$prods_arr = array();

				foreach ($result as $row) {
					$prods_arr[] = array(
						'product_id' => $row->product_id,
						'product_code' => $row->product_code,
						'product_name' => ucfirst($row->product_name),
						'in_stock' => $row->in_stock,
						'price' => $row->price,
						'uom' => $row->unit_of_measurement
					);
				}
				// encoding array to json format
			  echo json_encode($prods_arr);
			  exit;
			}
			// Get product list
			if($request == 3){
    	  $result = $this->p->getProductDetailsByName($search, $id);
				foreach ($result as $row) {
					$response[] = array(
						"value"=>$row->product_id,
						"label"=>$row->product_name
					);
				}
				// encoding array to json format
				echo json_encode($response);
				exit;
			}
		}
		/**
		 * Register a new product
		 */
    public function addProduct(){
			//Product information
			$category_id = $this->input->post('category-id');
	    $product_name = $this->input->post('product-name');
			$description = $this->input->post('description');
			$price = $this->input->post('price');
			$unit_of_measurement = $this->input->post('measurement-type-value');

			//Put information inside array
			$data = array(
				'category_id' => $category_id,
				'product_name' => strtolower($product_name),
				'description' => $description,
				'price' => $price,
				'unit_of_measurement' => strtolower($unit_of_measurement)
			);

      $result = $this->p->addProduct($data);
  		$msg['success'] = false;
  		$msg['type'] = 'add';
  		if($result){
  			$msg['success'] = true;
  		}
  		echo json_encode($msg);
    }
		/**
		 * Delete product.
		 * Return a boolean indicating if removal was successful
		 * @return json
		 */
		public function removeProduct(){
			$product_id = $this->input->post('product_id');
			$msg['success'] = false;
			if ($product_id != null) {
				$result = $this->p->removeProduct($product_id);
				if($result){
					$msg['success'] = true;
				}
			}
			echo json_encode($msg);
		}
		/**
		 * Get product using its product id
		 * @return json
		 */
		public function getProductById(){
			$result = $this->p->getProductById();
			echo json_encode($result);
		}
		/**
		 * Update product by id.
		 * Returns a boolean indicating if update was successful
		 * @return json
		 */
		public function updateProduct(){
			$product_id = $this->input->get('product_id');
			$result = $this->p->updateProduct();
			$msg['success'] = false;
			$msg['type'] = 'update';
			$msg['id'] = $product_id;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Get all available categories of a product
		 * @return json
		 */
		public function getAllCategories(){
			$result = $this->p->getAllCategories();
			echo json_encode($result);
		}
		/**
		 * Add a new catergory
		 */
		public function addCategory(){
			$category_name = $this->input->post('category-name');
			$category_description = $this->input->post('category-description');
			$msg['success'] = false;
			$msg['type'] = 'add';

			if ($category_name != null && $category_description != null) {
				$data = array(
					'category_name' => $category_name,
					'description' => $category_description
				);
				$result = $this->p->addCategory($data);
				if ($result) {
					$msg['success'] = true;
				}
			}
			echo json_encode($msg);
		}
		/**
		 * Submit a new inbound
		 * Return a boolean indicating if submission was successful
		 * @return json
		 */
		public function submitInbound(){
			$data = $this->input->post('data');
			$staff_id = $this->input->post('staffid');
			$msg['success'] = false;
			$result = $this->p->submitInbound($staff_id, $data);
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Get all inbounds for approval.
		 * Returns list of inbounds for approval
		 * @return json.
		 */
		public function getAllInboundsForApproval(){
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($from == null || $to == null) {
				$from = date("Y-m-d");
				$to = date("Y-m-d");
			}
			$result = $this->p->getAllInboundsForApproval($from, $to);
			echo json_encode($result);
		}
		/**
		 * Get an inbound using its inbound id
		 * @return json
		 */
		public function getInboundDetailsById(){
			$inbound_id = $this->input->get('inbound_id');
			$result = $this->p->getInboundDetailsById($inbound_id);
			echo json_encode($result);
		}
		/**
		 * Approve an inbound.
		 * Return a boolean indicating if approval successful
		 * @return json
		 */
		public function approveInbound(){
			$inbound_id = $this->input->post('inbound_id');
			$note = $this->input->post('approval_note');
			$result = $this->p->approveInbound($inbound_id, $note);
			$msg['success'] = false;
			if ($result) {
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}

	}
