<?php

	class Deliveries extends CI_Controller{

    public function __construct(){
			parent::__construct();
			$this->load->model('delivery_m', 'd');
		}
		/**
		 * Redirects to the selected page in relation to deliveries
		 * @param string $page
		 */
		public function index($page = 'orders'){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			if(!file_exists(APPPATH.'views/warehouse/'.$page.'.php')){
				show_404();
			}
			$access = false;
			if ($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 3) {
				$access = true;
			}
			if ($page == 'delivery_approval' && !$access) {
				show_404();
			}
			if ($page == 'inbound_approval' && !$access) {
				show_404();
			}
			if ($page == 'deliveries' && !$access) {
				show_404();
			}
			$this->load->view('layout/header');
			$this->load->view('warehouse/' . $page);
			$this->load->view('layout/footer');
    }
		/**
		 * Returns list of deliveries
		 * @return json
		 */
		public function getAllDeliveries(){
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($from == null || $to == null) {
				$from = date("Y-m-d");
				$to = date("Y-m-d");
			}
			$result = $this->d->getAllDeliveries($from, $to);
			echo json_encode($result);
		}
		/**
		 * Get all pending delivery orders for approval
		 * @return json
		 */
		public function getAllDeliveriesForApproval(){
			$result = $this->d->getAllDeliveriesForApproval();
			echo json_encode($result);
		}
		/**
		 * Returns details of delivery using their id
		 * @return json
		 */
		public function getDeliveryDetailsByID(){
			$delivery_id = $this->input->get('delivery_id');
			$result = $this->d->getDeliveryDetailsByID($delivery_id);
			echo json_encode($result);
		}
		/**
		 * Gathers data sent from the view
		 * and calls model function to approve a delivery.
		 * Returns a boolean if the approval was successful
		 * @return json
		 */
		public function approveDelivery(){
			$msg['success'] = false;
			$delivery_id = $this->input->post('id');
			$note = $this->input->post('note');
			$result = $this->d->approveDelivery($delivery_id,$note);
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Gathers data from view
		 * and calls model function to save delivery details.
		 * Returns a boolean if the delivery details was saved successfully
		 * @return json
		 */
		public function saveDeliveryDetails(){
			$delivery_id = $this->input->post('id');
			$data = $this->input->post('data');
			$note = $this->input->post('note');
			if (!empty($delivery_id) && !empty($data)) {
				$result = $this->d->saveDeliveryDetails($delivery_id,$data,$note);
				if($result){
					$msg['success'] = true;
				}
			}
			echo json_encode($msg);
		}
		/**
		 * Cancels a delivery using delivery id.
		 * Returns boolean if the order was cancelled successfully
		 * @return json
		 */
		public function cancelDelivery(){
			$delivery_id = $this->input->get('delivery_id');
			$result = $this->d->cancelDelivery($delivery_id);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Get list of waybills.
		 * @return json
		 */
		public function getAllWayBills(){
			$result = $this->d->getAllWayBills();
			echo json_encode($result);
		}
		/**
		 * Get number of pending deliveries.
		 * @return json
		 */
		public function getNbPendingDeliveries(){
			$result = $this->d->getNbPendingDeliveries();
			echo json_encode($result);
		}

	}
