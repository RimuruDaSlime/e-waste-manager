<?php

	class Invoices extends CI_Controller{

    public function __construct(){
			parent::__construct();
      $this->load->model('invoice_m', 'i');
		}
		/**
		 * Redirects to a selected page in relation with finance
		 * @param  string $page
		 */
		public function index($page = 'invoice'){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			if(!file_exists(APPPATH.'views/finance/'.$page.'.php')){
				show_404();
			}
			$access = false;
			if ($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 2 || $this->session->userdata('role_id') == 4) {
				$access = true;
			}
			if ($page == 'create_invoice' && !$access) {
				show_404();
			}
			if ($page == 'invoices' && !$access) {
				show_404();
			}
			$this->load->view('layout/header');
			$this->load->view('finance/' . $page);
			$this->load->view('layout/footer');
    }
		/**
		 * Gets list of invoices
		 * @return json
		 */
		public function getAllInvoices(){
			$from = $this->input->post('from');
			$to = $this->input->post('to');
			if ($from == null || $to == null) {
				$from = date("Y-m-d");
				$to = date("Y-m-d");
			}
			$result = $this->i->getAllInvoices($from, $to);
			echo json_encode($result);
		}
		/**
		 * Approves invoice.
		 * Returns boolean if the approval was successful
		 * @return json
		 */
		public function approveInvoice(){
			$invoice_id = $this->input->get('invoice_id');
			$payment_type = $this->input->get('payment_type');
			$amount = $this->input->get('amount');
			$yes = $this->input->get('credit');

			if ($amount == 0 && $yes) {
				$res = $this->i->getBalanceById($invoice_id);
				$amount = $res->balance;
				$this->i->reduceCompanyCredit($amount, $invoice_id);
			}else if ($amount > 0 && $yes) {
				$res = $this->i->getCompanyCreditByInvoiceId($invoice_id);
				$credit = $res->credit;
				$amount = $amount + $credit;
				$this->i->updateCustomerCredit(0, $invoice_id);
			}

			$data = array(
				'invoice_id' => $invoice_id,
				'payment_type' => $payment_type,
				'amount' => $amount
			);

			$result = $this->i->approveInvoice($invoice_id, $payment_type, $amount);
			$msg['success'] = false;

			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Create an invoice.
		 * Returns a boolean if creation was successful
		 * @return json
		 */
		public function createInvoice(){
			$order_id = $this->input->post('order_id');
			$amount = $this->input->post('amount');
			$data = $this->input->post('data');
			$result = $this->i->createInvoice($order_id, $amount, $data);
			$msg['success'] = false;
			if($result){
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Gets invoice details using invoice id
		 * @return json
		 */
		public function getInvoiceDetailsById(){
			$invoice_id = $this->input->get('invoice_id');
			$result = $this->i->getInvoiceDetailsById($invoice_id);
			$balance = $this->i->getBalanceById($invoice_id);
			$payment = $this->i->getPaymentDetailsByInvoiceId($invoice_id);
			$data['result'] = $result;
			$data['balance'] = $balance;
			$data['payment'] = $payment;
			echo json_encode($data);
		}
		/**
		 *
		 *
		 */
		public function getInvoiceDetailsById2(){
			$invoice_id = $this->input->get('invoice_id');
			$credit = $this->i->getCompanyCreditByInvoiceId($invoice_id);
			$balance = $this->i->getBalanceById($invoice_id);
			$data['credit'] = $credit;
			$data['balance'] = $balance;
			echo json_encode($data);
		}
		/**
		 * Get sales overview for the current year
		 * @return json
		 */
		public function getSalesOveriew(){
			$result = $this->i->getSalesOverview();
			$data = array();
			if (count($result) != 0) {
				foreach ($result as $key => $value) {
					$data[$value['SalesMonth']] = $value['TotalSales'];
				}
			}
			for ($i=1; $i <= 12; $i++) {
				if (!array_key_exists($i, $data)) {
					$data[$i] = 0;
				}
			}
			ksort($data);
			echo json_encode($data);
		}
		/**
		 * Get best selling product for the current year
		 * @return json
		 */
		public function getBestSellingProduct(){
			$result = $this->i->getBestSellingProduct();
			echo json_encode($result);
		}
		/**
		 * Get least selling product for the current year
		 * @return json
		 */
		public function getLeastSellingProduct(){
			$result = $this->i->getLeastSellingProduct();
			echo json_encode($result);
		}
		/**
		 * Get current balance of an invoice
		 * @return json
		 */
		public function getBalanceById(){
			$invoice_id = $this->input->get('invoice_id');
			$balance = $this->i->getBalanceById($invoice_id);
			$data['balance'] = $balance;
			echo json_encode($data);
		}
		/**
		 * Get total credit for a company
		 * @return
		 */
		public function getCompanyCreditByInvoiceId(){
			$invoice_id = $this->input->get('invoice_id');
			$credit = $this->i->getCompanyCreditByInvoiceId($invoice_id);
			$data['credit'] = $credit;
			echo json_encode($data);
		}
		/**
		 * Get 4 best selling products
		 * @return json
		 */
		public function getRevenueSources(){
			$highestSellers = $this->i->getHighestSeller();
			$highestSellersNames = $this->i->getHighestSellerNames();
			$msg['highestSellers'] = $highestSellers;
			$msg['highestSellersNames'] = $highestSellersNames;
			echo json_encode($msg);
		}

	}
