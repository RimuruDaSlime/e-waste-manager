<?php
	class Pages extends CI_Controller{

		public function __construct(){
			parent::__construct();
		}
		/**
		 * Redirect to home page
		 * @param  string $page
		 */
		public function index($page = 'home'){
			if(!file_exists(APPPATH.'views/pages/'.$page.'.php')){
				show_404();
			}
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			$this->load->view('layout/header');
			$this->load->view('pages/'.$page);
			$this->load->view('layout/footer');
			unset($_SESSION['id']);
		}
	}
