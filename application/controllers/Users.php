<?php
/**
 * User controller that manages all transactions regarding a user
 */
	class Users extends CI_Controller{
    public function __construct(){
			parent::__construct();
			$this->load->model('user_m', 'u');
		}

		/**
		 * Redirect to users page
		 */
		public function users(){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			if($this->session->userdata('role_id') == 2){
				show_404();
			}
			$this->load->view('layout/header');
			$this->load->view('users/users');
			$this->load->view('layout/footer');
		}

		/**
		 * Redirect to user registration page
		 */
		public function register_user(){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			if($this->session->userdata('role_id') == 2){
				show_404();
			}
			$this->load->view('layout/header');
			$this->load->view('users/signup');
			$this->load->view('layout/footer');
		}

		/**
		 * Redirect to user edit page
		 */
		public function edit_profile(){
			// Check login
			if(!$this->session->userdata('logged_in')){
				redirect('users/login');
			}
			$this->load->view('layout/header');
			$this->load->view('users/edit_profile');
			$this->load->view('layout/footer');
		}

		/**
		 * Register a new user
		 *
		 */
		public function signup(){
			$enc_password = md5($this->input->post('password'));
			$data = array(
				'first_name' => $this->input->post('first_name'),
        'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
        'username' => $this->input->post('username'),
				'role_id' => $this->input->post('privilege'),
        'password' => $enc_password,
			);
			$msg['success'] = false;
			$result = $this->u->signup($data);
			if ($result) {
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
		/**
		 * Login to the application
		 */
		public function login(){
			$data['title'] = 'Log In';
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() === FALSE){
				$this->load->view('users/login', $data);
			} else {

				// Get username
				$username = $this->input->post('username');
				// Get and encrypt the password
				$password = md5($this->input->post('password'));
				// Login user
				$user = $this->u->login($username, $password);
				if($user['id']){
					// Create session
					$user_data = array(
						'staff_id' => $user['id'],
            'name' => $user['name'],
						'username' => $username,
						'logged_in' => true,
						'img' => $user['img'],
						'role_id' => $user['role_id']
					);
					$this->session->set_userdata($user_data);
					// Set message
					redirect('home');
				} else {
					// Set message
					redirect('users/login');
				}
			}
		}
		/**
		 * Logout from the application
		 */
		public function logout(){
			// Unset user data
			$this->session->unset_userdata('logged_in');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('username');
			// Set message
			$this->session->set_flashdata('user_loggedout', 'You are now logged out');
			redirect('users/login');
		}
		/**
		 * Checks if username already exists
		 * @param  string $username
		 * @return boolean
		 */
		public function check_username_exists($username){
			$this->form_validation->set_message('check_username_exists', 'That username is taken. Please choose a different one');
			if($this->u->check_username_exists($username)){
				return true;
			} else {
				return false;
			}
		}
		/**
		 * Check if email already exists
		 * @param  string $email
		 * @return boolean
		 */
		public function check_email_exists($email){
			$this->form_validation->set_message('check_email_exists', 'That email is taken. Please choose a different one');
			if($this->u->check_email_exists($email)){
				return true;
			} else {
				return false;
			}
		}
		/**
		 * Get user using its user id
		 * @return json
		 */
		public function getUserInfo(){
			$id = $this->input->post('id');
			$result = $this->u->getUserInfo($id);
			if ($result) {
				echo json_encode($result);
			}
		}
		/**
		 * Edit user profile information.
		 * Return a boolean indicating if update was successful
		 * @param  string $upload_image
		 * @return json
		 */
		public function edit($upload_image = 'users.png'){
			$id = $this->input->post('id');
			$enc_password = md5($this->input->post('password'));
			$data = array(
				'first_name' => $this->input->post('first-name'),
        'last_name' => $this->input->post('last-name'),
				'email' => $this->input->post('email'),
        'username' => $this->input->post('username'),
        'password' => $enc_password,
				'img' => $upload_image
			);

			$msg['success'] = false;
			$result = $this->u->edit($id, $data);
			if ($result) {

				//update profile picture
				unset($_SESSION['img']);
				$this->session->set_userdata('img', $upload_image);

				$msg['success'] = true;
				$msg['img'] = $this->session->userdata('img');
			}
			echo json_encode($msg);
		}
		/**
		 * Upload user profile image
		 */
		public function upload_image(){
			//Upload image
			$config['upload_path'] = './assets/images/users/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '100000000';
			$config['max_width'] = '100000';
			$config['max_height'] = '100000';

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('userfile')) {
				$errors = array('error' => $this->upload->display_errors());
				$post_image = 'noimage.jpg';
			}
			else{
				$data = array('upload_data' => $this->upload->data());
				$post_image = $_FILES['userfile']['name'];
			}

			redirect('users/edit_profile');
		}
		/**
		 * Gets list of users
		 * @return json
		 */
		public function getAllusers(){
			$result = $this->u->getAllUsers();
			echo json_encode($result);
		}
		/**
		 * View staff info
		 * @return json
		 */
		public function viewStaffInfo(){
			$id = $this->input->post('id');
			$this->session->set_userdata('id', $id);
			echo json_encode($id);
		}
		/**
		 * Delete a user
		 * @return json
		 */
		public function deleteUser(){
			$id = $this->input->post('id');
			$result = $this->u->deleteUser($id);
			$msg['success'] = false;
			if ($result) {
				$msg['success'] = true;
			}
			echo json_encode($msg);
		}
	}
