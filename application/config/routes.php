<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['customers/customers'] = 'customers/index';
$route['customers/register_customer'] = 'customers/index2';

$route['products/products'] = 'products/index';
$route['products/add_product'] = 'products/index2';

$route['sales_orders/(:any)'] = 'orders/index/$1';

$route['warehouse/(:any)'] = 'deliveries/index/$1';

$route['finance/(:any)'] = 'invoices/index/$1';

$route['default_controller'] = 'pages/index';

$route['(:any)'] = 'pages/index/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
