<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Waybill model that manages all database transactions regarding a waybill
 */
class Waybill_m extends CI_Model{
  /**
   * Get list of waybill
   * @param  date $from
   * @param  date $to
   * @return array
   */
  public function getAllWayBills($from, $to){

    $query = $this->db->query("SELECT w.waybill_id,
                                      w.waybill_code,
                                      w.description,
                                      w.amount,
                                      w.status,
                                      (SELECT CONCAT(first_name, ' ', last_name)
                                       FROM tbl_staff
                                       WHERE staff_id = w.staff_id) AS staff_name,
                                      w.approve_date_time,
                                      w.cancel_date_time,
                                      w.approve_date,
                                      w.cancel_date
                                      FROM tbl_waybill w
                                      WHERE w.status NOT IN ('cancelled')
                                      AND w.update_date BETWEEN '$from' AND '$to'");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    else{
      return false;
    }
  }
  /**
   * Get waybill details by code
   * @return array
   */
  public function getWaybillDetailsByCode(){
		$search = $this->input->post('search');
    $query = $this->db->query("SELECT *
                               FROM tbl_waybill
                               WHERE waybill_code LIKE '$search%'
                               AND status NOT IN ('awaiting approval', 'cancelled')
                               AND waybill_id NOT IN
                               (SELECT waybill_id FROM tbl_invoice)");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
  /**
   * Get waybill details by ID
   * @return array
   */
  public function getWaybillDetailsById(){
		$id = $this->input->post('waybillid');
    $query = $this->db->query("SELECT dd.product_id, dd.quantity, p.product_name, p.unit_of_measurement,
                                  		(SELECT od.price
                                       FROM tbl_order_detail od
                                       WHERE od.order_id = d.order_id
                                       AND od.product_id = dd.product_id) AS price
                              FROM tbl_waybill w
                              JOIN tbl_delivery_detail dd ON w.delivery_id = dd.delivery_id
                              JOIN tbl_delivery d ON d.delivery_id = dd.delivery_id
                              JOIN tbl_product p ON p.product_id = dd.product_id
                              WHERE w.waybill_id = $id");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
  /**
   * Approve waybill
   * @param  int $waybill_id
   * @return boolean
   */
  public function approveWaybill($waybill_id){
    $data = array(
			'status'            => 'approved',
			'staff_id'          => $this->session->userdata('staff_id'),
			'approve_date'      => date('Y-m-d'),
			'approve_date_time' => date('Y-m-d H:i:s'),
      'update_date'       => date('Y-m-d'),
      'update_date_time'  => date('Y-m-d')
		);
    //Begin transaction
    $this->db->trans_begin();

		$this->db->where('waybill_id', $waybill_id);
		$this->db->update('tbl_waybill', $data);

    $query = $this->db->query("SELECT d.delivery_id, w.status, d.status
                               FROM tbl_waybill w
                               JOIN tbl_delivery d ON d.delivery_id = w.delivery_id
                               WHERE w.waybill_id = $waybill_id");

    $row = $query->row();

    $data = array(
			'status'            => 'ready for delivery',
      'update_date'       => date('Y-m-d'),
      'update_date_time'  => date('Y-m-d')
		);

    $this->db->where('delivery_id', $row->delivery_id);
    $this->db->update('tbl_delivery', $data);

    //Check status and return accordingly
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
  }
  /**
   * Cancel a waybill
   * @param  int $waybill_id
   * @return boolean
   */
  public function cancelWaybill($waybill_id){

    $data = array(
      'status' => 'cancelled',
      'staff_id' => $this->session->userdata('staff_id'),
      'cancel_date' => date('Y-m-d'),
      'cancel_date_time' => date('Y-m-d H:i:s'),
      'approve_date' => null,
      'approve_date_time' => null
    );

    //********************Condition for cancelling*********************************//
		//Cancel if invoice status != 'approved'
		//Or waybill has not yet been approved

    $query = $this->db->query("SELECT * FROM tbl_invoice WHERE waybill_id = $waybill_id");
    if ($query->num_rows() > 0){
      $row = $query->row();
      if ($row->status != 'approved'){
        //Update tbl_waybill
        $this->db->where('waybill_id', $waybill_id);
        $this->db->update('tbl_waybill', $data);
        //Update tbl_delivery
        if ($this->db->affected_rows() > 0) {
          $query = $this->db->query("SELECT delivery_id FROM tbl_waybill WHERE waybill_id = $waybill_id");
          if ($query->num_rows() > 0) {
            $row = $query->row();
            $field = array(
              'status' => 'awaiting approval',
              'staff_id' => null,
              'cancel_date' => null,
              'cancel_date_time' => null,
              'approve_date' => null,
              'approve_date_time' => null
            );
            $this->db->set($field);
            $this->db->where('delivery_id', $row->delivery_id);
            $this->db->update('tbl_delivery');

            if ($this->db->affected_rows() > 0) {
              return true;
            }
          }
        }
      }
    }
    else{
      //Update tbl_waybill
      $this->db->where('waybill_id', $waybill_id);
      $this->db->update('tbl_waybill', $data);
      //Update tbl_delivery
      if ($this->db->affected_rows() > 0) {
        $query = $this->db->query("SELECT delivery_id FROM tbl_waybill WHERE waybill_id = $waybill_id");
        if ($query->num_rows() > 0) {
          $row = $query->row();
          $field = array(
            'status' => 'awaiting approval',
            'staff_id' => null,
            'cancel_date' => null,
            'cancel_date_time' => null,
            'approve_date' => null,
            'approve_date_time' => null
          );
          $this->db->set($field);
          $this->db->where('delivery_id', $row->delivery_id);
          $this->db->update('tbl_delivery');

          if ($this->db->affected_rows() > 0) {
            return true;
          }
        }
      }
    }
    return false;
  }
  /**
   * Get waybill customer details by ID
   * @param  int $waybill_id
   * @return array
   */
  public function getWayBillCustomerDetailsById($waybill_id){
    $query = $this->db->query("SELECT w.waybill_id,
                                      w.waybill_code,
                                      w.approve_date_time,
                                      w.approve_date,
                                      CONCAT(c.first_name, ' ', c.last_name) AS customer_name,
                                      c.address,
                                      c.phone,
                                      c.email
                               FROM tbl_waybill w
                               JOIN tbl_delivery d ON d.delivery_id = w.delivery_id
                               JOIN tbl_order o ON o.order_id = d.order_id
                               JOIN tbl_customer c ON c.customer_id = o.customer_id
                               WHERE w.waybill_id = $waybill_id");
     if ($query->num_rows() > 0) {
       return $query->result_array();
     }
     else{
       return false;
     }
  }
  /**
   * Get waybill product details by Id
   * @param  int $waybill_id
   * @return array
   */
  public function getWayBillProductDetailsById($waybill_id){
    $query = $this->db->query("SELECT p.product_name,
                                      cat.category_name,
                                      p.description,
                                      dd.quantity,
                                      p.unit_of_measurement,
                                      p.product_code
                               FROM tbl_product p
                               JOIN tbl_delivery_detail dd ON dd.product_id = p.product_id
                               JOIN tbl_waybill w ON w.delivery_id = dd.delivery_id
                               JOIN tbl_category cat ON cat.category_id = p.category_id
                               WHERE w.waybill_id = $waybill_id");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    else{
      return false;
    }
  }
}
