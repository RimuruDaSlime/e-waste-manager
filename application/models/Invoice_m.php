<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Invoice model that manages all database transactions regarding an invoice
 */
class Invoice_m extends CI_Model{

  public function __construct(){
    parent::__construct();
    $this->load->model('invoice_m', 'i');
  }
  /**
   * Get list of invoices
   * @param  date $from
   * @param  date $to
   * @return array
   */
  public function getAllInvoices($from, $to){
    $query = $this->db->query("SELECT i.invoice_id,
                                      i.invoice_code,
                                	    CONCAT(c.first_name,' ', c.last_name) AS customer_name,
                                      c.company_name,
                                      i.total,
                                      i.balance,
                                      i.update_date_time,
                                      i.status
                               FROM tbl_customer c
                               JOIN tbl_order o ON o.customer_id = c.customer_id
                               JOIN tbl_delivery d ON d.order_id = o.order_id
                               JOIN tbl_invoice i ON i.order_id = o.order_id
                               AND i.status != 'cancelled'
                               AND i.update_date BETWEEN '$from' AND '$to'");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    else{
      return false;
    }
  }
  /**
   * Generate a new invoice
   * @param  int $order_id
   * @param  double $amount
   * @param  array $data
   * @return boolean
   */
  public function createInvoice($order_id, $amount, $data){
    $field = array(
      'order_id' => $order_id,
      'total' => $amount,
      'balance' => $amount,
      'update_date' => date('Y-m-d'),
      'update_date_time' => date('Y-m-d H:i:s')
    );
    //Begin transaction
    $this->db->trans_begin();

    $this->db->insert('tbl_invoice', $field);
    $invoice_id = $this->db->insert_id();

    $invoice_code = 'IN' . date("Y") . date("m") . date("d")  . sprintf("%04d", $invoice_id);
		$this->db->set('invoice_code', $invoice_code);
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('tbl_invoice');

    $query = $this->db->query("SELECT d.delivery_id FROM tbl_delivery d WHERE d.order_id = $order_id");
    $row = $query->row();

    foreach ($data as $key => $value) {
      $this->db->set('quantity', $value['qty']);
      $this->db->where('product_id', $value['prodid']);
      $this->db->where('delivery_id', $row->delivery_id);
      $this->db->update('tbl_delivery_detail');
    }

    $field = array(
      'status' => 'processing',
      'update_date' => date('Y-m-d'),
      'update_date_time' => date('Y-m-d H:i:s')
    );
    $this->db->where('delivery_id', $row->delivery_id);
    $this->db->update('tbl_delivery', $field);

    foreach ($data as $key => $value) {
      $id = $value['prodid'];
      $qty = $value['qty'];
      $query = $this->db->query("SELECT od.quantity, p.allocated, p.in_order, p.in_stock
                                  FROM tbl_order_detail od
                                  JOIN tbl_product p ON p.product_id = od.product_id
                                  WHERE od.product_id = $id
                                  AND od.order_id = $order_id");
      $row = $query->row();
      $field = array(
        'allocated' => $row->allocated - $row->quantity,
        'in_stock' => $row->in_stock - $qty,
        'in_order' => $row->in_order + $qty
      );
      $this->db->where('product_id', $id);
      $this->db->update('tbl_product', $field);
    }

    //If all goes well, return true else return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
  }
  /**
   * Approve an invoice
   * @param  int $invoice_id
   * @param  string $payment_type
   * @param  double $amount
   * @return boolean
   */
  public function approveInvoice($invoice_id, $payment_type, $amount){
    //Begin transaction
    $this->db->trans_begin();

    //Update balance of tbl_invoice where invoice_id = $invoice_id
    $this->db->where('invoice_id', $invoice_id);
    $query = $this->db->get('tbl_invoice');
    $row = $query->row();
    $balance = ((int)$row->balance - (int)$amount);
    if ($balance > 0) {
      $data = array(
        'balance' => $balance,
  			'staff_id' => $this->session->userdata('staff_id'),
  			'update_date' => date('Y-m-d'),
  			'update_date_time' => date('Y-m-d H:i:s')
  		);
      $this->db->set($data);
      $this->db->where('invoice_id', $invoice_id);
      $this->db->update('tbl_invoice');
    }
    else{
      $data = array(
        'status' => 'approved',
        'balance' => $balance,
  			'staff_id' => $this->session->userdata('staff_id'),
  			'update_date' => date('Y-m-d'),
  			'update_date_time' => date('Y-m-d H:i:s'),
        'approve_date' => date('Y-m-d'),
  			'approve_date_time' => date('Y-m-d H:i:s')
  		);
      $this->db->set($data);
      $this->db->where('invoice_id', $invoice_id);
      $this->db->update('tbl_invoice');

      if ($balance < 0) {
        $query = $this->db->query("SELECT c.credit, c.customer_id
                                    FROM tbl_invoice i
                                    JOIN tbl_order o ON o.order_id = i.order_id
                                    JOIN tbl_customer c ON c.customer_id = o.customer_id
                                    WHERE i.invoice_id = $invoice_id;");
        $row = $query->row();
        $credit = abs($balance) + $row->credit;
        $field = array(
          'credit' => $credit
        );
        $this->db->set($field);
        $this->db->where('customer_id', $row->customer_id);
        $this->db->update('tbl_customer');
      }
    }

    //Insert into tbl_payment
    $field = array(
      'invoice_id' => $invoice_id,
      'amount' => $amount,
      'payment_type' => $payment_type,
      'payment_date' => date('Y-m-d'),
      'payment_date_time' => date('Y-m-d H:i:s')
    );
    $this->db->insert('tbl_payment', $field);

    $query = $this->db->query("SELECT i.total, d.delivery_id, c.company_name, c.address
                                FROM tbl_invoice i
                                JOIN tbl_delivery d ON d.order_id = i.order_id
                                JOIN tbl_order o ON o.order_id = i.order_id
                                JOIN tbl_customer c ON c.customer_id = o.customer_id
                                WHERE i.invoice_id = $invoice_id");

    $result = $query->row();
    $description = "Delivery for " . $result->company_name . " to " . $result->address;

    $field = array(
      'status' => "awaiting approval",
      'delivery_id' => $result->delivery_id,
      'amount' => $amount,
      'description' => $description,
      'staff_id' => $this->session->userdata('staff_id'),
      'approve_date' => date('Y-m-d'),
			'approve_date_time' => date('Y-m-d H:i:s'),
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
    );

    $this->db->insert('tbl_waybill', $field);
    $waybill_id = $this->db->insert_id();

    $waybill_code = 'WB' . date("Y") . date("m") . date("d")  . sprintf("%04d", $waybill_id);
		$this->db->set('waybill_code', $waybill_code);
		$this->db->where('waybill_id', $waybill_id);
		$this->db->update('tbl_waybill');

    //If all goes well, return true else return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
  }
  /**
   * Cancel an invoice
   * @param  int $invoice_id
   * @return boolean
   */
  public function cancelInvoice($invoice_id){

    $data = array(
      'status' => 'cancelled',
      'staff_id' => $this->session->userdata('staff_id'),
      'cancel_date' => date('Y-m-d'),
      'cancel_date_time' => date('Y-m-d H:i:s'),
      'approve_date' => null,
      'approve_date_time' => null
    );

    //********************Condition for cancelling*********************************//
		//Cancel if payment status != 'approved'
		//Or invocie has not yet been approved

    $query = $this->db->query("SELECT * FROM tbl_paymen WHERE invoice_id = $invoice_id");
    if ($query->num_rows() > 0){
      $row = $query->row();
      if ($row->status != 'approved'){
        //Update tbl_invoice
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('tbl_invoice', $data);
        //Update tbl_payment
        if ($this->db->affected_rows() > 0) {
          $query = $this->db->query("SELECT waybill_id FROM tbl_invoice WHERE invoice_id = $invoice_id");
          if ($query->num_rows() > 0) {
            $row = $query->row();
            $field = array(
              'status' => 'awaiting approval',
              'staff_id' => null,
              'cancel_date' => null,
              'cancel_date_time' => null,
              'approve_date' => null,
              'approve_date_time' => null
            );
            $this->db->set($field);
            $this->db->where('waybill_id', $row->waybill_id);
            $this->db->update('tbl_waybil');

            if ($this->db->affected_rows() > 0) {
              return true;
            }
          }
        }
      }
    }
    else{
      //Update tbl_invoice
      $this->db->where('invoice_id', $invoice_id);
      $this->db->update('tbl_invoice', $data);
      //Update tbl_waybill
      if ($this->db->affected_rows() > 0) {
        $query = $this->db->query("SELECT waybill_id FROM tbl_invoice WHERE invoice_id = $invoice_id");
        if ($query->num_rows() > 0) {
          $row = $query->row();
          $field = array(
            'status' => 'awaiting approval',
            'staff_id' => null,
            'cancel_date' => null,
            'cancel_date_time' => null,
            'approve_date' => null,
            'approve_date_time' => null
          );
          $this->db->set($field);
          $this->db->where('waybill_id', $row->waybill_id);
          $this->db->update('tbl_waybill');

          if ($this->db->affected_rows() > 0) {
            return true;
          }
        }
      }
    }
    return false;
  }
  /**
   * Get invoice details by ID
   * @param  id $invoice_id
   * @return array
   */
  public function getInvoiceDetailsById($invoice_id){
    $query = $this->db->query("SELECT dd.product_id, dd.quantity, p.unit_of_measurement,p.product_name,i.invoice_code,
                                    (SELECT od.price
                                     FROM tbl_order_detail od
                                     WHERE od.product_id = dd.product_id
                                     AND od.order_id = d.order_id) as price
                                FROM tbl_invoice i
                                JOIN tbl_delivery d ON d.order_id = i.order_id
                                JOIN tbl_delivery_detail dd ON dd.delivery_id = d.delivery_id
                                JOIN tbl_product p ON p.product_id = dd.product_id
                                WHERE i.invoice_id = $invoice_id");
    if ($query->num_rows() > 0) {
      return $query->result();
    }
    return false;
  }
  /**
   * Get current balance of invoice
   * @param  int $invoice_id
   * @return array
   */
  public function getBalanceById($invoice_id){
    $query = $this->db->query("SELECT balance FROM tbl_invoice WHERE invoice_id = $invoice_id");
    if ($query->num_rows() > 0) {
      return $query->row();
    }
    return false;
  }
  /**
   * Get sales overview for current year
   * @return array
   */
  public function getSalesOverview(){
    $query = $this->db->query("SELECT SUM(p.amount) AS TotalSales, MONTH(i.approve_date) AS SalesMonth
                               FROM tbl_invoice i
                               JOIN tbl_payment p ON i.invoice_id = p.invoice_id
                               WHERE YEAR(i.approve_date) = YEAR(CURDATE())
                               GROUP BY MONTH(i.approve_date)
                              ORDER BY MONTH(i.approve_date)");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    return array();
  }
  /**
   * Get best selling product
   * @return array
   */
  public function getBestSellingProduct(){
    $query = $this->db->query("SELECT p.product_name, SUM(dd.quantity) as qty
                                FROM tbl_invoice i
                                JOIN tbl_delivery d ON d.order_id = i.order_id
                                JOIN tbl_delivery_detail dd ON dd.delivery_id = d.delivery_id
                                JOIN tbl_product p ON p.product_id = dd.product_id
                                WHERE i.status = 'approved'
                                GROUP BY dd.product_id
                                ORDER BY qty DESC
                                LIMIT 1");
    if ($query->num_rows() > 0) {
     return $query->result_array();
    }
    return array();
  }
  /**
   * Get highest selling product
   * @return array
   */
  public function getHighestSeller(){
    $query = $this->db->query("SELECT SUM(dd.quantity) as qty
                                FROM tbl_invoice i
                                JOIN tbl_delivery d ON d.order_id = i.order_id
                                JOIN tbl_delivery_detail dd ON dd.delivery_id = d.delivery_id
                                JOIN tbl_product p ON p.product_id = dd.product_id
                                WHERE i.status = 'approved'
                                GROUP BY dd.product_id
                                ORDER BY qty DESC");
    if ($query->num_rows() > 0) {
     return $query->result_array();
    }
    return array();
  }
  /**
   * Get product names of highest sellers
   * @return array
   */
  public function getHighestSellerNames(){
    $query = $this->db->query("SELECT p.product_name
                                FROM tbl_invoice i
                                JOIN tbl_delivery d ON d.order_id = i.order_id
                                JOIN tbl_delivery_detail dd ON dd.delivery_id = d.delivery_id
                                JOIN tbl_product p ON p.product_id = dd.product_id
                                WHERE i.status = 'approved'
                                GROUP BY dd.product_id
                                ORDER BY SUM(dd.quantity) DESC");
    if ($query->num_rows() > 0) {
     return $query->result_array();
    }
    return array();
  }
  /**
   * Get least selling product
   * @return array
   */
  public function getLeastSellingProduct(){
    $query = $this->db->query("SELECT p.product_name, SUM(dd.quantity) as qty
                                FROM tbl_invoice i
                                JOIN tbl_delivery d ON d.order_id = i.order_id
                                JOIN tbl_delivery_detail dd ON dd.delivery_id = d.delivery_id
                                JOIN tbl_product p ON p.product_id = dd.product_id
                                WHERE i.status = 'approved'
                                GROUP BY dd.product_id
                                ORDER BY qty ASC
                                LIMIT 1");
    if ($query->num_rows() > 0) {
     return $query->result_array();
    }
    return array();
  }
  /**
   * Get credit of company
   * @param  int $invoice_id
   * @return array
   */
  public function getCompanyCreditByInvoiceId($invoice_id){
    $query = $this->db->query("SELECT c.credit, c.customer_id
                               FROM tbl_invoice i
                               JOIN tbl_order o ON o.order_id = i.order_id
                               JOIN tbl_customer c ON c.customer_id = o.customer_id
                               WHERE i.invoice_id = $invoice_id");
     if ($query->num_rows() > 0) {
       return $query->row();
     }
     return false;
  }
  /**
   * Decrease company credit after payment
   * @param  double $amount
   * @param  int $invoice_id
   */
  public function reduceCompanyCredit($amount, $invoice_id){
    $row = $this->i->getCompanyCreditByInvoiceId($invoice_id);
    $customer_id = $row->customer_id;
    $this->db->query("UPDATE tbl_customer
                      SET credit = credit - '$amount'
                      WHERE customer_id = $customer_id");
  }
  /**
   * Clear customer credit
   * @param  double $amount
   * @param  int $invoice_id
   * @return
   */
  public function updateCustomerCredit($amount, $invoice_id){
    $row = $this->i->getCompanyCreditByInvoiceId($invoice_id);
    $customer_id = $row->customer_id;
    $this->db->query("UPDATE tbl_customer
                      SET credit = 0
                      WHERE customer_id = $customer_id");
  }
  /**
   * Get payment details of invoice
   * @param  int $invoice_id
   * @return array            
   */
  public function getPaymentDetailsByInvoiceId($invoice_id){
    $query = $this->db->query("SELECT * FROM tbl_payment p WHERE p.invoice_id = $invoice_id");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    return array();
  }

}
