<?php
/**
 * User model that manages all database transactions regarding an user
 */
	class User_m extends CI_Model{
		/**
		 * Register a new user
		 * @param  array $data
		 * @return boolean
		 */
		public function signup($data){
			//Begin transaction
			$this->db->trans_begin();
			// Insert user
			$this->db->insert('tbl_staff', $data);
			$insert_id = $this->db->insert_id();

			$staff_code = 9000000 + $insert_id;
			$this->db->set('staff_code', $staff_code);
			$this->db->where('staff_id', $insert_id);
			$this->db->update('tbl_staff');

			//Check status and return accordingly
			if ($this->db->trans_status() === FALSE){
	      $this->db->trans_rollback();
				return false;
	    }
	    else{
	      $this->db->trans_commit();
				return true;
	    }
		}
		/**
		 * Login to the application
		 * @param  string $username
		 * @param  string $password
		 * @return boolean
		 */
		public function login($username, $password){
			// Validate
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$result = $this->db->get('tbl_staff');
			if($result->num_rows() == 1){
				return  array(
					'id' => $result->row(0)->staff_id,
				'name' => $result->row(0)->first_name . ' ' . $result->row(0)->last_name,
				'role_id' => $result->row(0)->role_id,
				'img' => $result->row(0)->img
				);
			} else {
				return false;
			}
		}
		/**
		 * Checks if username already exists
		 * @param  string $username
		 * @return boolean
		 */
		public function check_username_exists($username){
			$query = $this->db->get_where('tbl_staff', array('username' => $username));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}
		/**
		 * Check if email already exists
		 * @param  string $email
		 * @return boolean
		 */
		public function check_email_exists($email){
			$query = $this->db->get_where('tbl_staff', array('email' => $email));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}
		/**
		 * Get user using its user id
		 * @return boolean
		 */
		public function getUserInfo($id){
			$query = $this->db->get_where('tbl_staff', array('staff_id' => $id));
			if (!empty($query->row_array())) {
				return $query->row_array();
			}
			return false;
		}
		/**
		 * Update user details
		 * @param  int $id
		 * @param  array $data
		 * @return boolean
		 */
		public function edit($id, $data){

			$this->db->where('staff_id', $id);
			$this->db->update('tbl_staff', $data);

			if ($this->db->affected_rows() > 0) {
				return true;
			}
			return false;
		}
		/**
		 * Upload user profile image
		 */
		public function upload_image($id, $upload_image){
			$this->db->set('img', $upload_image);
			$this->db->where('staff_id', $id);
			$this->db->update('tbl_staff');
		}
		/**
		 * Get list of users
		 * @return array
		 */
		public function getAllUsers(){
			$query = $this->db->get('tbl_staff');
			if ($query->num_rows() > 0) {
				return $query->result();
			}
			return array();
		}
		/**
		 * Delete user
		 * @param  int $id
		 * @return boolean
		 */
		public function deleteUser($id){
			$this->db->where('staff_id', $id);
			$this->db->delete('tbl_staff');
			if ($this->db->affected_rows() > 0) {
				return true;
			}
			return false;
		}
	}
