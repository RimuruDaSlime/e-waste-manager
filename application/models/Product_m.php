<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Product model that manages all database transactions regarding an product
 */
class Product_m extends CI_Model{
	/**
	 * Get list of available products
	 * @return array
	 */
	public function getAllProducts(){
		$query = $this->db->query("SELECT c.category_name,
																			p.product_id,
                                      p.product_code,
                                      p.product_name,
                                      p.description,
                                      p.unit_of_measurement,
                                      p.in_stock,
																			p.in_order,
																			p.allocated,
																			p.price
                                FROM tbl_product p
                                JOIN tbl_category c ON p.category_id = c.category_id");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    else{
      return false;
    }
	}
	/**
	 * Get product details of customer
	 * @param  string $search product code
	 * @param  int $id customer id
	 * @return array
	 */
	public function getProductDetailsByCode($search,$id){
		$query = $this->db->query("SELECT *
																FROM tbl_product p
																WHERE p.product_code LIKE '%$search%'
																AND p.product_id IN
																(SELECT cd.product_id
																FROM tbl_customer_detail cd
																WHERE cd.customer_id = $id)");

		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	/**
	 * Get product details of customer
	 * @param  string $search product name
	 * @param  int $id customer id
	 * @return array
	 */
	public function getProductDetailsByName($search, $id){
		$query = $this->db->query("SELECT *
																FROM tbl_product p
																WHERE p.product_name LIKE '%$search%'
																AND p.product_id IN
																(SELECT cd.product_id
																FROM tbl_customer_detail cd
																WHERE cd.customer_id = $id)");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	/**
	 * Get product details by id
	 * @return array
	 */
	public function getProductDetailsById(){
		$id = $this->input->post('productid');
		$this->db->where('product_id', $id);
		$query = $this->db->get('tbl_product');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	/**
	 * Add a new product to the database
	 * @param array $data
	 */
  public function addProduct($data){

			//Begin transaction
			$this->db->trans_begin();
			$this->db->insert('tbl_product', $data);
			$insert_id = $this->db->insert_id();

			$product_code = 2000000 + $insert_id;
			$this->db->set('product_code', $product_code);
			$this->db->where('product_id', $insert_id);
			$this->db->update('tbl_product');

			//Check status and return accordingly
			if ($this->db->trans_status() === FALSE){
	      $this->db->trans_rollback();
				return false;
	    }
	    else{
	      $this->db->trans_commit();
				return true;
	    }
	}
	/**
	 * Remove a product
	 * @param  int $product_id
	 * @return false
	 */
	public function removeProduct($product_id){
		$this->db->where('product_id', $product_id);
		$this->db->delete('tbl_product');

		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * Update product details
	 * @return boolean
	 */
	public function updateProduct(){
		$product_id = $this->input->post('product-id');
    $quantity = $this->input->post('quantity');
		//Begin transaction
		$this->db->trans_begin();
		//Get product details by ID
		$this->db->where('product_id', $product_id);
		$query = $this->db->get('tbl_product');
		$row = $query->row();
		//Add quantity to current stock and update row
		$in_stock = (int)$row->in_stock + (int)$quantity;
		$field = array(
			'in_stock'=> $in_stock
		);
		$this->db->where('product_id', $product_id);
		$this->db->update('tbl_product', $field);
		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
	}
	/**
	 * Get produyt by ID
	 * @return array
	 */
	public function getProductById(){
		$product_id = $this->input->get('product_id');
		$this->db->where('product_id', $product_id);
		$query = $this->db->get('tbl_product');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	/**
	 * Gte number of avaliable products
	 * @return array
	 */
	public function getNumberOfProducts(){
		$query = $this->db->query("SELECT * FROM tbl_product");
  	return $query->num_rows();
	}
	/**
	 * Get list of available categories
	 * @return array
	 */
	public function getAllCategories(){
		$query = $this->db->query("SELECT category_id, category_name FROM tbl_category");
		return $query->result_array();
	}
	/**
	 * Add a new category
	 * @param array $data
	 */
	public function addCategory($data){
		$this->db->insert('tbl_category', $data);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * Submit a new inbound
	 * @param  int $staff_id user id submitting the inbound
	 * @param  array $data
	 * @return boolean
	 */
	public function submitInbound($staff_id, $data){
		//Begin transaction
		$this->db->trans_begin();
		//Insert row into tbl_order
		$field = array(
			'staff_id'=> $staff_id,
			'inbound_date'=>date('Y-m-d'),
			'inbound_date_time'=>date('Y-m-d H:i:s'),
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
		);

		$this->db->insert('tbl_inbound', $field);
		$inbound_id = $this->db->insert_id();

		$inbound_code = 'IB' . date("Y") . date("m") . date("d")  . sprintf("%04d", $inbound_id);
		$this->db->set('inbound_code', $inbound_code);
		$this->db->where('inbound_id', $inbound_id);
		$this->db->update('tbl_inbound');

		//Insert row to tbl_inbound_detail
		foreach ($data as $key => $value) {
			$field = array(
				'product_id' => $value['prodid'],
				'quantity' => $value['qty'],
				'inbound_id' => $inbound_id,
			);
			$this->db->insert('tbl_inbound_detail', $field);
		}

		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}
		else{
			$this->db->trans_commit();
			return true;
		}
	}
	/**
	 * Get list of inbounds for approval
	 * @param  date $from
	 * @param  date $to
	 * @return array
	 */
	public function getAllInboundsForApproval($from, $to){
		$query = $this->db->query("SELECT i.inbound_code,
																			CONCAT(s.first_name, ' ', s.last_name) AS staff_name,
																			i.status,
																			i.inbound_id
															 FROM tbl_inbound i
															 JOIN tbl_staff s ON i.staff_id = s.staff_id
															 WHERE i.update_date BETWEEN '$from' AND '$to'");
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}
	/**
	 * Get inbound details by ID
	 * @param  int $inbound_id
	 * @return array
	 */
	public function getInboundDetailsById($inbound_id){
		$query = $this->db->query("SELECT p.product_name, id.quantity, p.unit_of_measurement
															 FROM tbl_inbound_detail id
															 JOIN tbl_product p ON id.product_id = p.product_id
															 WHERE id.inbound_id = $inbound_id");
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return false;
	}
	/**
	 * Approve inbound
	 * @param  int $inbound_id
	 * @param  string $note
	 * @return boolean
	 */
	public function approveInbound($inbound_id, $note){
		//Begin transaction
		$this->db->trans_begin();
		//Update tbl_order, approve order status
		$data = array(
			'status' => 'approved',
			'note' => $note,
			'admin_id' => $this->session->userdata('staff_id'),
			'approve_date' => date('Y-m-d'),
			'approve_date_time' => date('Y-m-d H:i:s'),
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
		);
		$this->db->where('inbound_id', $inbound_id);
		$this->db->update('tbl_inbound', $data);

		$query = $this->db->query("SELECT id.product_id, id.quantity, p.in_stock
															 FROM tbl_inbound_detail id
															 JOIN tbl_product p ON id.product_id = p.product_id
															 WHERE id.inbound_id = $inbound_id");
		if ($query->num_rows() > 0) {
			$result = $query->result();
			foreach ($result as $row) {
				$field = array(
	        'in_stock' => ($row->in_stock + $row->quantity)
	      );
	      $this->db->set($field);
	      $this->db->where('product_id', $row->product_id);
	      $this->db->update('tbl_product');
			}
		}
		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
	}

}
