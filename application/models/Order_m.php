<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Order model that manages all database transactions regarding an order
 */
class Order_m extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->model('customer_m', 'c');
	}
	/**
   * Get list of orders
   * @param  date $from
   * @param  date $to
   * @return array
   */
	public function getAllOrders($from, $to){
		$string = "SELECT	 o.order_id,
											 o.order_code,
											 o.order_type,
											 o.order_date_time,
											 c.company_name,
											 CONCAT(c.first_name, ' ', c.last_name) AS customer_name,
                       (SELECT SUM(od.quantity) FROM tbl_order_detail od
                        WHERE od.order_id = o.order_id) AS total_quantity,
                       (SELECT SUM(od.total) FROM tbl_order_detail od
                       	WHERE od.order_id = o.order_id) AS total_amount,
											 o.status,
											 (SELECT CONCAT(first_name, ' ', last_name)
											  FROM tbl_staff WHERE staff_id = o.staff_id) AS staff_name,
											 (SELECT CONCAT(first_name, ' ', last_name)
 											  FROM tbl_staff WHERE staff_id = o.ordered_by) AS ordered_by,
											 o.approve_date_time,
											 o.cancel_date_time,
											 o.update_date_time
								FROM tbl_order o
								JOIN tbl_customer c ON o.customer_id = c.customer_id
								WHERE o.update_date BETWEEN '$from' AND '$to'
								GROUP BY o.order_id
								ORDER BY o.order_date_time DESC";
		$query = $this->db->query($string);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else{
			return false;
		}
	}
	public function getAllOrders2(){

		$query = $this->db->query("SELECT	 o.order_id,
																			 o.order_type,
																			 o.order_date_time,
																			 od.product_id,
																			 od.quantity,
																			 CONCAT(c.first_name, ' ', c.last_name) AS customer_name,
                                       (SELECT SUM(od.quantity) FROM tbl_order_detail od
                                        WHERE od.order_id = o.order_id) AS total_quantity,
                                       (SELECT SUM(od.total) FROM tbl_order_detail od
                                       	WHERE od.order_id = o.order_id) AS total_amount,
																			 o.status,
																			 (SELECT CONCAT(first_name, ' ', last_name)
																			  FROM tbl_staff WHERE staff_id = o.staff_id) AS staff_name,
																			 o.approve_date_time,
																			 o.cancel_date_time,
																			 o.update_date_time
																FROM tbl_order o
																JOIN tbl_customer c ON o.customer_id = c.customer_id
																JOIN tbl_order_detail od ON od.order_id = o.order_id
																WHERE o.status != 'cancelled'
																ORDER BY o.order_date_time DESC");
		if ($query->num_rows() > 0) {
			return $query->result();
		}
		else{
			return false;
		}
	}
	/**
	 * Get all orders for approval
	 * @return boolean
	 */
	public function getAllOrdersForApproval(){

		$query = $this->db->query("SELECT	 o.order_id,
																			 o.order_code,
																			 o.order_type,
																			 o.order_date_time,
																			 c.company_name,
																			 CONCAT(c.first_name, ' ', c.last_name) AS customer_name,
                                       (SELECT SUM(od.quantity) FROM tbl_order_detail od
                                        WHERE od.order_id = o.order_id) AS total_quantity,
                                       (SELECT SUM(od.total) FROM tbl_order_detail od
                                       	WHERE od.order_id = o.order_id) AS total_amount,
																			 o.status,
																			 (SELECT CONCAT(first_name, ' ', last_name)
																			  FROM tbl_staff WHERE staff_id = o.staff_id) AS staff_name,
																			 (SELECT CONCAT(first_name, ' ', last_name)
																				FROM tbl_staff WHERE staff_id = o.ordered_by) AS ordered_by,
																			 o.approve_date_time,
																			 o.cancel_date_time,
																			 o.update_date_time
																FROM tbl_order o
																JOIN tbl_customer c ON o.customer_id = c.customer_id
																WHERE o.status NOT IN ('cancelled', 'approved')
																GROUP BY o.order_id
																HAVING total_amount > 0
																ORDER BY o.order_date_time DESC");
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		else{
			return false;
		}
	}
	/**
	 * Place a new order
	 * @param  int $custid
	 * @param  array $data list of products in order
	 * @return boolean
	 */
	public function placeOrder($custid, $data){
		//Begin transaction
		$this->db->trans_begin();
		//Insert row into tbl_order
	  $field = array(
	    'customer_id'			=> $custid,
			'ordered_by' 			=> $this->session->userdata('staff_id'),
	    'order_type'			=> 'sales',
	    'order_date'			=>date('Y-m-d'),
	    'order_date_time'	=>date('Y-m-d H:i:s'),
			'update_date'			=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
	  );

	  $this->db->insert('tbl_order', $field);
	  $order_id = $this->db->insert_id();

		$order_code = 'SO' . date("Y") . date("m") . date("d")  . sprintf("%04d", $order_id);
		$this->db->set('order_code', $order_code);
		$this->db->where('order_id', $order_id);
		$this->db->update('tbl_order');

		//Insert row to tbl_order_detail
		foreach ($data as $key => $value) {
			$total = $value['price'] *  $value['qty'];
			$field = array(
				'product_id' => $value['prodid'],
				'quantity' => $value['qty'],
				'price' => $value['price'],
				'order_id' => $order_id,
				'total' => $total
			);
			$this->db->insert('tbl_order_detail', $field);
			$insert_id = $this->db->insert_id();

			$order_detail_code = 'SOD' . date("Y") . date("m") . date("d")  . sprintf("%04d", $insert_id);
			$this->db->set('order_detail_code', $order_detail_code);
			$this->db->where('order_detail_id', $insert_id);
			$this->db->update('tbl_order_detail');
		}

		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
	}
	/**
	 * [getOrderDetailsById description]
	 * @param  [type] $order_id [description]
	 * @return [type]           [description]
	 */
	public function getOrderDetailsById($order_id){

		$query = $this->db->query("SELECT p.product_name,
																			o.order_code,
																			p.product_code,
																			p.description,
																   		od.price,
															       	od.quantity,
															       	p.unit_of_measurement,
															       	od.total,
																			p.product_id
															FROM tbl_order_detail od
															JOIN tbl_product p ON od.product_id = p.product_id
															JOIN tbl_order o ON od.order_id = o.order_id
															WHERE od.order_id = $order_id
															HAVING od.total != 0");
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	/**
	 * [cancelOrder description]
	 * @param  [type] $order_id [description]
	 * @return [type]           [description]
	 */
	public function cancelOrder($order_id){

		//Begin transaction
		$this->db->trans_begin();
		//Update fields
		$data = array(
			'status' => 'cancelled',
			'staff_id' => $this->session->userdata('staff_id'),
			'cancel_date' => date('Y-m-d'),
			'cancel_date_time' => date('Y-m-d H:i:s'),
			'approve_date' => null,
			'approve_date_time' => null,
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
		);
		//********************Condition for cancelling*********************************//
		//Cancel if delivery status != 'approved'
		//Or order has not yet been approved
		$query = $this->db->query("SELECT * FROM tbl_delivery WHERE order_id = $order_id");
		if ($query->num_rows() > 0) {
			$row = $query->row();
			if ($row->status != 'approved') {
				//Update tbl_order
				$this->db->where('order_id', $order_id);
				$this->db->update('tbl_order', $data);
				if ($this->db->affected_rows() > 0) {
					//Update tbl_product
					$query = $this->db->query("SELECT od.product_id, od.quantity
																		 FROM tbl_order o
																		 JOIN tbl_order_detail od ON o.order_id = od.order_id
																		 WHERE o.order_id = $order_id");
					$result = $query->result();
					foreach ($result as $row) {
						$query = $this->db->query("SELECT p.product_id, p.in_stock, p.in_order FROM tbl_product p WHERE p.product_id = $row->product_id");
						$product_row = $query->row();
						$field = array(
							'in_stock' => (int)$product_row->in_stock + (int)$row->quantity,
							'in_order' => (int)$product_row->in_order - (int)$row->quantity
						);
						$this->db->where('product_id', $row->product_id);
						$this->db->update('tbl_product', $field);
					}

					//Update tbl_delivery
					$field = array(
						'status' => 'cancelled',
						'staff_id' => $this->session->userdata('staff_id'),
						'cancel_date' => null,
						'cancel_date_time' => null,
						'approve_date' => null,
						'approve_date_time' => null,
						'update_date'=>date('Y-m-d'),
						'update_date_time'=>date('Y-m-d H:i:s')
					);
					$this->db->set($field);
					$this->db->where('order_id', $order_id);
					$this->db->update('tbl_delivery');
				}
			}
		}
		else{
			$this->db->where('order_id', $order_id);
			$this->db->update('tbl_order', $data);

			//Update tbl_product
			$query = $this->db->query("SELECT od.product_id, od.quantity, o.status
												FROM tbl_order o
												JOIN tbl_order_detail od ON o.order_id = od.order_id
												WHERE o.order_id = $order_id");
			$result = $query->result();
			foreach ($result as $row) {
				//Verify if order was approved before updating tbl_product
				//Else do nothing
				if ($row->status == 'approved') {
					$query = $this->db->query("SELECT p.product_id, p.in_stock, p.in_order FROM tbl_product p WHERE p.product_id = $row->product_id");
					$product_row = $query->row();
					$field = array(
						'in_stock' => (int)$product_row->in_stock + (int)$row->quantity,
						'in_order' => (int)$product_row->in_order - (int)$row->quantity
					);
					$this->db->where('product_id', $row->product_id);
					$this->db->update('tbl_product', $field);
				}
			}
		}

		//Update current customer
		$result = $this->c->getCurrentCustomer();
		$this->c->setCurrentCustomer($result[0]['customer_id']);

		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
	}
	/**
	 * [approveOrder description]
	 * @param  [type] $order_id [description]
	 * @return [type]           [description]
	 */
	public function approveOrder($order_id){
		$approval_note = $this->input->get('approval_note');
		//Begin transaction
		$this->db->trans_begin();
		//Update tbl_order, approve order status
		$data = array(
			'status' => 'approved',
			'note' => $approval_note,
			'staff_id' => $this->session->userdata('staff_id'),
			'approve_date' => date('Y-m-d'),
			'approve_date_time' => date('Y-m-d H:i:s'),
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
		);
		$this->db->where('order_id', $order_id);
		$this->db->update('tbl_order', $data);

		//Update allocated units
		$query = $this->db->query("SELECT od.product_id, od.quantity
															 FROM tbl_order_detail od
															 WHERE od.order_id = $order_id");
		$result = $query->result();
		foreach ($result as $row) {
			$query = $this->db->query("UPDATE tbl_product
																 SET allocated = allocated + '$row->quantity'
																 WHERE product_id = $row->product_id");
		}

		//Insert row to tbl_delivery with $order_id
		$field = array(
			'order_id'=> $order_id
		);
		$this->db->insert('tbl_delivery', $field);
		$delivery_id = $this->db->insert_id();

		$delivery_code = 'DL' . date("Y") . date("m") . date("d")  . sprintf("%04d", $delivery_id);
		$data = array(
			'delivery_code' => $delivery_code,
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
		);
		$this->db->where('delivery_id', $delivery_id);
		$this->db->update('tbl_delivery', $data);

		//Check order type :
		//If type == sales => decrease in_stock, increase in_order
		//Else vice versa
		//Insert rows to tbl_delivery_detail from tbl_order_detail
		$query = $this->db->query("SELECT * FROM tbl_order WHERE order_id = $order_id");
		$row = $query->row();
		if ($row->order_type == 'sales') {
			$query = $this->db->query("SELECT od.product_id, od.quantity, p.in_stock, p.in_order
																 FROM tbl_order_detail od
																 JOIN tbl_product p on od.product_id = p.product_id
																 WHERE od.order_id = $order_id");
			$result = $query->result();
 			foreach ($result as $row) {
 				$res = (int)$row->in_stock - (int)$row->quantity;
 				if ($res > 0) {
 					$field = array(
 						'delivery_id' => $delivery_id,
 						'product_id' => $row->product_id,
 						'quantity' => $row->quantity
 					);
 				}
 				else{
 					$field = array(
 						'delivery_id' => $delivery_id,
 						'product_id' => $row->product_id,
 						'quantity' => $row->in_stock
 					);
 				}
 				$this->db->insert('tbl_delivery_detail', $field);
 			}
		}
		else{
			$query = $this->db->query("SELECT od.product_id, od.quantity, p.in_stock, p.in_order
																 FROM tbl_order_detail od
																 JOIN tbl_product p on od.product_id = p.product_id
																 WHERE od.order_id = $order_id");

			$result = $query->result();
			foreach ($result as $row) {
				$field = array(
					'delivery_id' => $delivery_id,
					'product_id' => $row->product_id,
					'quantity' => $row->quantity
				);
				$this->db->insert('tbl_delivery_detail', $field);
			}
		}

		//Update current customer
		$result = $this->c->getCurrentCustomer();
		$this->c->setCurrentCustomer($result[0]['customer_id']);

		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
	}
	/**
	 * [addProductToOrder description]
	 */
	public function addProductToOrder(){
		$order_id = $this->input->post('order-id');
		$product_id = $this->input->post('product-id-add');
		$quantity = $this->input->post('quantity-add');

		//Begin transaction
		$this->db->trans_begin();

		//Check if product already in order
		//If true, add quantity
		//Else insert new row
		$query = $this->db->query("SELECT od.product_id, od.order_detail_id, od.quantity, od.price, p.discount
															 FROM tbl_order_detail od
															 JOIN tbl_product p ON od.product_id = p.product_id
															 WHERE od.product_id = $product_id
															 AND od.order_id = $order_id");
		if ($query->num_rows() > 0) {
			$row = $query->row();
			$total_quantity = ((int)$quantity + (int)$row->quantity);
			$total_discount = ($total_quantity * doubleval($row->discount));
			$total_amount = (($total_quantity * doubleval($row->price)) - $total_discount);
			$field = array(
				'quantity' => $total_quantity,
				'discount' => $total_discount,
				'total' => $total_amount
			);
			$this->db->where('order_detail_id', $row->order_detail_id);
			$this->db->update('tbl_order_detail', $field);
		}
		else{
			$query = $this->db->query("SELECT *
																 FROM tbl_product p
																 WHERE p.product_id = $product_id");
			$row = $query->row();
			$total_discount = ($quantity * $row->discount);
			$total_amount = (($row->price * $quantity) - $total_discount);
			$field = array(
				'price' => $row->price,
				'discount' => $total_discount,
				'quantity' => $quantity,
				'total' => $total_amount,
				'order_id' => $order_id,
				'product_id' => $product_id
			);
			$this->db->insert('tbl_order_detail', $field);
		}

		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
	}
	/**
	 * Get number of pending orders
	 * @return array
	 */
	public function getNbPendingOrders(){
		$query = $this->db->query("SELECT count(o.order_id) as nb FROM tbl_order o WHERE o.status = 'awaiting approval'");
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return false;
	}
	/**
	 * Get order details
	 * @param  string $search
	 * @return array      
	 */
	public function getOrderDetailsByCode($search){
		$query = $this->db->query("SELECT *
                               FROM tbl_order
                               WHERE order_code LIKE '$search%'
                               AND status NOT IN ('awaiting approval', 'cancelled')
                               AND order_id IN
                               (SELECT order_id FROM tbl_delivery WHERE status IN ('awaiting invoice'))");
	  if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

}
