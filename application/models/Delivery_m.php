<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Delivery model that manages all database transactions regarding a delivery
 */
class Delivery_m extends CI_Model{
  /**
   * Get all deliveries
   * @param  date $from filter starting date
   * @param  date $to   filter end date
   * @return array
   */
  public function getAllDeliveries($from, $to){
    $query = $this->db->query("SELECT d.delivery_id,
                                      d.delivery_code,
                                       CONCAT(c.first_name, ' ', c.last_name) AS customer_name,
                                       d.status,
                                       (SELECT CONCAT(first_name,' ', last_name)
                                        FROM tbl_staff
                                        WHERE staff_id = d.staff_id) AS staff_name,
                                       d.approve_date_time,
                                       d.cancel_date_time,
                                       d.update_date_time
                                FROM tbl_delivery d
                                JOIN tbl_order o ON d.order_id = o.order_id
                                JOIN tbl_customer c ON c.customer_id = o.customer_id
                                WHERE d.update_date BETWEEN '$from' AND '$to'");

      if ($query->num_rows() > 0) {
        return $query->result_array();
      }
      else{
        return false;
      }
	}
  /**
   * Get list of deliveries for approval
   * @return array
   */
  public function getAllDeliveriesForApproval(){
    $query = $this->db->query("SELECT d.delivery_id,
                                      d.delivery_code,
                                       CONCAT(c.first_name, ' ', c.last_name) AS customer_name,
                                       d.status,
                                       (SELECT CONCAT(first_name,' ', last_name)
                                        FROM tbl_staff
                                        WHERE staff_id = d.staff_id) AS staff_name,
                                       d.approve_date_time,
                                       d.cancel_date_time,
                                       d.update_date_time
                                FROM tbl_delivery d
                                JOIN tbl_order o ON d.order_id = o.order_id
                                JOIN tbl_customer c ON c.customer_id = o.customer_id
                                WHERE d.status IN ('awaiting approval','cancelled')");

      if ($query->num_rows() > 0) {
        return $query->result();
      }
      else{
        return false;
      }
	}
  /**
   * Get details of delivery using its delivery id
   * @param  int $delivery_id
   * @return array
   */
  public function getDeliveryDetailsByID($delivery_id){

    $query = $this->db->query("SELECT p.product_name,
                                      p.product_id,
                                	    dd.quantity,
                                      p.product_id,
                                      p.in_stock,
                                      p.unit_of_measurement,
                                      p.product_code,
                                      p.description,
                                      p.unit_of_measurement,
                                      (SELECT d.status
                                       FROM tbl_delivery d
                                       WHERE d.delivery_id = dd.delivery_id) AS status
                               FROM tbl_delivery_detail dd
                               JOIN tbl_product p ON dd.product_id = p.product_id
                               WHERE dd.delivery_id = $delivery_id");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    else{
      return false;
    }
  }
  /**
   * Save delivery details before approval.
   * Return boolean indicating if transaction successful
   * @param  int $delivery_id
   * @param  array $data
   * @param  string $note
   * @return boolean
   */
  public function saveDeliveryDetails($delivery_id, $data, $note){
    //Begin transaction
    $this->db->trans_begin();
    //Update tbl_delivery_detail
    foreach ($data as $key => $value) {
      $field = array(
        'quantity' => $value
      );
      $this->db->where('delivery_id', (int)$delivery_id);
      $this->db->where('product_id', (int)$key);
      $this->db->update('tbl_delivery_detail', $field);
    }

    $field = array(
      'status' => "awaiting invoice",
      'note' => $note,
      'staff_id' => $this->session->userdata('staff_id'),
      'approve_date' => date('Y-m-d'),
			'approve_date_time' => date('Y-m-d H:i:s'),
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
    );
    $this->db->where('delivery_id', $delivery_id);
    $this->db->update('tbl_delivery', $field);
    //Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
  }
  /**
   * Approve a delivery.
   * Return boolean indicating if transaction successful
   * @param  int $delivery_id
   * @param  string $note
   * @return boolean
   */
  public function approveDelivery($delivery_id, $note){
    $field = array(
      'status' => "approved",
      'note' => $note,
      'staff_id' => $this->session->userdata('staff_id'),
      'approve_date' => date('Y-m-d'),
			'approve_date_time' => date('Y-m-d H:i:s'),
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
    );
    //Begin transaction
    $this->db->trans_begin();
    //Update tbl_delivery
    $this->db->where('delivery_id', $delivery_id);
		$this->db->update('tbl_delivery', $field);

    //Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
  }
  /**
   * Cancel a delivery
   * @param  int $delivery_id
   * @return boolean
   */
  public function cancelDelivery($delivery_id){

    $update = false;

		$data = array(
			'status' => 'cancelled',
			'staff_id' => $this->session->userdata('staff_id'),
			'cancel_date' => date('Y-m-d'),
			'cancel_date_time' => date('Y-m-d H:i:s'),
			'approve_date' => null,
			'approve_date_time' => null,
			'update_date'=>date('Y-m-d'),
			'update_date_time'=>date('Y-m-d H:i:s')
		);

    //********************Condition for cancelling*********************************//
		//Cancel if waybill status != 'approved'
		//Or delivery has not yet been approved

    $query = $this->db->query("SELECT * FROM tbl_waybill WHERE delivery_id = $delivery_id");
    if ($query->num_rows() > 0){
      $row = $query->row();
      if ($row->status != 'approved'){
        //Update tbl_delivery
        $this->db->where('delivery_id', $delivery_id);
        $this->db->update('tbl_delivery', $data);
        //Update tbl_order and tbl_waybill
        if ($this->db->affected_rows() > 0) {
          $query = $this->db->query("SELECT order_id FROM tbl_delivery WHERE delivery_id = $delivery_id");
          if ($query->num_rows() > 0) {
            $row = $query->row();
            $field = array(
              'status' => 'awaiting approval',
              'staff_id' => null,
              'cancel_date' => null,
              'cancel_date_time' => null,
              'approve_date' => null,
              'approve_date_time' => null,
        			'update_date'=>date('Y-m-d'),
        			'update_date_time'=>date('Y-m-d H:i:s')
            );
            $this->db->set($field);
            $this->db->where('order_id', $row->order_id);
            $this->db->update('tbl_order');

            if ($this->db->affected_rows() > 0) {
              $field = array(
                'status' => 'cancelled',
                'staff_id' => null,
                'cancel_date' => null,
                'cancel_date_time' => null,
                'approve_date' => null,
                'approve_date_time' => null,
          			'update_date'=>date('Y-m-d'),
          			'update_date_time'=>date('Y-m-d H:i:s')
              );
              $this->db->where('delivery_id', $delivery_id);
              $this->db->update('tbl_waybill', $field);

              if ($this->db->affected_rows() > 0) {
                return true;
              }
            }
          }
        }
      }
    }
    else{
      $query = $this->db->query("SELECT * FROM tbl_delivery WHERE delivery_id = $delivery_id");
      if ($query->num_rows() > 0) {
        $row = $query->row();
        if($row->status != 'approved'){
          //Update tbl_delivery
          $this->db->where('delivery_id', $delivery_id);
          $this->db->update('tbl_delivery', $data);
          //Update tbl_order and tbl_waybill
          if ($this->db->affected_rows() > 0) {
            $query = $this->db->query("SELECT order_id FROM tbl_delivery WHERE delivery_id = $delivery_id");
            if ($query->num_rows() > 0) {
              $row = $query->row();
              $field = array(
                'status' => 'awaiting approval',
                'staff_id' => null,
                'cancel_date' => null,
                'cancel_date_time' => null,
                'approve_date' => null,
                'approve_date_time' => null,
          			'update_date'=>date('Y-m-d'),
          			'update_date_time'=>date('Y-m-d H:i:s')
              );
              $this->db->set($field);
              $this->db->where('order_id', $row->order_id);
              $this->db->update('tbl_order');

              if ($this->db->affected_rows() > 0) {
                $update = true;
              }
            }
            $field = array(
              'status' => 'cancelled',
              'staff_id' => null,
              'cancel_date' => null,
              'cancel_date_time' => null,
              'approve_date' => null,
              'approve_date_time' => null,
        			'update_date'=>date('Y-m-d'),
        			'update_date_time'=>date('Y-m-d H:i:s')
            );
            $this->db->set($field);
            $this->db->where('delivery_id', $delivery_id);
            $this->db->update('tbl_waybill');

            if ($this->db->affected_rows() > 0) {
              $update = true;
            }
          }
        }
      }
    }
    return $update;
  }
  /**
   * Get current stock per product
   * @return array
   */
  public function getCurrentStock(){
    $query = $this->db->query("SELECT product_id, product_name, in_stock FROM tbl_product");
    if ($query->num_rows() > 0) {
      return $query->result_array();
    }
    return false;
  }
  /**
   * Get number of pending deliveries
   * @return array
   */
  public function getNbPendingDeliveries(){
		$query = $this->db->query("SELECT count(d.delivery_id) as nb FROM tbl_delivery d WHERE d.status = 'awaiting approval'");
		if ($query->num_rows() > 0) {
			return $query->row();
		}
		return false;
	}
}
