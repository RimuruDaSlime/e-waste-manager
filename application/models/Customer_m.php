<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Customer model that manages all database transactions regarding a customer
 */
class Customer_m extends CI_Model{
	/**
	 * Get list of customers
	 * @return json
	 */
	public function getAllCustomers(){
		$this->db->order_by('customer_id', 'asc');
		$query = $this->db->get('tbl_customer');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	/**
	 * Add a new customer to database
	 * @param array $data  customer details
	 * @param array $array products which the customer are allowed to buy
	 * @return boolean
	 */
  public function addCustomer($data, $array){
		//Begin transaction
		$this->db->trans_begin();
		$this->db->insert('tbl_customer', $data);
		$insert_id = $this->db->insert_id();

		$customer_code = 1000000 + $insert_id;
		$this->db->set('customer_code', $customer_code);
		$this->db->where('customer_id', $insert_id);
		$this->db->update('tbl_customer');

		for ($i=0; $i < count($array); $i++) {
			$data = array(
				'product_id' => $array[$i],
				'customer_id' => $insert_id
			);

			$this->db->insert('tbl_customer_detail', $data);
		}

		$data = array(
			'customer_id' => $insert_id,
			'customer_code' => $customer_code
		);
		$this->db->insert('tbl_random', $data);

		//Check status and return accordingly
		if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
			return false;
    }
    else{
      $this->db->trans_commit();
			return true;
    }
	}
	/**
	 * Get customer details by name
	 * @return array
	 */
	public function getCustomerDetailsByName(){
		$search = $this->input->post('search');
		$this->db->like('first_name', $search);
		$query = $this->db->get('tbl_customer');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return array();
		}
	}
	/**
	 * Update customer information
	 * @return boolean true if update is a success else false
	 */
	public function updateCustomer(){
		$id = $this->input->post('id');
		$field = array(
		'first_name'=>$this->input->post('first_name'),
    'last_name'=>$this->input->post('last_name'),
		'email'=>$this->input->post('email'),
		'phone' => $this->input->post('phone'),
		'updated_at'=>date('Y-m-d'),
		'updated_date_time'=>date('Y-m-d H:i:s')
		);
		$this->db->where('id', $id);
		$this->db->update('tbl_customer', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * Delete customer from database
	 * @return boolean true if success, else false
	 */
	function deleteCustomer(){
		$id = $this->input->get('id');
		$this->db->where('id', $id);
		$this->db->delete('tbl_customer');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * Get list of order per customer
	 * @return array
	 */
	public function getAllOrdersPerCustomer(){
		$query = $this->db->query("SELECT t.customer_id,
																			 CONCAT(t.first_name, ' ', t.last_name) AS full_name,
																			 (SELECT MAX(order_date_time)
																				FROM tbl_order
																				WHERE customer_id = t.customer_id) AS recent_order
																FROM tbl_customer t");

		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		else{
			return false;
		}
	}
	/**
	 * Return a randomly chose customer
	 * @return array
	 */
	public function randomPick(){
		$this->db->order_by('rand()');
		$this->db->limit(1);
		$query = $this->db->get('tbl_customer');
		return $query->result();
	}
	/**
	 * Get customer details using its customer id
	 * @param  int $id id of customer
	 * @return array details of customer
	 */
	public function getCustomerDetails($id){
		$query = $this->db->query("SELECT cd.customer_id,
														   (SELECT p.product_id FROM tbl_product p WHERE p.product_id = cd.product_id) AS product_id,
														   (SELECT p.product_code FROM tbl_product p WHERE p.product_id = cd.product_id) AS product_code,
												       (SELECT p.product_name FROM tbl_product p WHERE p.product_id = cd.product_id) AS product_name,
												       (SELECT p.description FROM tbl_product p WHERE p.product_id = cd.product_id) AS description,
												       (SELECT p.unit_of_measurement FROM tbl_product p WHERE p.product_id = cd.product_id) AS unit,
												       (SELECT p.price FROM tbl_product p WHERE p.product_id = cd.product_id) AS price
													FROM tbl_customer_detail cd
													WHERE cd.customer_id = $id");
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return array();
	}
	/**
	 * Get current customer for sales
	 * @return array details of current customer
	 */
	public function getCurrentCustomer(){
		$query = $this->db->query("SELECT c.customer_id,
																			c.customer_code,
																	    c.company_name,
																			c.first_name,
																			c.last_name,
																			c.address,
																	    c.phone,
																	    c.email
																	FROM tbl_customer c
																	JOIN tbl_random r ON r.customer_id = c.customer_id
																	WHERE r.current = 1");
		$result = $query->result_array();
		return $result;
	}
	/**
	 * Set next current customer
	 * @param int $old_id id of current customer before changes
	 */
	public function setCurrentCustomer($old_id){
		//Begin transaction
    $this->db->trans_begin();

		//Update rows
		$this->db->query("UPDATE tbl_random SET current = 0 WHERE customer_id = $old_id");

		$query = $this->db->query("SELECT COUNT(r.customer_id) AS nb FROM tbl_random r");
		$row = $query->row();
		$nb = $row->nb;
		if ($old_id == $nb) {
			$this->db->query("UPDATE tbl_random SET current = 1 WHERE customer_id = 1");
		}
		$new_id = $old_id + 1;
		$this->db->query("UPDATE tbl_random SET current = 1 WHERE customer_id = $new_id");

		//Check transaction status
		//If all goes well,
		//Commit changes and return true
		//Else rollback and return false
    if ($this->db->trans_status() === FALSE){
      $this->db->trans_rollback();
      return false;
    }
    else{
      $this->db->trans_commit();
      return true;
    }
	}

}
