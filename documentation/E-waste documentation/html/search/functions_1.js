var searchData=
[
  ['canceldelivery',['cancelDelivery',['../class_deliveries.html#a4064a37b7f48c325c63f19e4031bb964',1,'Deliveries\cancelDelivery()'],['../class_delivery__m.html#a092406a23371c8881ff8abb35f5f0e6d',1,'Delivery_m\cancelDelivery()']]],
  ['cancelinvoice',['cancelInvoice',['../class_invoice__m.html#a4420a1ba68840de07c8f54c33039c37f',1,'Invoice_m']]],
  ['cancelorder',['cancelOrder',['../class_orders.html#a46c885e77083402e1d512d71f7409045',1,'Orders\cancelOrder()'],['../class_order__m.html#aba73cc0f746cf51d83ab4b72a415d5f4',1,'Order_m\cancelOrder()']]],
  ['cancelwaybill',['cancelWaybill',['../class_waybills.html#afd23546d005e9a9cc8e33654dcc4a5d1',1,'Waybills\cancelWaybill()'],['../class_waybill__m.html#aa6993c14b509466f366888b9b778e9da',1,'Waybill_m\cancelWaybill()']]],
  ['check_5femail_5fexists',['check_email_exists',['../class_users.html#a3cccc9edbbaeb2d131ee4141daa0ad8e',1,'Users\check_email_exists()'],['../class_user__m.html#a3cccc9edbbaeb2d131ee4141daa0ad8e',1,'User_m\check_email_exists()']]],
  ['check_5fusername_5fexists',['check_username_exists',['../class_users.html#a8d7df2f8c13c21b2a538895803595b51',1,'Users\check_username_exists()'],['../class_user__m.html#a8d7df2f8c13c21b2a538895803595b51',1,'User_m\check_username_exists()']]],
  ['createinvoice',['createInvoice',['../class_invoices.html#a4c6c715d0fa7ac3cbd8ef9c5f04d75bf',1,'Invoices\createInvoice()'],['../class_invoice__m.html#ab138803fdea527ebfbc9b770abbe170a',1,'Invoice_m\createInvoice()']]]
];
