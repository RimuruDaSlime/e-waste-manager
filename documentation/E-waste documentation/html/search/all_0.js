var searchData=
[
  ['addcategory',['addCategory',['../class_products.html#a54e6e10e5224a549bda28800f6f81692',1,'Products\addCategory()'],['../class_product__m.html#a0c811c7fdfc3f8676fa30ad0764894ed',1,'Product_m\addCategory()']]],
  ['addcustomer',['addCustomer',['../class_customers.html#a052d663dc03cf65219eda35be6c14650',1,'Customers\addCustomer()'],['../class_customer__m.html#a21d90b749d7101537b1dbfacfffbc351',1,'Customer_m\addCustomer()']]],
  ['addproduct',['addProduct',['../class_products.html#a91029f3f49efef2f4ff0e6f17d1a8169',1,'Products\addProduct()'],['../class_product__m.html#a90bb08eacecff63818d092afd9b00781',1,'Product_m\addProduct()']]],
  ['addproducttoorder',['addProductToOrder',['../class_orders.html#a2d59d3db87e2767d443b067ad205cc81',1,'Orders\addProductToOrder()'],['../class_order__m.html#a2d59d3db87e2767d443b067ad205cc81',1,'Order_m\addProductToOrder()']]],
  ['approvedelivery',['approveDelivery',['../class_deliveries.html#a27b98915ce023fd31ed9fc7c7156c709',1,'Deliveries\approveDelivery()'],['../class_delivery__m.html#a0da4006e5e92e627d40460024eab4fe8',1,'Delivery_m\approveDelivery()']]],
  ['approveinbound',['approveInbound',['../class_products.html#ab034996c6dd42a5dc993d337ff142066',1,'Products\approveInbound()'],['../class_product__m.html#a7db561b28b40a1674ec5c9764d8fd68e',1,'Product_m\approveInbound()']]],
  ['approveinvoice',['approveInvoice',['../class_invoices.html#ac631587f15a544e96e78e0e7e37208b7',1,'Invoices\approveInvoice()'],['../class_invoice__m.html#a58c5add09614936aa51a579f4676fa11',1,'Invoice_m\approveInvoice()']]],
  ['approveorder',['approveOrder',['../class_orders.html#a4bcf83f4598341cc2c9542566a49ec25',1,'Orders\approveOrder()'],['../class_order__m.html#a4da1a14a3aa13022a519dcbbb0670b52',1,'Order_m\approveOrder()']]],
  ['approvewaybill',['approveWaybill',['../class_waybills.html#a191b3f2a597c04d193a5ad462b1db6ef',1,'Waybills\approveWaybill()'],['../class_waybill__m.html#ae23059209624b37284be78cc8c20c916',1,'Waybill_m\approveWaybill()']]]
];
