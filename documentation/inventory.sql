-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 10, 2019 at 06:31 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`, `description`) VALUES
(1, 'packaging', 'All packaging materials'),
(2, 'raw materials', 'All raw materials\r\n'),
(3, 'container', 'Objects used to contain or transport other objects\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `credit` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_id`, `customer_code`, `company_name`, `first_name`, `last_name`, `address`, `phone`, `email`, `staff_id`, `credit`) VALUES
(1, '1000001', 'Balanced Fortune', 'Michael', 'Tevis', 'North,Memphis,2632 Woodridge Lane,38115', '901-314-0575', 'michael_tevis@gmail.com', 1, 0),
(2, '1000002', 'H. J. Wilson & Company', 'Robert', 'Elba', 'South,New Jersey,1809 Kelly Street Davidson,20863', '704-892-3911', 'rob_elba@yahoo.com', 1, 0),
(3, '1000003', 'Superior Interactive', 'Edward', 'Butler', 'New Orleans,Los Angeles,4153 Byrd Lane,70117', '504-947-8412', 'edward_butler@gmail.com', 1, 0),
(4, '1000004', 'Incluesiv', 'Beatrice', 'Raybon', 'Madison,Wisconsin,1272 Browning Lane,53703', ' 608-202-3333', 'bea_bon@gmail.com', 1, 0),
(5, '1000005', 'Monk Home Loans', 'Jesenia', 'Armijo', 'Medaryville,Indiana,3613 Duffy Street,47957', '219-843-5007', 'jennie-armijo@gmail.com', 1, 0),
(6, '1000006', 'Pro-Care Garden Maintenance', 'Lisa', 'Cohen', 'Mountain View,Wayomi,685 Arbor Court,82939', '307-360-7645', 'lisa_cohen@hotmail.com', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer_detail`
--

DROP TABLE IF EXISTS `tbl_customer_detail`;
CREATE TABLE IF NOT EXISTS `tbl_customer_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`detail_id`),
  KEY `customer_id` (`customer_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer_detail`
--

INSERT INTO `tbl_customer_detail` (`detail_id`, `customer_id`, `product_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 2, 3),
(5, 3, 3),
(6, 3, 4),
(7, 4, 1),
(8, 4, 4),
(9, 5, 2),
(10, 5, 4),
(11, 6, 3),
(12, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_delivery`
--

DROP TABLE IF EXISTS `tbl_delivery`;
CREATE TABLE IF NOT EXISTS `tbl_delivery` (
  `delivery_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_code` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `status` enum('awaiting approval','approved','cancelled','ready for delivery','en route','delivered successfully','processing','awaiting invoice') NOT NULL DEFAULT 'awaiting approval',
  `note` varchar(255) NOT NULL,
  `approve_date` date DEFAULT NULL,
  `approve_date_time` datetime DEFAULT NULL,
  `cancel_date` date DEFAULT NULL,
  `cancel_date_time` datetime DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `update_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`delivery_id`),
  KEY `order_id` (`order_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_delivery_detail`
--

DROP TABLE IF EXISTS `tbl_delivery_detail`;
CREATE TABLE IF NOT EXISTS `tbl_delivery_detail` (
  `delivery_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`delivery_detail_id`),
  KEY `delivery_id` (`delivery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inbound`
--

DROP TABLE IF EXISTS `tbl_inbound`;
CREATE TABLE IF NOT EXISTS `tbl_inbound` (
  `inbound_id` int(11) NOT NULL AUTO_INCREMENT,
  `inbound_code` varchar(255) NOT NULL,
  `approve_date` date DEFAULT NULL,
  `approve_date_time` datetime DEFAULT NULL,
  `cancel_date` date DEFAULT NULL,
  `cancel_date_time` datetime DEFAULT NULL,
  `staff_id` int(11) NOT NULL,
  `inbound_date` date DEFAULT NULL,
  `inbound_date_time` datetime DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `status` enum('awaiting approval','approved','cancelled') NOT NULL DEFAULT 'awaiting approval',
  `update_date` date DEFAULT NULL,
  `update_date_time` datetime DEFAULT NULL,
  `note` varchar(255) NOT NULL,
  PRIMARY KEY (`inbound_id`),
  KEY `staff_id` (`staff_id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inbound_detail`
--

DROP TABLE IF EXISTS `tbl_inbound_detail`;
CREATE TABLE IF NOT EXISTS `tbl_inbound_detail` (
  `inbound_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `inbound_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`inbound_detail_id`),
  KEY `inbound_id` (`inbound_id`),
  KEY `product_id` (`product_id`),
  KEY `quantity` (`quantity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invoice`
--

DROP TABLE IF EXISTS `tbl_invoice`;
CREATE TABLE IF NOT EXISTS `tbl_invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_code` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `total` double NOT NULL,
  `balance` double NOT NULL,
  `status` enum('awaiting approval','approved','cancelled','fully paid','ongoing') NOT NULL,
  `staff_id` int(11) NOT NULL,
  `approve_date` date DEFAULT NULL,
  `approve_date_time` datetime DEFAULT NULL,
  `cancel_date` date DEFAULT NULL,
  `cancel_date_time` datetime DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `update_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`invoice_id`),
  KEY `staff_id` (`staff_id`),
  KEY `waybill_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE IF NOT EXISTS `tbl_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_code` varchar(255) NOT NULL,
  `order_type` enum('sales','return') DEFAULT 'sales',
  `order_date` date DEFAULT NULL,
  `order_date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `error_msg` varchar(255) NOT NULL,
  `ordered_by` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `status` enum('awaiting approval','approved','cancelled') NOT NULL DEFAULT 'awaiting approval',
  `customer_id` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `approve_date` date DEFAULT NULL,
  `approve_date_time` datetime DEFAULT NULL,
  `cancel_date` date DEFAULT NULL,
  `cancel_date_time` datetime DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `update_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `staff_id` (`staff_id`),
  KEY `customer_id` (`customer_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_detail`
--

DROP TABLE IF EXISTS `tbl_order_detail`;
CREATE TABLE IF NOT EXISTS `tbl_order_detail` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_detail_code` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` double NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`order_detail_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

DROP TABLE IF EXISTS `tbl_payment`;
CREATE TABLE IF NOT EXISTS `tbl_payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `status` enum('cancelled','approved','awaiting approval') DEFAULT NULL,
  `payment_type` enum('cash','cheque','card') DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `invoice_id` (`invoice_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `unit_of_measurement` enum('cartons','kgs','pcs','palettes') NOT NULL,
  `in_stock` int(11) NOT NULL DEFAULT '0',
  `allocated` int(11) NOT NULL,
  `in_order` int(11) NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_code`, `product_name`, `description`, `price`, `unit_of_measurement`, `in_stock`, `allocated`, `in_order`, `note`, `category_id`) VALUES
(1, '2000001', 'cartons', 'A box or container usually made of paperboard and sometimes of corrugated fiberboard.', 1200, 'kgs', 1000000, 0, 0, '', 1),
(2, '2000002', 'drums', 'A cylindrical container used for shipping bulk cargo.', 3000, 'pcs', 1000000, 0, 0, '', 2),
(3, '2000003', 'bottles', 'A glass or plastic container with a narrow neck, used for storing drinks or other liquids.', 25, 'pcs', 1000000, 0, 0, '', 3),
(4, '2000004', 'reels', 'A round, drum-shaped object such as a spool used to carry various types of electrical wires.', 18000, 'kgs', 1000000, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_random`
--

DROP TABLE IF EXISTS `tbl_random`;
CREATE TABLE IF NOT EXISTS `tbl_random` (
  `customer_id` int(11) NOT NULL,
  `customer_code` varchar(255) NOT NULL,
  `current` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_random`
--

INSERT INTO `tbl_random` (`customer_id`, `customer_code`, `current`) VALUES
(1, '1000001', 1),
(2, '1000002', 0),
(3, '1000003', 0),
(4, '1000004', 0),
(5, '1000005', 0),
(6, '1000006', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE IF NOT EXISTS `tbl_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`role_id`, `role_name`, `description`) VALUES
(1, 'admin', 'Has all the privileges'),
(2, 'sales_staff', 'Sales order priviliges'),
(3, 'warehouse_staff', 'Warehouse priviliges'),
(4, 'finance_staff', 'Finance privileges');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

DROP TABLE IF EXISTS `tbl_staff`;
CREATE TABLE IF NOT EXISTS `tbl_staff` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_code` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `img` varchar(255) NOT NULL DEFAULT 'users.png',
  PRIMARY KEY (`staff_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`staff_id`, `staff_code`, `first_name`, `last_name`, `email`, `username`, `password`, `role_id`, `img`) VALUES
(1, '9000001', 'John', 'Doe', 'johndoe@gmail.com', 'admin1', 'a5ae4a73b76ac09a0e3430b185a033b0', 1, 'johndoe1.png'),
(2, '9000002', 'Jane', 'Doe', 'janedoe@yahoo.com', 'staff1', 'a5ae4a73b76ac09a0e3430b185a033b0', 2, 'janedoe2.png'),
(3, '9000003', 'Jack', 'Doe', 'jackdoe@gmail.com', 'warehouse1', 'a5ae4a73b76ac09a0e3430b185a033b0', 3, 'jackdoe3.png'),
(4, '9000004', 'Mary', 'Poppins', 'mpoppins@gmail.com', 'finance1', 'a5ae4a73b76ac09a0e3430b185a033b0', 4, 'users.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_waybill`
--

DROP TABLE IF EXISTS `tbl_waybill`;
CREATE TABLE IF NOT EXISTS `tbl_waybill` (
  `waybill_id` int(11) NOT NULL AUTO_INCREMENT,
  `waybill_code` varchar(255) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('awaiting approval','approved','cancelled') NOT NULL DEFAULT 'awaiting approval',
  `staff_id` int(11) NOT NULL,
  `approve_date` date DEFAULT NULL,
  `approve_date_time` datetime DEFAULT NULL,
  `cancel_date` date DEFAULT NULL,
  `cancel_date_time` datetime DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `update_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`waybill_id`),
  KEY `delivery_id` (`delivery_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD CONSTRAINT `tbl_customer_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `tbl_staff` (`staff_id`);

--
-- Constraints for table `tbl_delivery_detail`
--
ALTER TABLE `tbl_delivery_detail`
  ADD CONSTRAINT `tbl_delivery_detail_ibfk_1` FOREIGN KEY (`delivery_id`) REFERENCES `tbl_delivery` (`delivery_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD CONSTRAINT `tbl_order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customer` (`customer_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_order_detail`
--
ALTER TABLE `tbl_order_detail`
  ADD CONSTRAINT `tbl_order_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_order` (`order_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_order_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`product_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD CONSTRAINT `tbl_payment_ibfk_1` FOREIGN KEY (`invoice_id`) REFERENCES `tbl_invoice` (`invoice_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD CONSTRAINT `tbl_product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_category` (`category_id`);

--
-- Constraints for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD CONSTRAINT `tbl_staff_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tbl_role` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
